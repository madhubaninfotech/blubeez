require 'rubygems'
require 'sitemap_generator'

SitemapGenerator::Sitemap.default_host = "https://www.blubeez.com/"
SitemapGenerator::Sitemap.create do
	add '/destinations', :changefreq => 'daily', :priority => 0.9
	add '/hotels', :changefreq => 'daily', :priority => 0.9
	add '/flights', :changefreq => 'daily', :priority => 0.9
	add '/events', :changefreq => 'daily', :priority => 0.9
	add '/attractions', :changefreq => 'daily', :priority => 0.9
	add '/forums', :changefreq => 'daily', :priority => 0.9
	add '/blog', :changefreq => 'daily', :priority => 0.9
	add '/contribute', :changefreq => 'daily', :priority => 0.9
	add '/advertise', :changefreq => 'daily', :priority => 0.9
	add '/privacy', :changefreq => 'daily', :priority => 0.9
	add '/terms', :changefreq => 'daily', :priority => 0.9
	add '/contact', :changefreq => 'daily', :priority => 0.9
	AppUtils.seo_url_generator.each do |page|
		add "#{page}", :changefreq => 'always', :priority => 1
	end
end
SitemapGenerator::Sitemap.ping_search_engines # Not needed if you use the rake tasks