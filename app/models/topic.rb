class Topic < ActiveRecord::Base
  has_many :post_topics
  belongs_to :customer


  def self.filter_topics(type)
    if type["sortby"] == AppConstants::SORT_BY_COUNTRY_FOR_TOPIC.to_s
      topic = Topic.includes(:customer, :post_topics).where(:location_country=>type["sort_by_country"])
    elsif type["sortby"] == AppConstants::SORT_BY_STATE_FOR_TOPIC.to_s
      topic = Topic.includes(:customer, :post_topics).where(:location_state=>type["sort_by_country"])
    elsif type["sortby"] == AppConstants::SORT_BY_CITY_FOR_TOPIC.to_s
      topic = Topic.includes(:customer, :post_topics).where(:location_city=>type["sort_by_country"])
    end
    topic
  end

  def self.search_topic_by_country
    topics = Topic.all
    country_data_hash = Hash.new
    AppUtils.get_regions_by_country_code.each do |country|
      country_data_hash[country[1]] = Array.new
      topics.each do |topic|
        if topic.location_country == country[1]
          country_data_hash[country[1]] << topic
        end
      end
    end
    country_data_hash
  end

  def self.get_view_count(country_obj)
    data_obj = 0
    country_obj.each do |data|
      data_obj += data.view_count
    end
    data_obj
  end

  def self.get_view_region(data_obj)
    region_hash = Hash.new
    AppUtils.get_regions_by_country_code.each do |region|
      region_hash[region[2]] = Array.new unless region_hash.key?(region[2])
      region_hash[region[2]] << data_obj[region[1]] if data_obj[region[1]].present?
    end
    region_hash
  end

end
