
class ProfilesController < ApplicationController
	before_action :both_accessible?, :except=> [:index]

	def show
    @customer = Customer.find(params[:customer_id])
    @profile = @customer.profile
    if params[:notification_id].present?
      notification = UserNotification.where(id: params[:notification_id]).first
      notification.update(read_at: DateTime.now) if notification.present?
    end
    if !@profile.nil?
      @profile.d_o_b = @profile.d_o_b.to_date.strftime("%d/%m/%Y") if !@profile.d_o_b.nil?
      unless @profile.location.nil? 
        @location = AppUtils.location_formatter_parse(@profile.location)
      else
        @location = Hash.new
      end
    end
    
    if @profile.nil?
      redirect_to "/customers/#{@customer.id}/edit_profile"
    else
      render layout: "v2"
    end
  end

	def edit
    @customer = current_customer
    @profile = @customer.profile
    if !@profile.nil?
      @profile.d_o_b = @profile.d_o_b.to_date.strftime("%d/%m/%Y") if !@profile.d_o_b.nil?
      unless @profile.location.nil? 
        @location = AppUtils.location_formatter_parse(@profile.location) 
      else
        @location = Hash.new
      end
    else
      @location = Hash.new
    end
    render layout: "v2"
  end

  def update
    params["profile"][:d_o_b] = params["profile"][:d_o_b].to_date if !params["profile"][:d_o_b].nil?
    params[:profile][:location] = AppUtils.location_formatter_stringify(params[:locations]) unless params[:locations].nil?
    current_customer.update(customer_params)
    @profile = current_customer.profile
    if @profile.nil?
      params[:profile][:customer_id] = current_customer.id
    end
    @profile = @profile.nil? ? Profile.new : @profile

    if @profile.update(profile_params)
      if params[:profile][:avatar].present?
        flash[:notice] = "Image uploaded, please resize and crop as you wanted to show"
        render :crop, layout: "v2"
      else
    	  flash[:notice] = "Updated Successfully"
    	  redirect_to "/customers/#{params[:customer_id]}/profile"
      end
    else
    	flash[:error] = @profile.errors.full_messages.join("<br/>")
    end
  end

  def update_profile_image
    @profile = current_customer.profile.update(profile_params)
    flash[:notice] = "Profile picture is successfully uploaded."
    redirect_to "/customers/#{params[:customer_id]}/profile"
  end

  private
  def profile_params
    params.require(:profile).permit(:name, :email, :d_o_b, :avatar, :about_user, :location, :gender, :customer_id, :age, :avatar_crop_x, :avatar_crop_y, :avatar_crop_w, :avatar_crop_h, :crop_x, :crop_y, :crop_w, :crop_h)
  end

  def customer_params
    params.require(:customer).permit(:first_name, :last_name)
  end

end
