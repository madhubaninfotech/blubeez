class WelcomeJob
  include SuckerPunch::Job

  def perform(customer)
    t1 = Time.now
    puts "============================= JOB STARTED ======================="
    CustomerMailer.welcome_email(customer).deliver_now!
    puts "============================= JOB DONE ======================="
    puts "Time taken = #{Time.now - t1}"
  end

end