class Customers::RegistrationsController < Devise::RegistrationsController
before_action :configure_sign_up_params, only: [:create]
before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  def new
    super
  end

  # POST /resource
  def create
    build_resource(sign_up_params)
    session[:first_name] = sign_up_params[:first_name]
    session[:last_name] = sign_up_params[:last_name]
    session[:email] = sign_up_params[:email]
    if verify_recaptcha
      resource_saved = resource.save
      yield resource if block_given?
      if resource_saved
        if resource.active_for_authentication?
          set_flash_message :notice, :signed_up if is_flashing_format?
          sign_up(resource_name, resource)
          Customer.new.create_profile_for_current_user(current_customer)
          DynamicJob.perform_async
          WelcomeJob.perform_async(@customer)
          respond_with resource, location: after_sign_up_path_for(resource)
          session[:email] = session[:last_name] = session[:first_name] = nil
        else
          set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_flashing_format?
          expire_data_after_sign_in!
          respond_with resource, location: after_inactive_sign_up_path_for(resource)
        end
      else
        flash[:error] = resource.errors.full_messages.join("<br/>")
        clean_up_passwords resource
        @validatable = devise_mapping.validatable?
        if @validatable
          @minimum_password_length = resource_class.password_length.min
        end
        redirect_to "/customers/signin?signup=true"
      end
    else
      flash[:error] = "There was an error with the recaptcha code below. Please re-enter the code."
      clean_up_passwords(resource)
      redirect_to "/customers/signin?signup=true"
      flash.delete :recaptcha_error
    end
  end

  # GET /resource/edit
  def edit
    super
  end

  # PUT /resource
  def update
    super
    DynamicJob.perform_async
  end

  # DELETE /resource
  def destroy
    super
    DynamicJob.perform_async
  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :first_name, :last_name, :email, :password, :password_confirmation, :user_status])
  end

  # If you have extra params to permit, append them to the sanitizer.
  def configure_account_update_params
    devise_parameter_sanitizer.permit(:account_update, keys: [:name, :username, :first_name, :last_name, :email, :password, :password_confirmation, :user_status])
  end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end
