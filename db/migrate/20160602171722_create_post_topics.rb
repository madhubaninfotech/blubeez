class CreatePostTopics < ActiveRecord::Migration
  def change
    create_table :post_topics do |t|
      t.string :title
      t.text :content
      t.boolean :safe
      t.boolean :closed
      t.boolean :trusted
      t.belongs_to :topic, index: true, foreign_key: true
      t.belongs_to :customer, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
