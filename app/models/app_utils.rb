require 'digest/sha1'
require 'net/http'
require 'uri'
require 'money'
require 'money-rails'
require 'money/bank/google_currency'
require 'timezone'
require 'open_weather'
require 'rest-client'

module AppUtils

  def self.welcome
    puts "lets Start"
  end

  def self.country_list
    countries = ISO3166::Country.all_names_with_codes
    countries
  end
  
  def self.country_list_by_first_letter(keyword = "")
    list = ISO3166::Country.countries
    return list if keyword == ""
    list.select{|a| a.alpha2.upcase.starts_with?(keyword.upcase)}
  end

  def self.get_states_selectable_format(country_code)
    states = CS.get country_code
    states_arr = Array.new
    states.each do |city, value|
      states_arr << ["#{value}", "#{city}"]
    end

    states_arr

  end


  def self.get_location_details_for_flight_or_hotel(location_hash)
    destination = Destination.where("destination_name = ? AND destination_cities = ? AND destination_country = ? AND destination_state =? ", location_hash["name"], location_hash["cities"], location_hash["country"], location_hash["state"])
    if destination.empty?
      Destination.create(destination_name: location_hash["name"], destination_cities: location_hash["cities"], destination_country: location_hash["country"], destination_state:location_hash["state"] )
      destination = Destination.where("destination_name = ? AND destination_cities = ? AND destination_country = ? AND destination_state =? ", location_hash["name"], location_hash["cities"], location_hash["country"], location_hash["state"])
    end
    destination
  end

  def self.utc_time_to_local_time(utc_time)
    local_time = utc_time.getlocal
    local_time
  end

  def self.local_time_to_utc_time(local_time)
    utc_time = local_time.in_time_zone
    utc_time
  end

  def self.get_cities_selectable_format(country_code, state_code)
    cities = CS.get country_code, state_code

    cities_arr = Array.new
    if cities.present?
      cities.each do |city|
        cities_arr << ["#{city}", "#{city}"]
      end
    end

    cities_arr
  end

  def self.get_state_name_by_country(country_code, state_code)
    state = CS.get country_code
    state[state_code.to_sym]
  end

  def self.get_regions_by_country_name
    region_arr = Array.new

    ISO3166::Country.all.each do |country|
      region_arr << [country.name, country.country_code, country.continent]
    end

    region_arr
  end

  def self.get_regions_by_country_code
    region_arr = Array.new

    ISO3166::Country.all.each do |country|
      region_arr << [country.name, country.alpha2, country.continent]
    end

    region_arr
  end

  def self.countries_from_region(region_name)
    country_per_arr = Array.new

    self.get_regions_by_country_name.each do |country|
      if country[2] == region_name
        country_per_arr << [country[0], country[1]]
      end
    end

    country_per_arr
  end

  def self.countries_from_region_by_country_code_name(region_name)
    country_per_arr = Array.new

    self.get_regions_by_country_code.each do |country|
      if country[2] == region_name
        country_per_arr << [country[0], country[1]]
      end
    end

    country_per_arr
  end

  def self.countries_code_from_region(region_name)
    country_per_arr = Array.new

    self.get_regions_by_country_code.each do |country|
      if country[2] == region_name
        country_per_arr << [country[1]]
      end
    end

    country_per_arr
  end

  def self.country_name_by_country_code(country_code)
    country_name = ISO3166::Country[country_code].present? ? ISO3166::Country[country_code].name : ""
    country_name
  end

  def self.image_retreiver(name)
    uploader = ImageUploader.new
    uploader.retrieve_from_store!(name)
  end

  def self.location_formatter_stringify(location_params)
    location_str = "#{location_params["cities"]}, #{location_params["state"]}, #{location_params["country"]}"
    location_str
  end

  def self.location_formatter_parse(location_str)
    location_hash = Hash.new
    l_s = location_str.split(", ")
    location_hash["country"] = l_s[2]
    location_hash["state"] = l_s[1]
    location_hash["cities"] = l_s[0]
    location_hash
  end

  def self.seperate_muliple_images_from_array(string)
    images_arr = Array.new
    images = string.split("\"")
    images.each_with_index do |value, index|
      images_arr << value if index % 2 == 1
    end

    images_arr
  end

  def self.other_than_friends_sorter(all_users, myfriends, my_id)
    friends_id_arr = Array.new
    user_arr = Array.new
    friends_id_arr << my_id
    myfriends.each do |friend|
      friends_id_arr << friend.friend_id
    end
    friends_id_arr.uniq!
    all_users.each do |user|
      if !friends_id_arr.include?(user.id)
        user_arr << user
      end
    end
    user_arr
  end

  def self.destinations_mapping
    destination_hash = Hash.new
    Destination.all.each do |destination|
      destination_hash[destination.id] = destination.destination_name
    end

    destination_hash
  end
  #format String must be in the format of dd/mm/yyyy
  # def self.custom_date_formatter(date_str, format)
  #   d = date_str.split("/")
  #   n_d = "#{d[1]}/#{d[0]}/#{d[2]}"
  #   result_date = n_d.to_date.strftime(format)
  #   result_date
  # end

  def self.compile_search_data
    destinations = Destination.all.pluck(:destination_name)
    hotels = Hotel.all.pluck(:name)
    events = Event.all.pluck(:name)
    flights = Flight.all.pluck(:name)
    blogs = TravelBlog.all.pluck(:title)
    travellers = Customer.all.pluck(:name)


    search_arr = Array.new
    [destinations, hotels, events, flights, blogs, travellers].each_with_index do |search_data, index|
      categor = AppConstants::SEARCH_DATA_ARRAY[index]
      search_data.each do |data|
        temp = Hash.new
        temp["label"] = data
        temp["category"] = categor
        search_arr << temp
      end
    end
    search_arr.to_json
  end

  def self.get_recent_blogs
    blogs = TravelBlog.last(4)
    blogs
  end

  def self.get_popular_blogs
    blogs = TravelBlog.order(view_count: "DESC").last(4)
    blogs
  end

  def self.get_notification_format(notification)
    friend = Customer.find(notification.from_customer)
    html = ""
    link = ""
    html += "<strong>#{friend.name}<strong> has been"
    if notification.notify_type == AppConstants::TRAVEL_TRIP
      html += "Updated Travel Trip"
      link = "/customers/#{friend.id}/trips/#{notification.content_id}"
    elsif notification.notify_type == AppConstants::TRAVEL_BLOG
      html += "Updated Blog"
      link = "/customers/#{friend.id}/travel_blogs/#{notification.content_id}"
    elsif notification.notify_type == AppConstants::TRAVEL_PHOTOS
      html += "Uploaded Files"
      link = "/customers/#{friend.id}/photos/#{notification.content_id}"
    end
    [html, link]
  end

  def self.view_counter(result)
    if result.present?
      result.view_count += 1
      result.save!
    end
  end

  def self.sync_geocoder_coordinations
    destins = Destination.all
    destins.each do |destin|
      next if destin.latitude.present? && destin.longitude.present?
      location_params = Hash.new
      location_params["cities"] = destin.destination_cities
      location_params["state"] = destin.destination_state
      location_params["country"] = destin.destination_country
      location = self.location_formatter_stringify(location_params)
      destin.latitude, destin.longitude = Geocoder.coordinates(location)
      destin.save!
    end
  end

  def self.get_region_code(region_name)
    region = AppConstants::REGIONS_HASH.map{ |k, i| k if i == region_name }
    region.reject!(&:blank?)
    region.join().to_i
  end

  def self.get_tag_names_for_destination(destination)
    tag_arr = Array.new
    tag_arr << destination.destination_name
    tag_arr << destination.destination_cities
    tag_arr << AppUtils.country_name_by_country_code(destination.destination_country)
    tag_arr << AppUtils.get_state_name_by_country(destination.destination_country, destination.destination_state)
    tag_arr.join("|")
  end

  def self.convert_image_data_to_Images(d)
    image_paths_arr = []
    img_arr = []
    s_img = ""
    dir = ""
    images = []
    d_dir = ""
    if d.try(:image).present?
      dir = d.image[0].store_dir
      images = AppUtils.seperate_muliple_images_from_array(d.image[0].current_path)
    end
    if d.try(:destination_image).present?
      dir = d.destination_image[0].store_dir
      images = AppUtils.seperate_muliple_images_from_array(d.destination_image[0].current_path)
    end

    if images.present?
      images.each do |image|
        image_paths_arr << "#{Rails.root}/public/#{dir}/#{image}"
      end
    end


    if image_paths_arr.present?
      image_paths_arr.each do |img_path|
        if File.exists?(img_path)
          image = Image.new
          image.images = File.open(img_path)
          if image.save
            img_arr << image.id
          end
        end
      end
    end

    if d.main_image.present?
      single_image = d.main_image
      if File.exists?(single_image.path)
        s_image = Image.new
        s_image.image = File.open(single_image.path)
        if s_image.save
          s_img = s_image.id
        end
      end
    end
    temp = d.build_temp_image
    temp.image = s_img
    temp.images = img_arr
    temp.save
  end

  def self.genarate_image_for_event_blog_datas(d)
    image_paths_arr = []
    img_arr = []
    s_img = ""
    dir = ""
    images = []

    if d.image.present?
      single_image = d.image
      if File.exists?(single_image.path)
        s_image = Image.new
        s_image.image = File.open(single_image.path)
        if s_image.save
          s_img = s_image.id
        end
      end
    end
    temp = d.build_temp_image
    temp.image = s_img
    temp.save
  end

  def self.generate_each_data
    hotels = Hotel.all
    hotels.each do |h|
      AppUtils.convert_image_data_to_Images(h)
    end

    Destination.all.each do |h|
      AppUtils.convert_image_data_to_Images(h)
    end

    Flight.all.each do |h|
      AppUtils.convert_image_data_to_Images(h)
    end
  end

  def self.genarate_image_for_event_blog
    TravelBlog.all.each do |d|
      self.genarate_image_for_event_blog_datas(d)
    end

    Event.all.each do |d|
      self.genarate_image_for_event_blog_datas(d)
    end
  end

  def self.genarate_image_data
    self.generate_each_data
    self.genarate_image_for_event_blog
  end

  def self.assign_to_generated_data
    Hotel.all.each do |hotel|
      hotel.main_image = hotel.temp_image.image
      hotel.image = hotel.temp_image.images
      hotel.save!
    end

    Flight.all.each do |flight|
      flight.main_image = flight.temp_image.image
      flight.image = flight.temp_image.images
      flight.save!
    end

    Destination.all.each do |destination|
      destination.main_image = destination.temp_image.image
      destination.destination_image = destination.temp_image.images
      destination.save!
    end

    TravelBlog.all.each do |tb|
      tb.image = tb.temp_image.image
      tb.save!
    end

    Event.all.each do |tb|
      tb.image = tb.temp_image.image
      tb.save!
    end


  end

  def self.get_single_image_from_image_modal(img)
    images = Image.where("id = (?)", img).first
    images
  end

  def self.get_images_from_image_modal(array)
    data_arr = JSON.parse(array)
    images_arr = [];
    images = Image.where("id in (?)", data_arr)
    images.each do |image|
      images_arr << [image.id, image.images]
    end
    images_arr
  end
  
  def self.get_travellers_images(reviews)
    image_hash = Hash.new
    img_arr = Array.new
    reviews.each do |review|
      if review.images.present?
        customer_id = review.customer.id
        unless image_hash[customer_id].present?
          image_hash[customer_id] =  Hash.new
          image_hash[customer_id]["name"] = review.customer.name
          img_arr = Array.new
        end
        images = JSON.parse(review.images)
        images.each do |img|
          img_arr << img
        end
        image_hash[customer_id]["images"] = Image.where(:id=> img_arr)
      end  
    end
    image_hash
  end

  def self.get_time_zone_name(latitude, longitude)
    zone_name = ""
    zone_format_name = ""
    zone_offset = ""
    begin
      zone_name = Timezone.lookup(latitude, longitude).name
      zone = ActiveSupport::TimeZone[zone_name]
      zone_format_name = zone.now.zone
      zone_offset = zone.now.formatted_offset
    rescue StandardError => error
      
    end
    [zone_name, zone_format_name + " " + zone_offset]
  end

  def self.get_current_weather_data_by_destination(latitude, longitude)
    data = OpenWeather::Current.geocode(latitude, longitude, options = {APPID: "#{AppConfig.get_weather_key}", units: "metric"})
    data
  end

  def self.convert_celcius_to_forenheit(celcius)
    fahrenheit = (celcius * (9.0 / 5.0)) + 32
    fahrenheit.round(2)
  end

  def self.convert_forenheit_to_celcius(fahrenheit)
    celcius = (fahrenheit - 32) * 5.0 / 9.0
    celcius.round(2)
  end

  def self.send_simple_message
    RestClient.post "https://api:key-dba1c1d81c21a6009d50d99adf559475@api.mailgun.net/v3/sandbox25dcaae7ea184dbb8a6a449130b60f88.mailgun.org/messages",
    :from => "postmaster <postmaster@sandbox25dcaae7ea184dbb8a6a449130b60f88.mailgun.org>",
    :to => "mkmmslearn@gmail.com",
    :subject => "Hello",
    :text => "Testing some Mailgun awesomeness!"
  end

  def self.seo_url_generator
    seo_urls = Array.new

    destinations = Destination.includes(:hotels, :flights, :attractions, :ratings, :events).all

    destinations.each do |dest|
      seo_urls << "/destinations/#{dest.id}/#{dest.destination_name}"
      dest.hotels do |hotel|
        seo_ulrs << "/destinations/#{dest.id}/#{dest.destination_name}/hotels/#{hotel.id}/#{hotel.name}"
      end
      dest.flights do |flight|
        seo_ulrs << "/destinations/#{dest.id}/#{dest.destination_name}/flights/#{flight.id}/#{flight.name}"
      end
      dest.events do |event|
        seo_ulrs << "/destinations/#{dest.id}/#{dest.destination_name}/events/#{event.id}/#{event.name}"
      end
      dest.attractions do |attraction|
        seo_ulrs << "/destinations/#{dest.id}/#{dest.destination_name}/attractions/#{attraction.id}/#{attraction.name}"
      end
    end

    TravelBlog.all.each do |blog|
      seo_urls << "/blog/post/#{blog.id}"
    end

    seo_urls
  end

  def self.update_slug

    destinations = Destination.includes(:hotels, :flights, :attractions, :events).all

    destinations.each do |dest|
      dest.slug = dest.destination_name.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
      dest.save

      dest.attractions.each do |atr|
        atr.slug = atr.name.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
        atr.save
      end

      dest.events.each do |evt|
        evt.slug = evt.name.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
        evt.save
      end

      dest.hotels.each do |htl|
        htl.slug = htl.name.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
        htl.save
      end

      dest.flights.each do |flt|
        flt.slug = flt.name.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
        flt.save
      end
    end
    
  end

  def self.update_customer_visibility
    Customer.all.each do |cust|
      next if cust.is_public.present?
      cust.is_public = AppConstants::NO
      cust.save
    end
  end

  def self.get_popular_countries
    counties_list = self.country_list

    destinations_hash = Destination.all.pluck(:destination_country, :destination_state).inject({}){|obj, arr|
      country_code, state_id = arr
      
      state = self.get_state_name_by_country(country_code, state_id)

      country = counties_list.select{|ct| ct[1] == country_code}[0]
      country = country[0] unless country.blank?

      if country.present? && state.present?
        if obj.keys.include?(country)
          if obj[country].keys.include?(state)
            obj[country][state] += 1
          else
            obj[country][state] = 1
          end
        else  
          obj[country] = {}

          if obj[country].keys.include?(state)
            obj[country][state] += 1
          else
            obj[country][state] = 1
          end
        end  
      end
      obj
    }
  end

  def self.set_image_association image, id, model_name, image_type
    image.imageable_type = model_name
    image.imageable_id = id
    image.image_type = image_type
    image.save
  end

  def self.associate_images_with_destination
    Destination.all.each do |dest|
      image = Image.where("id = (?)", dest.main_image).first
      if image.present?
        set_image_association(image, dest.id, "Destination", "main_image")
      end
      if dest.destination_image.present?
        images = Image.where(id: JSON.parse(dest.destination_image))
        images.each do |img|
          set_image_association(img, dest.id, "Destination", "destination_image")
        end
      end
    end
  end

  def self.update_existing_user_confirmation
    Customer.all.each do |customer|
      customer.confirmation_sent_at = DateTime.new(2019, 1, 1, 9,00)
      customer.confirmed_at = DateTime.new(2019, 1, 1, 10,00)
      customer.save
    end
  end

  def self.generate_username
    Customer.all.each do |customer|
      name = customer.fullname.to_s.split("@")[0]
      customer.username = name.to_s.gsub(" ",'') + customer.id.to_s
      customer.save
    end
  end
    
end
