class CreateTempImage < ActiveRecord::Migration
  def change
    create_table :temp_images do |t|
      t.string :image
      t.text :images

      t.belongs_to :travel_blog, index: true, foreign_key: true
      t.belongs_to :hotel, index: true, foreign_key: true
      t.belongs_to :flight, index: true, foreign_key: true
      t.belongs_to :destination, index: true, foreign_key: true
      t.belongs_to :event, index: true, foreign_key: true
    end
  end
end
