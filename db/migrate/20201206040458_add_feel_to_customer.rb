class AddFeelToCustomer < ActiveRecord::Migration[5.0]
  def change
    add_column :customers, :feel_type, :integer, default: Customer::FEELING_HAPPY
    add_column :customers, :feel_msg, :text
  end
end
