# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20210620171831) do

  create_table "about_us", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.text "about", limit: 65535
  end

  create_table "active_admin_comments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string   "namespace"
    t.text     "body",                   limit: 65535
    t.string   "resource_type"
    t.integer  "resource_id"
    t.string   "author_type"
    t.integer  "author_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "email",                  limit: 20
    t.string   "encrypted_password",     limit: 60
    t.string   "reset_password_token",   limit: 1
    t.string   "reset_password_sent_at", limit: 1
    t.string   "remember_created_at",    limit: 1
    t.integer  "sign_in_count"
    t.string   "current_sign_in_at",     limit: 19
    t.string   "last_sign_in_at",        limit: 19
    t.string   "current_sign_in_ip",     limit: 14
    t.string   "last_sign_in_ip",        limit: 14
    t.string   "name",                   limit: 1
    t.string   "is_admin",               limit: 1
    t.string   "is_active",              limit: 1
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree
  end

  create_table "admin_users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string   "email",                             default: "", null: false
    t.string   "encrypted_password",                default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count"
    t.string   "current_sign_in_at",     limit: 19
    t.string   "last_sign_in_at",        limit: 19
    t.string   "current_sign_in_ip",     limit: 14
    t.string   "last_sign_in_ip",        limit: 14
    t.string   "created_at",             limit: 19
    t.string   "updated_at",             limit: 19
    t.string   "name",                   limit: 1
    t.string   "is_admin",               limit: 1
    t.string   "is_active",              limit: 1
    t.index ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "admins", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "name"
    t.boolean  "is_admin"
    t.integer  "is_active"
    t.index ["email"], name: "index_admins_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree
  end

  create_table "attractions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string   "name"
    t.string   "addr_no"
    t.string   "addr_street"
    t.string   "addr_city"
    t.text     "image",          limit: 65535
    t.integer  "price"
    t.integer  "destination_id"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.integer  "view_count",                   default: 0
    t.string   "main_image"
    t.string   "slug"
    t.text     "title",          limit: 65535
    t.text     "keywords",       limit: 65535
    t.index ["destination_id"], name: "index_attractions_on_destination_id", using: :btree
    t.index ["slug"], name: "index_attractions_on_slug", using: :btree
  end

  create_table "banners", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string   "banner_image"
    t.integer  "admin_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "title"
    t.text     "desc",         limit: 65535
    t.string   "button_text"
    t.text     "button_link",  limit: 65535
    t.index ["admin_id"], name: "index_banners_on_admin_id", using: :btree
  end

  create_table "blog_comments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string   "name"
    t.text     "comment",        limit: 65535
    t.integer  "reply_for"
    t.integer  "travel_blog_id"
    t.integer  "customer_id"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.index ["customer_id"], name: "index_blog_comments_on_customer_id", using: :btree
    t.index ["travel_blog_id"], name: "index_blog_comments_on_travel_blog_id", using: :btree
  end

  create_table "careers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.text "careers", limit: 65535
  end

  create_table "checkins", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.integer  "customer_id"
    t.integer  "destination_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["customer_id"], name: "index_checkins_on_customer_id", using: :btree
    t.index ["destination_id"], name: "index_checkins_on_destination_id", using: :btree
  end

  create_table "comments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "name"
    t.text     "comment",     limit: 65535
    t.integer  "customer_id"
    t.integer  "post_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["customer_id"], name: "index_comments_on_customer_id", using: :btree
    t.index ["post_id"], name: "index_comments_on_post_id", using: :btree
  end

  create_table "commontator_comments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "thread_id",                                   null: false
    t.string   "creator_type",                                null: false
    t.integer  "creator_id",                                  null: false
    t.string   "editor_type"
    t.integer  "editor_id"
    t.text     "body",              limit: 65535,             null: false
    t.datetime "deleted_at"
    t.integer  "cached_votes_up",                 default: 0
    t.integer  "cached_votes_down",               default: 0
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.integer  "parent_id"
    t.index ["cached_votes_down"], name: "index_commontator_comments_on_cached_votes_down", using: :btree
    t.index ["cached_votes_up"], name: "index_commontator_comments_on_cached_votes_up", using: :btree
    t.index ["creator_id", "creator_type", "thread_id"], name: "index_commontator_comments_on_c_id_and_c_type_and_t_id", using: :btree
    t.index ["editor_type", "editor_id"], name: "index_commontator_comments_on_editor_type_and_editor_id", using: :btree
    t.index ["parent_id"], name: "index_commontator_comments_on_parent_id", using: :btree
    t.index ["thread_id", "created_at"], name: "index_commontator_comments_on_thread_id_and_created_at", using: :btree
  end

  create_table "commontator_subscriptions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "thread_id",       null: false
    t.string   "subscriber_type", null: false
    t.integer  "subscriber_id",   null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["subscriber_id", "subscriber_type", "thread_id"], name: "index_commontator_subscriptions_on_s_id_and_s_type_and_t_id", unique: true, using: :btree
    t.index ["thread_id"], name: "index_commontator_subscriptions_on_thread_id", using: :btree
  end

  create_table "commontator_threads", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "commontable_type"
    t.integer  "commontable_id"
    t.string   "closer_type"
    t.integer  "closer_id"
    t.datetime "closed_at"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["closer_type", "closer_id"], name: "index_commontator_threads_on_closer_type_and_closer_id", using: :btree
    t.index ["commontable_type", "commontable_id"], name: "index_commontator_threads_on_c_id_and_c_type", unique: true, using: :btree
  end

  create_table "conversations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "sender_id"
    t.integer  "receiver_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "customers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string   "email",                                default: "", null: false
    t.string   "encrypted_password",                   default: "", null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                        default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.string   "name"
    t.integer  "user_status"
    t.integer  "is_public",                            default: 0
    t.integer  "feel_type",                            default: 1
    t.text     "feel_msg",               limit: 65535
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "username"
    t.index ["email"], name: "index_customers_on_email", unique: true, using: :btree
    t.index ["first_name"], name: "index_customers_on_first_name", using: :btree
    t.index ["last_name"], name: "index_customers_on_last_name", using: :btree
    t.index ["reset_password_token"], name: "index_customers_on_reset_password_token", unique: true, using: :btree
    t.index ["username"], name: "index_customers_on_username", using: :btree
  end

  create_table "destinations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string   "destination_name"
    t.string   "destination_country"
    t.string   "destination_state"
    t.string   "destination_cities"
    t.text     "destination_image",       limit: 65535
    t.string   "destination_category"
    t.float    "price_from",              limit: 24
    t.text     "destination_description", limit: 65535
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.integer  "best_for"
    t.integer  "view_count",                            default: 0
    t.float    "latitude",                limit: 24
    t.float    "longitude",               limit: 24
    t.string   "main_image"
    t.string   "slug"
    t.text     "title",                   limit: 65535
    t.text     "keywords",                limit: 65535
    t.index ["slug"], name: "index_destinations_on_slug", using: :btree
  end

  create_table "events", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string   "name"
    t.string   "image"
    t.text     "about",          limit: 65535
    t.text     "detail",         limit: 65535
    t.datetime "date"
    t.integer  "destination_id"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.integer  "view_count",                   default: 0
    t.datetime "start_date"
    t.datetime "end_date"
    t.string   "slug"
    t.text     "title",          limit: 65535
    t.text     "keywords",       limit: 65535
    t.index ["destination_id"], name: "index_events_on_destination_id", using: :btree
    t.index ["slug"], name: "index_events_on_slug", using: :btree
  end

  create_table "faqs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.text "faq", limit: 65535
  end

  create_table "flight_facilities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string  "checkin"
    t.string  "checkout"
    t.text    "cancelorprepay", limit: 65535
    t.string  "childrenbeds"
    t.string  "pets"
    t.text    "groups",         limit: 65535
    t.string  "cardsaccepted"
    t.integer "flight_id"
    t.index ["flight_id"], name: "index_flight_facilities_on_flight_id", using: :btree
  end

  create_table "flights", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string   "name"
    t.string   "service_no"
    t.integer  "price"
    t.string   "image"
    t.integer  "destination_id"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.string   "highlights",                   default: "--- []\n"
    t.integer  "view_count",                   default: 0
    t.string   "main_image"
    t.string   "flight_to"
    t.string   "slug"
    t.text     "title",          limit: 65535
    t.text     "keywords",       limit: 65535
    t.index ["destination_id"], name: "index_flights_on_destination_id", using: :btree
    t.index ["slug"], name: "index_flights_on_slug", using: :btree
  end

  create_table "follows", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "followable_type",                 null: false
    t.integer  "followable_id",                   null: false
    t.string   "follower_type",                   null: false
    t.integer  "follower_id",                     null: false
    t.boolean  "blocked",         default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["followable_id", "followable_type"], name: "fk_followables", using: :btree
    t.index ["follower_id", "follower_type"], name: "fk_follows", using: :btree
  end

  create_table "friends", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string   "name"
    t.integer  "friend_id"
    t.integer  "customer_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["customer_id"], name: "index_friends_on_customer_id", using: :btree
  end

  create_table "hotel_facilities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string  "view"
    t.text    "activities",      limit: 65535
    t.text    "mediatechnology", limit: 65535
    t.text    "food",            limit: 65535
    t.string  "internet"
    t.string  "parking"
    t.text    "services",        limit: 65535
    t.text    "general",         limit: 65535
    t.text    "languages",       limit: 65535
    t.integer "hotel_id"
    t.index ["hotel_id"], name: "index_hotel_facilities_on_hotel_id", using: :btree
  end

  create_table "hotels", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.integer  "destination_id"
    t.string   "name"
    t.text     "description",    limit: 65535
    t.string   "image"
    t.text     "price_from",     limit: 65535
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.string   "amenities",                    default: "--- []\n"
    t.integer  "view_count",                   default: 0
    t.string   "main_image"
    t.string   "slug"
    t.text     "title",          limit: 65535
    t.text     "keywords",       limit: 65535
    t.index ["destination_id"], name: "index_hotels_on_destination_id", using: :btree
    t.index ["slug"], name: "index_hotels_on_slug", using: :btree
  end

  create_table "identities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string   "provider"
    t.string   "uid"
    t.integer  "customer_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["customer_id"], name: "index_identities_on_customer_id", using: :btree
  end

  create_table "images", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string   "image"
    t.text     "images",               limit: 65535
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.text     "desc",                 limit: 65535
    t.boolean  "is_watermark_enabled"
    t.string   "imageable_type"
    t.integer  "imageable_id"
    t.string   "image_type"
    t.index ["image_type"], name: "index_images_on_image_type", using: :btree
    t.index ["imageable_type", "imageable_id"], name: "index_images_on_imageable_type_and_imageable_id", using: :btree
  end

  create_table "listings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.string   "location"
    t.string   "service_desc"
    t.string   "website"
    t.boolean  "is_owner"
    t.text     "message",      limit: 65535
    t.boolean  "is_read",                    default: false
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  create_table "messages", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.text     "body",            limit: 65535
    t.integer  "conversation_id"
    t.integer  "customer_id"
    t.boolean  "read",                          default: false
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.index ["conversation_id"], name: "index_messages_on_conversation_id", using: :btree
    t.index ["customer_id"], name: "index_messages_on_customer_id", using: :btree
  end

  create_table "mores", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string  "check_in"
    t.string  "check_out"
    t.text    "pets",      limit: 65535
    t.string  "payments"
    t.integer "hotel_id"
    t.index ["hotel_id"], name: "index_mores_on_hotel_id", using: :btree
  end

  create_table "notifications", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.integer  "from_customer"
    t.string   "to_customer",                 default: "--- []\n"
    t.text     "note",          limit: 65535
    t.integer  "notify_type"
    t.integer  "content_id"
    t.integer  "status"
    t.string   "link"
    t.integer  "customer_id"
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.index ["customer_id"], name: "index_notifications_on_customer_id", using: :btree
  end

  create_table "photos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string   "trip_name"
    t.string   "location"
    t.text     "trip_description", limit: 65535
    t.string   "attachments",                    default: "--- []\n"
    t.string   "videos"
    t.datetime "date"
    t.integer  "customer_id"
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.index ["customer_id"], name: "index_photos_on_customer_id", using: :btree
  end

  create_table "post_images", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "image"
    t.integer  "post_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["post_id"], name: "index_post_images_on_post_id", using: :btree
  end

  create_table "post_topics", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string   "title"
    t.text     "content",     limit: 65535
    t.boolean  "safe"
    t.boolean  "closed"
    t.boolean  "trusted"
    t.integer  "topic_id"
    t.integer  "customer_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["customer_id"], name: "index_post_topics_on_customer_id", using: :btree
    t.index ["topic_id"], name: "index_post_topics_on_topic_id", using: :btree
  end

  create_table "posts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "name"
    t.text     "address",     limit: 65535
    t.string   "city"
    t.string   "country"
    t.integer  "customer_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "state"
    t.index ["customer_id"], name: "index_posts_on_customer_id", using: :btree
  end

  create_table "privacies", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.text "privacy", limit: 65535
  end

  create_table "profiles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "d_o_b"
    t.string   "avatar"
    t.text     "about_user",  limit: 65535
    t.string   "location"
    t.string   "gender"
    t.integer  "customer_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "age"
    t.index ["customer_id"], name: "index_profiles_on_customer_id", using: :btree
  end

  create_table "ratings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.integer  "flight_id"
    t.integer  "hotel_id"
    t.integer  "destination_id"
    t.integer  "customer_id"
    t.integer  "score",          default: 0
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["customer_id"], name: "index_ratings_on_customer_id", using: :btree
    t.index ["destination_id"], name: "index_ratings_on_destination_id", using: :btree
    t.index ["flight_id"], name: "index_ratings_on_flight_id", using: :btree
    t.index ["hotel_id"], name: "index_ratings_on_hotel_id", using: :btree
  end

  create_table "report_frauds", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "name"
    t.string   "email"
    t.boolean  "is_registered_with_blubeez"
    t.string   "phone"
    t.string   "fraud_against_person"
    t.string   "fraud_against_url"
    t.text     "message",                    limit: 65535
    t.boolean  "is_action_taken",                          default: false
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
  end

  create_table "report_spams", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "name"
    t.string   "email"
    t.boolean  "is_registered_with_blubeez"
    t.string   "phone"
    t.string   "spam_against_person"
    t.string   "spam_profile_link"
    t.integer  "spam_type"
    t.text     "message",                    limit: 65535
    t.boolean  "is_action_taken",                          default: false
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
  end

  create_table "reviews", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string   "name"
    t.text     "comment",        limit: 65535
    t.integer  "customer_id"
    t.integer  "hotel_id"
    t.integer  "flight_id"
    t.integer  "destination_id"
    t.integer  "event_id"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.text     "images",         limit: 65535
    t.text     "tip",            limit: 65535
    t.integer  "vote",                         default: 0
    t.text     "voter_id",       limit: 65535
    t.index ["customer_id"], name: "index_reviews_on_customer_id", using: :btree
    t.index ["destination_id"], name: "index_reviews_on_destination_id", using: :btree
    t.index ["event_id"], name: "index_reviews_on_event_id", using: :btree
    t.index ["flight_id"], name: "index_reviews_on_flight_id", using: :btree
    t.index ["hotel_id"], name: "index_reviews_on_hotel_id", using: :btree
  end

  create_table "settings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string   "var",                      null: false
    t.text     "value",      limit: 65535
    t.integer  "thing_id"
    t.string   "thing_type", limit: 30
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["thing_type", "thing_id", "var"], name: "index_settings_on_thing_type_and_thing_id_and_var", unique: true, using: :btree
  end

  create_table "short_links", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "url"
    t.string   "slug"
    t.integer  "clicked",    default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "taggings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
    t.index ["context"], name: "index_taggings_on_context", using: :btree
    t.index ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
    t.index ["tag_id"], name: "index_taggings_on_tag_id", using: :btree
    t.index ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree
    t.index ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy", using: :btree
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id", using: :btree
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type", using: :btree
    t.index ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type", using: :btree
    t.index ["tagger_id"], name: "index_taggings_on_tagger_id", using: :btree
  end

  create_table "tags", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string  "name",                       collation: "utf8_bin"
    t.integer "taggings_count", default: 0
    t.index ["name"], name: "index_tags_on_name", unique: true, using: :btree
  end

  create_table "temp_images", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string  "image"
    t.text    "images",         limit: 65535
    t.integer "travel_blog_id"
    t.integer "hotel_id"
    t.integer "flight_id"
    t.integer "destination_id"
    t.integer "event_id"
    t.index ["destination_id"], name: "index_temp_images_on_destination_id", using: :btree
    t.index ["event_id"], name: "index_temp_images_on_event_id", using: :btree
    t.index ["flight_id"], name: "index_temp_images_on_flight_id", using: :btree
    t.index ["hotel_id"], name: "index_temp_images_on_hotel_id", using: :btree
    t.index ["travel_blog_id"], name: "index_temp_images_on_travel_blog_id", using: :btree
  end

  create_table "terms", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.text "terms", limit: 65535
  end

  create_table "topics", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string   "title"
    t.text     "content",          limit: 65535
    t.boolean  "safe"
    t.boolean  "closed"
    t.boolean  "trusted"
    t.integer  "customer_id"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.string   "location_name"
    t.string   "location_country"
    t.string   "location_state"
    t.string   "location_city"
    t.integer  "view_count",                     default: 0
    t.integer  "additional_topic"
    t.index ["customer_id"], name: "index_topics_on_customer_id", using: :btree
  end

  create_table "travel_blogs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string   "title"
    t.text     "description", limit: 65535
    t.integer  "customer_id"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "image"
    t.integer  "view_count",                default: 0
    t.integer  "admin_id"
    t.text     "category",    limit: 65535
    t.index ["admin_id"], name: "index_travel_blogs_on_admin_id", using: :btree
    t.index ["customer_id"], name: "index_travel_blogs_on_customer_id", using: :btree
  end

  create_table "trips", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.string   "trip_name"
    t.string   "location"
    t.text     "trip_description", limit: 65535
    t.string   "trip_photos"
    t.datetime "start_date"
    t.datetime "end_date"
    t.integer  "trip_status"
    t.integer  "customer_id"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.integer  "view_count",                     default: 0
    t.index ["customer_id"], name: "index_trips_on_customer_id", using: :btree
  end

  create_table "user_notifications", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "recipient_id"
    t.integer  "friend_id"
    t.datetime "read_at"
    t.string   "action"
    t.integer  "notifiable_id"
    t.string   "notifiable_type"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "votes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "votable_type"
    t.integer  "votable_id"
    t.string   "voter_type"
    t.integer  "voter_id"
    t.boolean  "vote_flag"
    t.string   "vote_scope"
    t.integer  "vote_weight"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope", using: :btree
    t.index ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope", using: :btree
  end

  create_table "wishlists", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin" do |t|
    t.integer  "customer_id"
    t.integer  "destination_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["customer_id"], name: "index_wishlists_on_customer_id", using: :btree
    t.index ["destination_id"], name: "index_wishlists_on_destination_id", using: :btree
  end

  add_foreign_key "attractions", "destinations"
  add_foreign_key "banners", "admins"
  add_foreign_key "blog_comments", "customers"
  add_foreign_key "blog_comments", "travel_blogs"
  add_foreign_key "checkins", "customers"
  add_foreign_key "checkins", "destinations"
  add_foreign_key "comments", "customers"
  add_foreign_key "comments", "posts"
  add_foreign_key "commontator_comments", "commontator_comments", column: "parent_id", on_delete: :cascade
  add_foreign_key "commontator_comments", "commontator_threads", column: "thread_id", on_update: :cascade, on_delete: :cascade
  add_foreign_key "commontator_subscriptions", "commontator_threads", column: "thread_id", on_update: :cascade, on_delete: :cascade
  add_foreign_key "events", "destinations"
  add_foreign_key "flight_facilities", "flights"
  add_foreign_key "flights", "destinations"
  add_foreign_key "friends", "customers"
  add_foreign_key "hotel_facilities", "hotels"
  add_foreign_key "identities", "customers"
  add_foreign_key "messages", "conversations"
  add_foreign_key "messages", "customers"
  add_foreign_key "mores", "hotels"
  add_foreign_key "notifications", "customers"
  add_foreign_key "photos", "customers"
  add_foreign_key "post_images", "posts"
  add_foreign_key "post_topics", "customers"
  add_foreign_key "post_topics", "topics"
  add_foreign_key "posts", "customers"
  add_foreign_key "profiles", "customers"
  add_foreign_key "ratings", "customers"
  add_foreign_key "ratings", "destinations"
  add_foreign_key "ratings", "flights"
  add_foreign_key "ratings", "hotels"
  add_foreign_key "reviews", "customers"
  add_foreign_key "reviews", "destinations"
  add_foreign_key "reviews", "events"
  add_foreign_key "reviews", "flights"
  add_foreign_key "reviews", "hotels"
  add_foreign_key "temp_images", "destinations"
  add_foreign_key "temp_images", "events"
  add_foreign_key "temp_images", "flights"
  add_foreign_key "temp_images", "hotels"
  add_foreign_key "temp_images", "travel_blogs"
  add_foreign_key "topics", "customers"
  add_foreign_key "travel_blogs", "admins"
  add_foreign_key "travel_blogs", "customers"
  add_foreign_key "trips", "customers"
  add_foreign_key "wishlists", "customers"
  add_foreign_key "wishlists", "destinations"
end
