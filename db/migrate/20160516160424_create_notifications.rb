class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.integer :from_customer
      t.string :to_customer, array: true, default: []
      t.text :note
      t.integer :notify_type
      t.integer :content_id
      t.integer :status
      t.string :link
      t.belongs_to :customer, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
