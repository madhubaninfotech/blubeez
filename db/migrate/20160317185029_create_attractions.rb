class CreateAttractions < ActiveRecord::Migration
  def change
    create_table :attractions do |t|
      t.string :name
      t.string :addr_no
      t.string :addr_street
      t.string :addr_city
      t.string :image
      t.integer :price
      t.belongs_to :destination, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
