class WishlistsController < ApplicationController
  before_filter :authenticate_customer!

  def index
    @customer = Customer.includes(:wishlist).find(params["customer_id"])
    @wishlist = @customer.wishlist.pluck(:destination_id)
    @destinations = Destination.includes(:attractions, :ratings, :events).where("id in (?) ", @wishlist).order("created_at DESC")
    @profile = @customer.profile
    render layout: "v2"
  end
  
  def create
    @wishlist = Wishlist.where("customer_id = (?) AND destination_id = (?)", params["wish"]["customer_id"], params["wish"]["destination_id"])
    @is_added = true
    if @wishlist.present?
      @destination = @wishlist.first.destination_id
      if @wishlist.destroy_all
        @is_added = false
        flash.now[:success] = "Removed from Wishlist"
      else
        flash.now[:error] = @wishlist.errors
      end
    else
      @wishlist = Wishlist.new(wishlist_params)
      @destination = @wishlist.destination_id
      if @wishlist.save!
        flash.now[:success] = "Added to Wishlist"
      else
        flash.now[:error] = @wishlist.errors
      end
    end
    @wishlist_count = Wishlist.where("destination_id = (?)", params["wish"]["destination_id"]).count
  end

  def destroy
    @customer = Customer.find(params["customer_id"])
    @wishlist = Wishlist.where("customer_id = (?) AND destination_id = (?)", params["customer_id"], params["id"])
    if @wishlist.destroy_all
      flash[:success] = "Removed from Wishlist"
      redirect_to "/customers/#{@customer.id}/wishlists"
    end
  end
  
  private
  def wishlist_params
    params.require(:wish).permit(:customer_id, :destination_id)
  end

end
