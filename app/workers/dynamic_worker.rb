class DynamicWorker
	include Sidekiq::Worker

	def perform

    t1 = Time.now
		puts "============================= JOB STARTED ======================="
    @search_data = AppUtils.compile_search_data

    renderer = HomeController.new
    data = renderer.render_to_string :partial => "dynamic_content", :locals=> {search_data: @search_data }, :layout => false
    data = Uglifier.new.compile(data)
    FileUtils.mkdir_p "public/cache/dynamic/"
    File.open("#{Rails.root}/public/cache/dynamic/dynamic_js_content.js", 'w') {|f| f.write(data) }
		puts "============================= JOB DONE ======================="
    puts "Time taken = #{Time.now - t1}"
	end
end