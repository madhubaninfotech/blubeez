class AddMainImageToDestination < ActiveRecord::Migration
  def change
    add_column :destinations, :main_image, :string
    add_column :hotels, :main_image, :string
    add_column :flights, :main_image, :string
  end
end
