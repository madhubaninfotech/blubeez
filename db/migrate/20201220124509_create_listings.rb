class CreateListings < ActiveRecord::Migration[5.0]
  def change
    create_table :listings do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.string :location
      t.string :service_desc
      t.string :website
      t.boolean :is_owner
      t.text :message
      t.boolean :is_read, default: false

      t.timestamps
    end
  end
end
