class AddAdditionalTopicToTopic < ActiveRecord::Migration
  def change
    add_column :topics, :additional_topic, :integer
  end
end
