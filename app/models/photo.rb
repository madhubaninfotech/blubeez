class Photo < ActiveRecord::Base
  belongs_to :customer
  mount_uploaders :attachments, ImageUploader
  mount_uploader :videos, VideoUploader
  has_one :temp_image, dependent: :destroy
end
