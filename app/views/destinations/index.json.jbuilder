json.array!(@destinations) do |destination|
  json.extract! destination, :id, :destination_name, :destination_country, :destination_cities, :destination_image, :destination_description
  json.url destination_url(destination, format: :json)
end
