lock '3.16.0'

set :use_sudo, false
set :user, 'deploymaster'
set :application, 'Airvtr'
set :repo_url, 'git@bitbucket.org:madhubaninfotech/blubeez.git' # Edit this to match your repository
set :scm, 'git'
set :scm_verbose, true
set :branch, :master
set :ssh_options, {:forward_agent => true}
set :deploy_via, :copy
set :deploy_to, '/home/deploymaster/airvtr'
set :pty, false
set :linked_files, %w{config/database.yml config/application.yml}
set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/uploads}
set :bundle_binstubs, nil
set :keep_releases, 10
set :rvm_type, :user
set :rvm_ruby_version, 'ruby-2.3.1@airvtr' # Edit this if you are using MRI Ruby
set :rails_env, 'production'

set :normalize_asset_timestamps, %{public/images public/javascripts public/stylesheets} #  touch public/images, public/javascripts and public/stylesheets on each deploy
set :puma_rackup, -> { File.join(current_path, 'config.ru') }
set :puma_state, "#{shared_path}/tmp/pids/puma.state"
set :puma_pid, "#{shared_path}/tmp/pids/puma.pid"
set :puma_bind, "unix://#{shared_path}/tmp/sockets/puma.sock"    #accept array for multi-bind
set :puma_conf, "#{shared_path}/puma.rb"
set :puma_access_log, "#{shared_path}/log/puma_error.log"
set :puma_error_log, "#{shared_path}/log/puma_access.log"
set :puma_role, :app
set :puma_env, fetch(:rack_env, fetch(:rails_env, 'production'))
set :puma_threads, [0, 8]
set :puma_workers, 0
set :puma_worker_timeout, nil
set :puma_init_active_record, true
set :puma_preload_app, false

SSHKit.config.command_map[:rake]  = "bundle exec rake"


namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do

      # Your restart mechanism here, for example:
      execute :touch, release_path.join('tmp/restart.txt')
      # run "touch #{current_path}/tmp/restart.txt"
    end
  end
end

# namespace :sidekiq do
#   task :quiet do
#     on roles(:app) do
#       puts capture("pgrep -f 'workers' | xargs kill -USR1")
#     end
#   end
#   task :restart do
#     on roles(:app) do
#       execute :sudo, :initctl, :restart, :workers
#     end
#   end
# end

# after 'deploy:starting', 'sidekiq:quiet'
# after 'deploy:reverted', 'sidekiq:restart'
# after 'deploy:published', 'sidekiq:restart'

# If you wish to use Inspeqtor to monitor Sidekiq
# https://github.com/mperham/inspeqtor/wiki/Deployments
# namespace :inspeqtor do
#   task :start do
#     on roles(:app) do
#       execute :inspeqtorctl, :start, :deploy
#     end
#   end
#   task :finish do
#     on roles(:app) do
#       execute :inspeqtorctl, :finish, :deploy
#     end
#   end
# end

# before 'deploy:starting', 'inspeqtor:start'
# after 'deploy:finished', 'inspeqtor:finish'