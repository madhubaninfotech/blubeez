class ChangeDestinationImageToDestination < ActiveRecord::Migration
  def change
    change_column :destinations, :destination_image, :string, array: true, default: []
    change_column :flights, :image, :string, array: true, default: []
    change_column :hotels, :image, :string, array: true, default: []
  end
end
