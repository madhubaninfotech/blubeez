class AddCategoriesToTravelBlog < ActiveRecord::Migration
  def change
    add_column :travel_blogs, :category, :text, array: true, default: nil, null: true
  end
end
