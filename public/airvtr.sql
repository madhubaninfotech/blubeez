-- MySQL dump 10.13  Distrib 5.5.47, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: airvtr
-- ------------------------------------------------------
-- Server version	5.5.47-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `encrypted_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) NOT NULL DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_admins_on_email` (`email`),
  UNIQUE KEY `index_admins_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,'admin@gmail.com','$2a$10$ks7qFaaJf.0x.VtCa7r9UulJeMBkmbYseCZrIo0GyWHyVoEA1gAwO',NULL,NULL,NULL,18,'2016-05-25 15:14:32','2016-05-25 14:58:58','103.16.70.82','103.16.70.82','2016-04-30 15:43:52','2016-05-25 15:14:32');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attractions`
--

DROP TABLE IF EXISTS `attractions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attractions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `addr_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `addr_street` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `addr_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `destination_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `view_count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_attractions_on_destination_id` (`destination_id`),
  CONSTRAINT `fk_rails_7592e6c31f` FOREIGN KEY (`destination_id`) REFERENCES `destinations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attractions`
--

LOCK TABLES `attractions` WRITE;
/*!40000 ALTER TABLE `attractions` DISABLE KEYS */;
/*!40000 ALTER TABLE `attractions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` text COLLATE utf8_unicode_ci,
  `button_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `button_link` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `index_banners_on_admin_id` (`admin_id`),
  CONSTRAINT `fk_rails_0c13b4a637` FOREIGN KEY (`admin_id`) REFERENCES `admins` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,'banner-1.jpg',1,'2016-04-30 15:44:53','2016-05-25 14:52:42','Get The Best Deals Now','Book your next trip','Book Now','/destinations'),(2,'banner-2.jpg',1,'2016-04-30 15:45:18','2016-05-12 16:23:45','Discount on hotels','visit our partner website to avail offers!','Visit Now','#'),(3,'banner-3.jpg',1,'2016-04-30 15:45:43','2016-05-12 16:28:18','Destinations for you','Explore the destinations you love!','Explore Now','/destinations');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_comments`
--

DROP TABLE IF EXISTS `blog_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `reply_for` int(11) DEFAULT NULL,
  `travel_blog_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_blog_comments_on_travel_blog_id` (`travel_blog_id`),
  KEY `index_blog_comments_on_customer_id` (`customer_id`),
  CONSTRAINT `fk_rails_b7f29f9e5a` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  CONSTRAINT `fk_rails_df11622cf8` FOREIGN KEY (`travel_blog_id`) REFERENCES `travel_blogs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_comments`
--

LOCK TABLES `blog_comments` WRITE;
/*!40000 ALTER TABLE `blog_comments` DISABLE KEYS */;
INSERT INTO `blog_comments` VALUES (1,'SJ Suthakar','Very useful blog..!',1,1,6,'2016-05-25 16:19:56','2016-05-25 16:20:12');
/*!40000 ALTER TABLE `blog_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `encrypted_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) NOT NULL DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_customers_on_email` (`email`),
  UNIQUE KEY `index_customers_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'mkmmslearn@gmail.com','$2a$10$1AX5.fxhWoiC/MkvWLeyX.KiG/SNzh6.QAv14qESGcDYi0z43jhIS',NULL,NULL,NULL,NULL,NULL,12,'2016-05-25 19:39:56','2016-05-22 13:54:24','103.16.70.82','103.16.70.82','2016-04-08 15:52:49','2016-05-25 19:39:56','mohankumar',NULL),(2,'mohankumar.m@ticketsimply.com','$2a$10$8qU2MWi1g6lc/EnHj8a2C.nh2/.fZcnH6WlwGXk6IrYnSMOLux.12',NULL,NULL,NULL,NULL,NULL,1,'2016-04-10 08:07:37','2016-04-10 08:07:37','103.16.70.82','103.16.70.82','2016-04-10 08:07:37','2016-04-10 08:07:37','Mohan',NULL),(3,'b1@b.com','$2a$10$tAJjHbqNI6Z083T65zrQWeiKZ02zytiDBUcB4C/d7IinTTwnNm/ly',NULL,NULL,NULL,NULL,NULL,1,'2016-04-10 08:28:28','2016-04-10 08:28:28','103.16.70.82','103.16.70.82','2016-04-10 08:28:28','2016-04-10 08:28:28','mohankumar',NULL),(4,'change@me-1065800143484586-facebook.com','$2a$10$iwE6FrJVlorkSrRgsB1MjOL2aIt1KFmSn.Cnd2n/33ERaqaGSTod2',NULL,NULL,NULL,NULL,NULL,1,'2016-04-10 12:05:58','2016-04-10 12:05:58','103.16.70.82','103.16.70.82','2016-04-10 12:05:58','2016-04-10 12:05:58','Mohankumar Mutha',NULL),(6,'change@me-1140335819333185-facebook.com','$2a$10$mchtRcquwjOwZHx0v3au0.yZmu6X1H18Y7B3SNPigbl/hkWejFCF.',NULL,NULL,NULL,NULL,NULL,8,'2016-05-26 12:47:10','2016-05-25 15:58:33','43.224.158.246','103.16.70.82','2016-04-11 14:55:51','2016-05-26 12:47:10','SJ Suthakar',NULL),(7,'change@me-890408485-twitter.com','$2a$10$3S46Xf9uKU39YLw18HT5UeAVdjC9BTKFqXyHZUsGjUfG6zsItf77m',NULL,NULL,NULL,NULL,NULL,4,'2016-05-22 13:38:46','2016-04-11 16:11:09','103.16.70.82','103.16.70.82','2016-04-11 15:06:38','2016-05-22 13:38:46','Süthäkär',NULL),(8,'ushasrd09@gmail.com','$2a$10$Xi4z34khcJTJtkxi/edRMuNdeXp1NSynjp1b02CtVoyEm8TdiGgqC',NULL,NULL,NULL,NULL,NULL,15,'2016-05-24 11:58:55','2016-05-04 11:45:11','73.175.145.171','73.175.145.171','2016-04-11 16:00:48','2016-05-24 11:58:55','ushagv',NULL),(9,'change@me-3148901324-twitter.com','$2a$10$MPJMpgYvLzHhgtJTw7MWsuVrjQekuJCAYP6z3Wn5xdEOJYrAGjCVu',NULL,NULL,NULL,NULL,NULL,1,'2016-04-28 18:51:22','2016-04-28 18:51:22','103.16.70.82','103.16.70.82','2016-04-28 18:51:22','2016-04-28 18:51:22','Mohankumar',NULL),(10,'mohankumar.m@bitlasoft.com','$2a$10$Dg/W2zM1EzFUlTbaOBnD.uSKdbDqFEE2Cv24TsKVNN1f8Qm50Wgmy',NULL,NULL,NULL,NULL,NULL,1,'2016-04-30 15:47:34','2016-04-30 15:47:34','103.16.70.82','103.16.70.82','2016-04-30 15:47:34','2016-04-30 15:47:34','Mohan kumar Murugesan',1),(11,'a21@a.com','$2a$10$mmTsv/VgEQlbyYfPmQVGvO5UQlcFuTt/yGD2EIz55.Dy4sGYFyJVO',NULL,NULL,NULL,NULL,NULL,1,'2016-04-30 16:40:06','2016-04-30 16:40:06','103.16.70.82','103.16.70.82','2016-04-30 16:40:06','2016-04-30 16:40:06','Mohan kumar Murugesan',1),(12,'b@b.com','$2a$10$QfOdxK3t2CBOzjKOHyiO5ewyM3C4hIeRPFrx5ppUY/QahWuucvQWm',NULL,NULL,NULL,NULL,NULL,1,'2016-05-02 19:20:12','2016-05-02 19:20:12','103.16.70.82','103.16.70.82','2016-05-02 19:20:12','2016-05-02 19:20:12','Mohan',1),(13,'r@r.com','$2a$10$349i.MulS20Hyza564Wma.3Ne.k.7RwuGKeZynMXyBFA0KLGP9kKq',NULL,NULL,NULL,NULL,NULL,2,'2016-05-02 19:43:26','2016-05-02 19:21:14','223.227.8.174','103.16.70.82','2016-05-02 19:21:14','2016-05-02 19:43:26','Ravikumar',1),(14,'suthakar.vks@gmail.com','$2a$10$Y6fW20ClM5cJbqYvndC3de4IbPVuqGV0rID0wIrerMrLazfOOTvfG',NULL,NULL,NULL,NULL,NULL,1,'2016-05-22 05:34:42','2016-05-22 05:34:42','103.16.70.82','103.16.70.82','2016-05-22 05:34:42','2016-05-22 05:34:42','Suthakar ',1),(15,'smilesuthakar@gmail.com','$2a$10$uUnHLWNDKVaAaZ3SOKdjXeDNRw5XnGiRAdmI9mDNN0CuTreh9Gx/q',NULL,NULL,NULL,NULL,NULL,1,'2016-05-25 15:53:02','2016-05-25 15:53:02','103.16.70.82','103.16.70.82','2016-05-25 15:53:02','2016-05-25 15:53:02','Suthakar Raja',1);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `destinations`
--

DROP TABLE IF EXISTS `destinations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `destinations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `destination_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `destination_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `destination_cities` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `destination_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `destination_category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_from` float DEFAULT NULL,
  `destination_description` text COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `best_for` int(11) DEFAULT NULL,
  `view_count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `destinations`
--

LOCK TABLES `destinations` WRITE;
/*!40000 ALTER TABLE `destinations` DISABLE KEYS */;
INSERT INTO `destinations` VALUES (12,'Bled','SI','003','Bled','Bled.jpg','5',NULL,'Bled itself, then part of the March of Carniola, was first mentioned as Ueldes (Veldes) in a document from 1004, in which German King Henry II ceded ownership of the area to Albuin, Bishop of Brixen as a sign of gratitude for the assistance the Church was giving to the king in his attempt to strengthen imperial rule in that part of northern Italy. In 1011, Henry II signed another deed of donation that added the castle and an area of land the size of thirty king’s farms. That area, between the Sava Bohinjka and the Sava Dolinka, became known as the Lordship of Veldes (German: Herrschaft Veldes). These donations marked a turning point in the history of Bled and, for the following 800 years, the area remained under the sovereignty of the prince-bishops of Brixen.\r\n\r\nThe bishops very seldom visited their remote possession 300 km away. Initially, the lordship was administered by ministeriales (bonded knights), castellans, and castle staff in accordance with feudal practices, but in the middle of the 14th century the prince-bishops decided instead to lease the estate. Under one of the 16th-century lessees, Bled Castle became a Protestant stronghold for a time. When the leasehold era came to an end, the prince-bishops began to appoint governors to manage their distant lordship. Until the middle of the 18th century, those administrators were exclusively aristocratic, but later they included non-nobles.','2016-05-22 09:04:08','2016-05-28 06:43:41',2,2),(13,'Colombo','LK','1','Colombo','Colombo.jpg','3',NULL,'Like many cities, Colombo\'s urban area extends well beyond the boundaries of a single local authority, encompassing other municipal and urban councils such as Sri Jayawardenepura Kotte Municipal Council, Dehiwala Mount Lavinia Municipal Council, Kolonnawa Urban Council, Kaduwela Municipal Council and Kotikawatte Mulleriyawa Pradeshiya Sabha. The main city is home to a majority of Sri Lanka\'s corporate offices, restaurants and entertainment venues. Famous landmarks in Colombo include Galle Face Green, Viharamahadevi Park, Beira Lake, Colombo Racecourse, Planetarium, University of Colombo, Mount Lavinia beach, Nelum Pokuna Theatre, Colombo Lotus Tower(is being constructed) as well as the National Museum.','2016-05-22 09:28:24','2016-05-28 06:43:41',1,1),(14,'Ubud','ID','BA','Ubud','Ubud.jpg','5',NULL,'The town of Ubud, in the uplands of Bali, Indonesia, is known as a center for traditional crafts and dance. The surrounding Ubud District’s rainforest and terraced rice paddies, dotted with Hindu temples and shrines, are among Bali’s most iconic landscapes. Ancient holy sites include the Tirta Empul temple complex, intricately carved Goa Gajah (“Elephant Cave”) and Gunung Kawi with its rock-cut shrines.','2016-05-22 09:33:17','2016-05-22 09:33:17',4,0),(15,'Tirana','AL','11','Tirana','Tirana.jpg','3',NULL,'The area occupied by Tirana has been populated since the Paleolithic era, dating back 10,000 to 30,000 years ago, as suggested by evidence from tools excavated near Mount Dajt\'s quarry and in Pellumba Cave. As argued by various archaeologists, Tirana and its suburbs are filled with Illyrian toponyms, as its precincts are some of the earliest inhabited regions in Albania. The Illyrians called the settlement Tërana.\r\n\r\nThe oldest discovery in downtown Tirana was a Roman house, later transformed into an aisleless church with a mosaic-floor, dating to the 3rd century A.D., with other remains found near a medieval temple at Shengjin Fountain in the eastern suburbs. A castle possibly called Tirkan or Theranda, whose remnants are found along Murat Toptani Street, was built by Emperor Justinian in 520 A.D. and restored by Ahmed Pasha Toptani in the 18th century. The area had no special importance in Illyrian and classical times. In 1510, Marin Barleti, an Albanian Catholic priest and scholar, in the biography of the Albanian national hero Skanderbeg, Historia de vita et gestis Scanderbegi Epirotarum principis (The story of life and deeds of Skanderbeg, the prince of Epirotes), referred to this area as a small village.','2016-05-22 09:39:38','2016-05-28 06:44:12',3,1),(16,'Hong Kong','HK','KKT','Cha Kwo Ling','Hong-kong.jpg','3',NULL,'Hong Kong is a city, and former British colony, in southeastern China. Vibrant and densely populated, it’s a major port and global financial center famed for its tower-studded skyline. It’s also known for its lively food scene – from Cantonese dim sum to extravagant high tea – and its shopping, with options spanning chaotic Temple Street Night Market to the city’s innumerable bespoke tailors.','2016-05-22 09:46:40','2016-05-28 06:44:12',1,1),(17,'Ha Long Bay','VN','68','Ha Long','Ha-Long-Bay.jpg','5',NULL,'Hạ Long Bay, in northeast Vietnam, is known for its emerald waters and thousands of towering limestone islands topped by rainforests. Junk boat tours and sea kayak expeditions take visitors past islands named for their shapes, including Stone Dog and Teapot islands. The region draws scuba divers, rock climbers and hikers, the latter favoring mountainous Cát Bà National Park.','2016-05-22 09:54:59','2016-05-28 06:44:16',1,1),(18,'Bora Bora','PF','V','','Bora-Bora.jpg','6',NULL,'Bora Bora is a small South Pacific island northwest of Tahiti in French Polynesia. Surrounded by sand-fringed motus (islets) and a turquoise lagoon protected by a coral reef, it’s known for its scuba diving. It\'s also a popular luxury resort destination where some guest bungalows are perched over the water on stilts. At the island\'s center rises 727m Mt. Otemanu, a dormant volcano.','2016-05-22 09:58:41','2016-05-28 18:03:49',2,3),(19,'Jaipur','IN','RJ','Jaipur','Jaipur.jpg','3',NULL,'Jaipur, the capital of India’s Rajasthan state, evokes the royal family that once ruled the region and that, in 1727, founded what is now called the Old City, or “Pink City” for its trademark building color. At the center of its stately street grid (notable in India) stands the opulent, collonaded City Palace complex, which today houses several museum collections of textiles and art.','2016-05-22 10:03:52','2016-05-28 06:44:17',3,1);
/*!40000 ALTER TABLE `destinations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` text COLLATE utf8_unicode_ci,
  `detail` text COLLATE utf8_unicode_ci,
  `date` datetime DEFAULT NULL,
  `destination_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `view_count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_events_on_destination_id` (`destination_id`),
  CONSTRAINT `fk_rails_a11fa9ff7d` FOREIGN KEY (`destination_id`) REFERENCES `destinations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flight_facilities`
--

DROP TABLE IF EXISTS `flight_facilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flight_facilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `checkin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `checkout` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cancelorprepay` text COLLATE utf8_unicode_ci,
  `childrenbeds` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pets` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `groups` text COLLATE utf8_unicode_ci,
  `cardsaccepted` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flight_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_flight_facilities_on_flight_id` (`flight_id`),
  CONSTRAINT `fk_rails_4be5ccf240` FOREIGN KEY (`flight_id`) REFERENCES `flights` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flight_facilities`
--

LOCK TABLES `flight_facilities` WRITE;
/*!40000 ALTER TABLE `flight_facilities` DISABLE KEYS */;
/*!40000 ALTER TABLE `flight_facilities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flights`
--

DROP TABLE IF EXISTS `flights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `destination_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `highlights` varchar(255) COLLATE utf8_unicode_ci DEFAULT '--- []\n',
  `view_count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_flights_on_destination_id` (`destination_id`),
  CONSTRAINT `fk_rails_17532769c7` FOREIGN KEY (`destination_id`) REFERENCES `destinations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flights`
--

LOCK TABLES `flights` WRITE;
/*!40000 ALTER TABLE `flights` DISABLE KEYS */;
/*!40000 ALTER TABLE `flights` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `friends`
--

DROP TABLE IF EXISTS `friends`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `friends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `friend_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_friends_on_customer_id` (`customer_id`),
  CONSTRAINT `fk_rails_529fc5a74f` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `friends`
--

LOCK TABLES `friends` WRITE;
/*!40000 ALTER TABLE `friends` DISABLE KEYS */;
INSERT INTO `friends` VALUES (1,'mohankumar',1,6,'2016-04-11 14:59:29','2016-04-11 14:59:29'),(2,'mohankumar',1,7,'2016-04-11 16:12:02','2016-04-11 16:12:02'),(3,'Mohan',2,7,'2016-04-11 16:12:10','2016-04-11 16:12:10'),(4,'mohankumar',3,7,'2016-04-11 16:12:12','2016-04-11 16:12:12'),(5,'SJ Suthakar',6,7,'2016-04-11 16:12:13','2016-04-11 16:12:13'),(6,'ushagv',8,7,'2016-04-11 16:12:14','2016-04-11 16:12:14'),(7,'SJ Suthakar',6,7,'2016-04-11 16:12:15','2016-04-11 16:12:15'),(8,'Mohan',2,7,'2016-04-11 16:12:18','2016-04-11 16:12:18'),(9,'mohankumar',1,7,'2016-04-11 16:12:19','2016-04-11 16:12:19'),(10,'mohankumar',1,7,'2016-04-11 16:12:43','2016-04-11 16:12:43'),(11,'Mohan',2,8,'2016-04-11 16:18:11','2016-04-11 16:18:11'),(12,'Mohan',2,8,'2016-04-11 16:18:17','2016-04-11 16:18:17'),(13,'ushagv',8,6,'2016-04-25 17:52:29','2016-04-25 17:52:29'),(14,'ushagv',8,6,'2016-04-25 17:52:31','2016-04-25 17:52:31'),(15,'Süthäkär',7,6,'2016-04-25 17:52:34','2016-04-25 17:52:34'),(16,'mohankumar',3,6,'2016-04-25 17:52:35','2016-04-25 17:52:35'),(17,'Mohan',2,6,'2016-04-25 17:52:36','2016-04-25 17:52:36');
/*!40000 ALTER TABLE `friends` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hotel_facilities`
--

DROP TABLE IF EXISTS `hotel_facilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel_facilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `view` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activities` text COLLATE utf8_unicode_ci,
  `mediatechnology` text COLLATE utf8_unicode_ci,
  `food` text COLLATE utf8_unicode_ci,
  `internet` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parking` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `services` text COLLATE utf8_unicode_ci,
  `general` text COLLATE utf8_unicode_ci,
  `languages` text COLLATE utf8_unicode_ci,
  `hotel_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_hotel_facilities_on_hotel_id` (`hotel_id`),
  CONSTRAINT `fk_rails_5858e89acf` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotel_facilities`
--

LOCK TABLES `hotel_facilities` WRITE;
/*!40000 ALTER TABLE `hotel_facilities` DISABLE KEYS */;
/*!40000 ALTER TABLE `hotel_facilities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hotels`
--

DROP TABLE IF EXISTS `hotels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_from` text COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `amenities` varchar(255) COLLATE utf8_unicode_ci DEFAULT '--- []\n',
  `view_count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_hotels_on_destination_id` (`destination_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotels`
--

LOCK TABLES `hotels` WRITE;
/*!40000 ALTER TABLE `hotels` DISABLE KEYS */;
/*!40000 ALTER TABLE `hotels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `identities`
--

DROP TABLE IF EXISTS `identities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `identities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_identities_on_customer_id` (`customer_id`),
  CONSTRAINT `fk_rails_db5b4231e9` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `identities`
--

LOCK TABLES `identities` WRITE;
/*!40000 ALTER TABLE `identities` DISABLE KEYS */;
INSERT INTO `identities` VALUES (1,'facebook','1065800143484586',4,'2016-04-10 12:05:58','2016-04-10 12:05:58'),(3,'facebook','1140335819333185',6,'2016-04-11 14:55:51','2016-04-11 14:55:51'),(4,'twitter','890408485',7,'2016-04-11 15:06:37','2016-04-11 15:06:38'),(5,'twitter','3148901324',9,'2016-04-28 18:51:22','2016-04-28 18:51:22');
/*!40000 ALTER TABLE `identities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_customer` int(11) DEFAULT NULL,
  `to_customer` varchar(255) COLLATE utf8_unicode_ci DEFAULT '--- []\n',
  `note` text COLLATE utf8_unicode_ci,
  `notify_type` int(11) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_notifications_on_customer_id` (`customer_id`),
  CONSTRAINT `fk_rails_e82fd73b00` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photos`
--

DROP TABLE IF EXISTS `photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trip_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trip_description` text COLLATE utf8_unicode_ci,
  `attachments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `videos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_photos_on_customer_id` (`customer_id`),
  CONSTRAINT `fk_rails_7eebac098e` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photos`
--

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `d_o_b` datetime DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about_user` text COLLATE utf8_unicode_ci,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `age` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_profiles_on_customer_id` (`customer_id`),
  CONSTRAINT `fk_rails_f3a6465ce5` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profiles`
--

LOCK TABLES `profiles` WRITE;
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
INSERT INTO `profiles` VALUES (1,'mohankumar','mkmmslearn@gmail.com','1990-04-17 00:00:00','i2.jpg','I am Mohan','Bangalore, KA, IN','1',1,'2016-04-08 15:52:49','2016-04-10 07:54:13',NULL),(2,'Mohan','mohankumar.m@ticketsimply.com',NULL,NULL,NULL,NULL,NULL,2,'2016-04-10 08:07:37','2016-04-10 08:07:37',NULL),(3,'mohankumar','b1@b.com',NULL,NULL,NULL,NULL,NULL,3,'2016-04-10 08:28:28','2016-04-10 08:28:28',NULL),(4,'Suthakar','suthakar.vks@gmail.com','1999-04-21 00:00:00','noavatar.png','I am Suthakar','Bangalore, KA, IN','1',6,'2016-04-11 15:05:16','2016-04-11 15:05:16',NULL),(5,'Suthakar','suthakar.vks@gmail.com','1992-01-02 00:00:00','303747_508316219201818_1288070419_n.jpg','I am a creative web designer and developer.','Bangalore, KA, IN','1',7,'2016-04-11 15:24:41','2016-05-22 13:51:28',24),(6,'ushagv','ushasrd09@gmail.com',NULL,NULL,NULL,NULL,NULL,8,'2016-04-11 16:00:48','2016-04-11 16:00:48',NULL),(7,'Mohan kumar Murugesan','mohankumar.m@bitlasoft.com',NULL,NULL,NULL,NULL,NULL,10,'2016-04-30 15:47:34','2016-04-30 15:47:34',NULL),(8,'Mohan kumar Murugesan','a21@a.com',NULL,NULL,NULL,NULL,NULL,11,'2016-04-30 16:40:06','2016-04-30 16:40:06',NULL),(9,'Mohan','b@b.com',NULL,NULL,NULL,NULL,NULL,12,'2016-05-02 19:20:12','2016-05-02 19:20:12',NULL),(10,'Ravikumar','r@r.com',NULL,NULL,NULL,NULL,NULL,13,'2016-05-02 19:21:14','2016-05-02 19:21:14',NULL),(11,'Suthakar ','suthakar.vks@gmail.com',NULL,NULL,NULL,NULL,NULL,14,'2016-05-22 05:34:42','2016-05-22 05:34:42',NULL),(12,'Suthakar Raja','smilesuthakar@gmail.com',NULL,NULL,NULL,NULL,NULL,15,'2016-05-25 15:53:02','2016-05-25 15:53:02',NULL);
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ratings`
--

DROP TABLE IF EXISTS `ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flight_id` int(11) DEFAULT NULL,
  `hotel_id` int(11) DEFAULT NULL,
  `destination_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_ratings_on_flight_id` (`flight_id`),
  KEY `index_ratings_on_hotel_id` (`hotel_id`),
  KEY `index_ratings_on_destination_id` (`destination_id`),
  KEY `index_ratings_on_customer_id` (`customer_id`),
  CONSTRAINT `fk_rails_29ded712aa` FOREIGN KEY (`flight_id`) REFERENCES `flights` (`id`),
  CONSTRAINT `fk_rails_526461a96a` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`),
  CONSTRAINT `fk_rails_64a8aa0ef7` FOREIGN KEY (`destination_id`) REFERENCES `destinations` (`id`),
  CONSTRAINT `fk_rails_fcd35895f8` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ratings`
--

LOCK TABLES `ratings` WRITE;
/*!40000 ALTER TABLE `ratings` DISABLE KEYS */;
INSERT INTO `ratings` VALUES (3,NULL,NULL,12,6,5,'2016-05-22 09:04:09','2016-05-22 09:04:09'),(4,NULL,NULL,13,6,5,'2016-05-22 09:28:24','2016-05-22 09:28:24'),(5,NULL,NULL,14,6,5,'2016-05-22 09:33:18','2016-05-22 09:33:18'),(6,NULL,NULL,15,6,5,'2016-05-22 09:39:38','2016-05-22 09:39:38'),(7,NULL,NULL,16,6,5,'2016-05-22 09:46:40','2016-05-22 09:46:40'),(8,NULL,NULL,17,6,5,'2016-05-22 09:55:00','2016-05-22 09:55:00'),(9,NULL,NULL,18,6,5,'2016-05-22 09:58:42','2016-05-22 09:58:42'),(10,NULL,NULL,19,6,5,'2016-05-22 10:03:52','2016-05-22 10:03:52'),(11,NULL,NULL,13,7,5,'2016-05-22 13:40:57','2016-05-22 13:40:57');
/*!40000 ALTER TABLE `ratings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `customer_id` int(11) DEFAULT NULL,
  `hotel_id` int(11) DEFAULT NULL,
  `flight_id` int(11) DEFAULT NULL,
  `destination_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_reviews_on_customer_id` (`customer_id`),
  KEY `index_reviews_on_hotel_id` (`hotel_id`),
  KEY `index_reviews_on_flight_id` (`flight_id`),
  KEY `index_reviews_on_destination_id` (`destination_id`),
  KEY `index_reviews_on_event_id` (`event_id`),
  CONSTRAINT `fk_rails_5bbf97fba9` FOREIGN KEY (`flight_id`) REFERENCES `flights` (`id`),
  CONSTRAINT `fk_rails_6429803e23` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`),
  CONSTRAINT `fk_rails_74e2428a18` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  CONSTRAINT `fk_rails_8133a103a2` FOREIGN KEY (`destination_id`) REFERENCES `destinations` (`id`),
  CONSTRAINT `fk_rails_b9f3d67017` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (1,'Suthakar','This is very good place for family outing...!',7,NULL,NULL,13,NULL,'2016-05-22 13:41:42','2016-05-22 13:41:42');
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20160310164555'),('20160310164605'),('20160311160454'),('20160311161225'),('20160314190818'),('20160315172258'),('20160316174236'),('20160317185029'),('20160318181427'),('20160319051737'),('20160319060122'),('20160321152843'),('20160321154720'),('20160323155020'),('20160323184749'),('20160419182606'),('20160423092842'),('20160423144457'),('20160424100022'),('20160428173453'),('20160507092018'),('20160507142832'),('20160507185731'),('20160508125753'),('20160508131707'),('20160509165540'),('20160510174923'),('20160510175141'),('20160516160424'),('20160525182905'),('20160525182906'),('20160525182907'),('20160525182908'),('20160525182909'),('20160526174449');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `var` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  `thing_id` int(11) DEFAULT NULL,
  `thing_type` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_settings_on_thing_type_and_thing_id_and_var` (`thing_type`,`thing_id`,`var`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taggings`
--

DROP TABLE IF EXISTS `taggings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taggings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) DEFAULT NULL,
  `taggable_id` int(11) DEFAULT NULL,
  `taggable_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tagger_id` int(11) DEFAULT NULL,
  `tagger_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `context` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `taggings_idx` (`tag_id`,`taggable_id`,`taggable_type`,`context`,`tagger_id`,`tagger_type`),
  KEY `index_taggings_on_taggable_id_and_taggable_type_and_context` (`taggable_id`,`taggable_type`,`context`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taggings`
--

LOCK TABLES `taggings` WRITE;
/*!40000 ALTER TABLE `taggings` DISABLE KEYS */;
/*!40000 ALTER TABLE `taggings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `taggings_count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_tags_on_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `travel_blogs`
--

DROP TABLE IF EXISTS `travel_blogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `travel_blogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `customer_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `view_count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_travel_blogs_on_customer_id` (`customer_id`),
  CONSTRAINT `fk_rails_474808db8c` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `travel_blogs`
--

LOCK TABLES `travel_blogs` WRITE;
/*!40000 ALTER TABLE `travel_blogs` DISABLE KEYS */;
INSERT INTO `travel_blogs` VALUES (1,'Mesmerising Slovenia','<b>Mesmerising Slovenia<b> \r\nIn Slovenia, one is excused if caught gaping unabashedly at a beautiful mountainside scene or found embarrassingly speechless on seeing the sights of a valley. It is quite normal here and you would just invite an indulgent smile. After all, not too many places are left on earth with this kind of hypnotic beauty. \r\nSlovenia is the land of fables with its picturesque lakes, castles seemingly lifted from the pages of a fairy tale book and even legendary dragons. It may be heaven on earth for its perfect vistas of snow capped mountain, rustic valleys and greenery. Slovenia is located in the pulsating part of Europe, and is bordered by the Adriatic Sea. The country is essentially the place to go if you love nature at her best – there are the snow capped Alps to start with, beautiful valleys with rivers snaking through them and the charming Adriatic coastline. In this mesmerising land, there are mountains waiting to be hiked on, vineyards aplenty to sample local produce, rivers that invite you to raft and when you are tired, eat to your heart’s content from the country’s most sophisticated, yet rustic food. \r\nSome awe inspiring statistics about the country: 7000 kilometres of mountain hiking trails, more than 5 different food festivals (ever heard of the Cabbage festival?!), Slovenia is the third most forested European country and is astoundingly home to more 24,000 species of animals! \r\nNeed more reason to visit Slovenia? Here we go... \r\nOf Capital Cities: Ljubljana\r\nThe largest city in Slovenia, Ljubljana is the capital of the country. Being the capital of the country may be serious business, but this city has retained its castles, culture and most importantly charm. It tops the list among Europe’s greenest and most liveable capitals. The city has the river Ljubljana flowing beside it, reflecting the green of the trees on its banks. Feel like you are in Venice, as you take a romantic boat ride along the river.  Ljubljana is where the mythical Greek warrior Jason (from Jason and the Argonauts) killed a dangerous dragon, that later became the city’s lovable symbol on the famous Dragon bridge (Slovene: Zmajski most). If you are travelling to Ljubljana between March and October, visit the Central Market’s street food open kitchen, a haven for food buffs in search of Slovenian cuisine. Start your Slovenian tour with the capital city’s very own castle, called by the same name of course. The 900 year old castle is a study in history, but more importantly offers the best views of the city from its towers. Ljubljana has a culture of outdoor entertainment and street side cafes; explore these as you watch the city pass by. The city has a vivid nightlife too with many clubs and bars, and its own microbreweries. \r\nMust Do: Ljubljana is a cyclist friendly one, cycling tours often take one along the river banks through the city’s landmarks. Visit the ‘Love Bridge’ weirdly called the Butchers Bridge, where hang many a couple’s padlocks of love. \r\nWine County: Maribor\r\nIt may be the second largest city in Slovenia, but is not the typical bustling large town. Maribor is a cosy city that oozes old-town charm. The hills here are famous for its vineyards and maybe a reason why Maribor is so welcoming to tourists! While you are looking in on the world’s largest and oldest wine cellar in Europe, the marvellous views of the country side vie for your attention on the hills. There are many home stays on these slopes that offer one a home cooked meal, with wine on the side. The River Drava winds through Maribor, perfect for a casual evening stroll. The mount Pohorje in winter turns into Slovenia’s most touted skiing slopes. \r\nOf Adventure and history: Soca Valley \r\nMuseums, Waterfalls, fortresses, gorges, rivers and even a cheese farm – Soca Valley is one of the best adventure holiday destinations in Slovenia! The Soca River is where the adventure starts – white water rafting on these blue waters is a must. The valley has canyons and if you want a birds-eye view, then zip-lining across is for you. The Tolmin Gorges and the Slap Kozjak waterfall is an easy hike away. Paragliding, bungee jumping and more are available here. Before you leave, visit an organic dairy farm and take home some Slovenian cheese. \r\nCastle Country \r\nYou could say that for most of Slovenia, but more so for Bled; this lake-resort town is a dream, a place where evil cannot exist and maybe where the fairies reside! The town is centred around the glacial lake Bled and the 11th century Castle Bled situated on an island in Lake Bled. The lake that is a stunning blue turns ethereal orange as the sun sets picturesquely over the towers of the Castle Bled. Take a boat ride into the island to visit the castle and have a look around medieval history. The river Radovna flows near the town and has carved the remarkable Bled Gorge, a favourite among visitors for its waterfalls and rapids. \r\nA rather unique and mysterious looking castle, the Predjama Castle in the Karst region is set on a 123 metre steep and tough looking cliff. The castle was home to the medieval Robin Hood of Slovenia, the knight Erazem. 700 years after the legendary Erazem, the castle is still preserved in its near-original state. \r\nMust Do: Don’t miss the puppet museum at the Bled castle, Slovenia is country known for its puppet making history. And at Predjama Castle, adventure awaits those who dare take the Erazem’s tunnel!\r\nAlong the Adriatic Coast \r\nPiran is the south-western triangle of Slovenia that drops into the Adriatic. If you look with an eagle eye (and binoculars), Italy lies opposite to the tip of Piran. Not surprisingly, Italian was one of the languages spoken popularly in Piran. The rustic allure of the town, its lazy seaside ambience is probably what makes Piran a popular destination. They say walls have ears, Piran’s walls have seen much history being a seaside town, and were built to prevent attacks from the Ottomans. Visit Tartini Square, named so in memory of the musician Giuseppe Tartini; set in the middle of red roofed buildings this is where one can absorb the essence of the seaside town.\r\nKoper city is more Mediterranean than its other coastal counterparts in Slovenia. The city is an important port in Slovenia and is connected to the capital Ljubljana by rail and also to Italy. The city is busy and may even have an all-important air about it, but its historic past and attractions are many. Take for example the 15th century Praetorian Palace in the city square that was originally two 13th century houses which when built over became the present Venetian style palace! Koper’s entertainment is by the sea, in its many sea-side cafe’s are where one can stretch the legs or its clean beaches for that perfect sun tan.\r\nFrom Koper, it is easy to visit the Skocjan Caves, a UNESCO world heritage site. The underground river Reka winds its way creating the phenomenal 6 km Skocjan cave system. It can leave you wondering about the power of water that can create such miraculous wonders .The River empties out into the Dead Lake, after which it disappears again to resurface in Italy! A must visit also because the world’s largest underground canyon is located here.\r\nPortoroz translates in Italian to ‘Port of Roses’, but is less of roses and more of sea. It has been a much visited sea-side resort and has been part of the famous Austrian Riviera. Once a health resort town, Portoroz is now a seaside entertainment town, maybe the Las Vegas of Slovenia with its many Casinos. For the adrenalin junkies, sailing, kayaking and even scuba diving are a rage in Portoroz. \r\nToo beautiful to inspire murder\r\nSo said novelist Agatha Christie of the captivating beauty of the Alpine valleys of Slovenia. Slovenia is a fairy tale come true and the perfect holiday destination, one where you wouldn’t mind getting lost in! \r\n',7,'2016-04-11 15:49:16','2016-05-29 05:27:41','ed.jpg',2),(2,'The new blog','This is blog bala snka sndka nsdnkj amsnkbad alskdmlask dnkasd lkamsldk amlskd lakmsldkamlskd mlaksmdlk amslmdml',6,'2016-04-25 17:19:48','2016-05-28 06:46:32','1440_900_20100710092341558181.jpg',1),(3,'THis is also some test blog','This is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blogThis is the blog',6,'2016-04-25 17:20:41','2016-05-28 06:46:26','1531621.jpg',1),(4,'Trip to Saudhi','sadjhsad akjsdbmn kjbasd jkbsad h asdnbkasd jkabsd kjbasd jbasdn kj asdj askd  jaksbd ',NULL,'2016-05-03 18:36:40','2016-05-28 13:29:44','s1.jpg',4);
/*!40000 ALTER TABLE `travel_blogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trips`
--

DROP TABLE IF EXISTS `trips`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trip_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trip_description` text COLLATE utf8_unicode_ci,
  `trip_photos` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `trip_status` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `view_count` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_trips_on_customer_id` (`customer_id`),
  CONSTRAINT `fk_rails_2755afd336` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trips`
--

LOCK TABLES `trips` WRITE;
/*!40000 ALTER TABLE `trips` DISABLE KEYS */;
INSERT INTO `trips` VALUES (1,'Kodaikanal','Kodaikanal, TN, IN','\r\nKodaikanal is very cool area compared to other states in tamilnadu.','ed.jpg','2016-04-04 00:00:00','2016-04-05 00:00:00',NULL,7,'2016-04-11 16:06:25','2016-04-11 16:06:25',0),(2,'',', BI, BS','','493.JPG',NULL,NULL,NULL,8,'2016-04-11 16:16:56','2016-04-11 16:16:56',0),(3,'Chennai','Erode, TN, IN','Thisis nicee rip Thisis nicee rip Thisis nicee rip Thisis nicee rip Thisis nicee rip ','birds_eye_view_layout_plan.jpg','2016-04-03 00:00:00','2016-04-05 00:00:00',NULL,6,'2016-04-25 17:12:56','2016-04-27 17:40:51',0),(4,'Kodaikanal','Bangalore, KA, IN','UHBafj adjnaksd aoksdmokas doaksd oaksm dokasndjas dijasndij ansidj naisdjnaisjd najsnd  asnd','entrance_arch.jpg','2016-04-03 00:00:00','2016-04-20 00:00:00',NULL,6,'2016-04-25 17:17:52','2016-04-25 17:17:52',0);
/*!40000 ALTER TABLE `trips` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-29  2:20:28
