class ReviewsController < ApplicationController
  before_action :set_review, only: [:show, :edit, :update, :destroy, :vote]
  # before_action :authenticate_customer!, except: [:index, :show]

  # GET /reviews
  # GET /reviews.json
  def index
    @reviews = Review.all
  end

  # GET /reviews/1
  # GET /reviews/1.json
  def show
  end

  # GET /reviews/new
  def new
    @review = Review.new
  end

  # GET /reviews/1/edit
  def edit
    @rating = @review.get_rating_by_type_of_rating(@review).first
  end

  # POST /reviews
  # POST /reviews.json
  def create
    @review = Review.new(review_params)

    if @review.save
      flash.now[:notice] = 'Review was successfully created.'
      @rating = @review.get_rating_by_type_of_rating(@review).first
    else
      flash.now[:error] =  @review.errors
    end
  end

  def vote
    @review.voter_id = @review.voter_id ||= ""
    reviewers_arr = @review.voter_id.split(",")
    reviewers_arr.uniq!
    @is_voted = false
    if customer_signed_in?
      unless reviewers_arr.include?(current_customer.id.to_s)
        reviewers_arr.push(current_customer.id.to_i)
        @review.vote += 1
        @is_voted = true
      else
        reviewers_arr.delete(current_customer.id.to_s)
        @review.vote -= 1
        @is_voted = false
      end
      @review.voter_id = reviewers_arr.join(",")
      @review.save
    end
  end

  # PATCH/PUT /reviews/1
  # PATCH/PUT /reviews/1.json
  def update
    if @review.update(review_params)
      flash.now[:notice] = 'Review was successfully updated.'
      @rating = @review.get_rating_by_type_of_rating(@review).first
    else
      flash.now[:error] =  @review.errors
    end
  end

  # DELETE /reviews/1
  # DELETE /reviews/1.json
  def destroy
    @review.destroy
    if @review.images.present? && JSON.parse(@review.images).present?
      imgs = Image.where(:id=> JSON.parse(@review.images))
      imgs.destroy_all
    end
    respond_to do |format|
      format.html { redirect_to request.referer, notice: 'Review was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_review
      @review = Review.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def review_params
      params.require(:review).permit(:name, :comment, :customer_id, :hotel_id, :flight_id, :destination_id, :event_id, :tip, :images, :vote, :voter_id => [])
    end
end
