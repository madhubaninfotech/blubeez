module FlightsHelper

	def destination_name_from_flight
		dest = Destination.find(@flight.destination_id)
    dest.destination_name
	end

	def destination_id_from_name
		dest = Destination.where(destination_name: params["destination_name"]).first
    dest.id if dest.present?
	end

end
