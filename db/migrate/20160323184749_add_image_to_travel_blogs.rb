class AddImageToTravelBlogs < ActiveRecord::Migration
  def change
    add_column :travel_blogs, :image, :string
  end
end
