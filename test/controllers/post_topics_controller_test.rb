require 'test_helper'

class PostTopicsControllerTest < ActionController::TestCase
  setup do
    @post_topic = post_topics(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:post_topics)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create post_topic" do
    assert_difference('PostTopic.count') do
      post :create, post_topic: { closed: @post_topic.closed, content: @post_topic.content, safe: @post_topic.safe, title: @post_topic.title, topic_id_id: @post_topic.topic_id_id, trusted: @post_topic.trusted }
    end

    assert_redirected_to post_topic_path(assigns(:post_topic))
  end

  test "should show post_topic" do
    get :show, id: @post_topic
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @post_topic
    assert_response :success
  end

  test "should update post_topic" do
    patch :update, id: @post_topic, post_topic: { closed: @post_topic.closed, content: @post_topic.content, safe: @post_topic.safe, title: @post_topic.title, topic_id_id: @post_topic.topic_id_id, trusted: @post_topic.trusted }
    assert_redirected_to post_topic_path(assigns(:post_topic))
  end

  test "should destroy post_topic" do
    assert_difference('PostTopic.count', -1) do
      delete :destroy, id: @post_topic
    end

    assert_redirected_to post_topics_path
  end
end
