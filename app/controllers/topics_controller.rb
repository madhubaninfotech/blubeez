class TopicsController < ApplicationController
  before_action :set_topic, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_customer!, :except=> [:show, :index]

  def index
    if params["sortby"].present?
      @topics = Topic.filter_topics(params)
    else
      @topics_by_country = Topic.search_topic_by_country
      @topics = Topic.includes(:customer, :post_topics).all
    end
    add_breadcrumb "Forum", "/forum"
    add_breadcrumb "#{AppConstants::REGIONS_HASH[params[:region].to_i]}", "#{params[:region]}"
    render layout: "v2"
  end

  def additional_index
    @topics = Topic.includes(:post_topics, :customer).where("additional_topic = (?)", params[:id].to_i).paginate(page: params[:page], per_page: 10)
    render layout: "v2"
  end

  def additional_show
    @topic = Topic.includes(:post_topics, :customer).find(params[:topic_id].to_i)
    @post_topics = @topic.post_topics.paginate(page: params[:page], per_page: 10)
    AppUtils.view_counter(@topic)
  end

  def show
    @post_topics = @topic.post_topics.paginate(page: params[:page], per_page: 10)
    AppUtils.view_counter(@topic)
    add_breadcrumb "Forum", "/forum"
    country_name = AppUtils.country_name_by_country_code(@topic.location_country)
    add_breadcrumb "#{country_name}", "/forum/country/#{@topic.location_country}"
    add_breadcrumb "#{Nokogiri::HTML(@topic.title).text.truncate(50)}"
  end

  def new
    @topic = Topic.new
    add_breadcrumb "Forum", "/forum"
    add_breadcrumb "New"
  end

  def edit
    add_breadcrumb "Forum", "/forum"
    country_name = AppUtils.country_name_by_country_code(@topic.location_country)
    add_breadcrumb "#{country_name}", "/forum/country/#{@topic.location_country}"
    add_breadcrumb "#{Nokogiri::HTML(@topic.title).text.truncate(50)}", "/forum/topics/#{@topic.id}"
    add_breadcrumb "Edit"
  end

  def create

    @topic = Topic.new(topic_params)

    respond_to do |format|
      if @topic.save
        format.html { redirect_to "/forum/topics", notice: 'Topic was successfully created.' }
        format.json { render :show, status: :created, location: "/forum/topics" }
      else
        format.html { render :new }
        format.json { render json: @topic.errors, status: :unprocessable_entity }
      end
    end
  end

  def update

    respond_to do |format|
      if @topic.update(topic_params)
        format.html { redirect_to "/forum/topics/#{@topic.id}", notice: 'Topic was successfully updated.' }
        format.json { render :show, status: :ok, location: "/forum/topics/#{@topic.id}" }
      else
        format.html { render :edit }
        format.json { render json: @topic.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @topic.destroy
    respond_to do |format|
      format.html { redirect_to topics_url, notice: 'Topic was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_topic
      @topic = Topic.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def topic_params
      params.require(:topic).permit(:title, :content, :safe, :closed, :trusted, :customer_id, :location_name, :location_country, :location_state, :location_city, :additional_topic)
    end
end
