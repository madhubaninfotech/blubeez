source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'nio4r', '1.2'
gem 'rails', '5.0.0'
gem 'actionpack', '5.0.0'
gem 'railties', '5.0.0'
gem 'actioncable', '5.0.0'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc
gem "lazyload-rails"
gem 'email_validator'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development
gem 'font-kit-rails', '~> 1.2.0'
gem 'rails-i18n', github: 'svenfuchs/rails-i18n', branch: 'rails-5-x' # For 4.x
group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  gem "pry-rails"
  gem "pry-nav"
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end

gem 'sprockets-rails', :require => 'sprockets/railtie'

gem 'rest-client'
gem 'devise_token_auth'
gem 'omniauth-facebook'
gem 'omniauth-google-oauth2'
gem 'omniauth-twitter'
gem 'timezone', '~> 1.0'
gem 'open-weather'
gem 'sitemap_generator'
# gem 'omniauth-github'
gem 'rack-cors', :require => 'rack/cors'

gem "rmagick", '4.1.2'
gem 'carrierwave', '2.0.0'
gem 'carrierwave-video'
gem 'rails-assets-jcrop', source: 'https://rails-assets.org'
gem 'carrierwave-imageoptimizer'
# gem 'mini_magick'
gem "bugsnag"
gem "slot_machine"

gem 'impressionist'
gem 'mandrill_mailer'
gem "sendgrid"
gem 'rollbar'

gem 'bootstrap-sass', '3.3.5'
gem 'font-awesome-sass'
gem 'momentjs-rails'
gem 'jquery_notify_bar'
gem "therubyracer"
gem 'flexslider'
gem 'bxslider-rails'
gem 'jquery-ui-rails'
gem "browser"

gem 'country_select'
gem 'validate_url'

gem "breadcrumbs_on_rails"
gem 'omniauth'

gem 'devise'
gem "rails-settings-cached", "~> 0.4.0"

gem 'mysql2', '< 0.5'
gem 'sucker_punch', '~> 2.0'
gem 'sinatra', require: false
gem 'sidekiq-limit_fetch'
gem 'slim'
gem 'city-state'


gem "money"
gem "google_currency", "~> 3.1.0"
gem "money-rails"

# https://github.com/mbleigh/acts-as-taggable-on
gem 'acts-as-taggable-on', '~> 4.0'
gem 'acts_as_follower', github: 'tcocca/acts_as_follower', branch: 'master'
gem 'activemerchant'

gem "redis"
gem "redis-rails"
gem "ckeditor_rails"
gem 'jquery-fileupload-rails', '0.4.1'

gem 'will_paginate'
gem 'will_paginate-bootstrap'

gem 'oauth2'
gem "geocoder"
gem "gmaps4rails"


# Deplyment
# 
gem "puma"

gem 'capistrano'
gem 'capistrano3-puma'
gem 'capistrano-rails'
gem 'capistrano-bundler'
gem 'capistrano-rvm'
gem 'capistrano-rails-console'
gem 'rails_12factor', group: :production
# gem 'capistrano-sidekiq'#, github: 'seuros/capistrano-sidekiq'

gem 'remotipart', '~> 1.2'
gem 'acts_as_votable'

# https://www.willandskill.se/en/install-elasticsearch-6-x-on-ubuntu-18-04-lts/
gem 'searchkick'
gem 'letter_opener', group: :development
gem 'utf8-cleaner'

gem 'mimemagic', github: 'mimemagicrb/mimemagic', ref: '01f92d86d15d85cfd0f20dabd025dcbd36a8a60f'
gem 'pry'

gem "recaptcha"
gem "flag-icons-rails"