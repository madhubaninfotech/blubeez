class CreateCheckin < ActiveRecord::Migration
  def change
    create_table :checkins do |t|
      t.belongs_to :customer, index: true, foreign_key: true
      t.belongs_to :destination, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
