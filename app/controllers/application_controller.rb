class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  respond_to :json, :js, :html
  helper_method :is_prod?
  before_action :set_notification
  before_action :set_locale
  before_action :set_url
  after_action :store_location
  before_action :set_meta_tags


  def set_url
    unless action_name == "search" || params[:search_data].present?
      redirect_to request.url.gsub!('%20', '-') if request.url.index("%20").present?
    end
  end

  def set_meta_tags
    @meta_desc = "Airvtr"
    @meta_key = "Airvtr"
  end

  def set_locale
    if !params[:locale].nil?
      session[:locale] = params[:locale]
    end
    I18n.locale = session[:locale] || params[:locale] || I18n.default_locale
  end

  def not_found
    redirect_to "/404.html"
  end

  private

  def resource_name
    :customer
  end
  helper_method :resource_name
 
  def resource
    @resource ||= Customer.new
  end
  helper_method :resource
 
  def devise_mapping
    @devise_mapping ||= Devise.mappings[:customer]
  end
  helper_method :devise_mapping
 
  def resource_class
    Customer
  end
  helper_method :resource_class

  def set_notification
    if customer_signed_in?
      @unread = true
      @notification = current_customer.notifications.where("status = ? AND customer_id = ? ", AppConstants::UNREAD, current_customer.id)
      if @notification.nil?
        @unread = false
        @notification = current_customer.notifications.last(5)
      end
    end
  end

  def is_prod?
    Rails.env.production? ? true : false
  end

  def both_accessible?
    if signed_in?
      true
    else
      if request.format.symbol == :js
        render js: "window.location.href='#{request.path}'"
      else
        authenticate_customer!
      end
    end
  end

  def store_location
    is_url_store = false
    ["customers", "multiple_image", "admin", "single_image", "traveller_images", "vote"].each do |url|
      if request.fullpath.index(url).present?
        is_url_store = true
        break
      end
    end
    session[:previous_url] = request.fullpath unless is_url_store
  end

  def after_sign_in_path_for(resource)
    if resource.class.name == "Customer"
      if resource.profile.present?
        "/customers/#{resource.id}/posts"
      else
        customer_profile_path(nil, resource.id)
      end
    else
      root_path
    end
  end




end
