class FlightFacilitiesController < ApplicationController

  def create
    @flight_facility = FlightFacility.new(facilities_params)
    if @flight_facility.save!
      flash.now[:notice] = "Successfully Updated"
    else
      flash.now[:error] = @facility.errors
    end
  end

  def update
    @flight_facility = FlightFacility.where(:flight_id=> params["flight_id"]).first
    if @flight_facility.update(facilities_params)
      flash.now[:notice] = "Successfully Updated"
    else
      flash.now[:error] = @facility.errors
    end
  end

  def destroy
  end

  private

  def facilities_params
    params.require(:flight_facility).permit(:checkin, :checkout, :cancelorprepay, :childrenbeds, :pets, :parking, :groups, :cardsaccepted, :flight_id)
  end

end