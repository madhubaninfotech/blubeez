class HomeController < ApplicationController

	def locale_index
		if request.env["HTTP_REFERER"].present? && (request.env["HTTP_REFERER"] != request.env["REQUEST_URI"]) && params["locale"].present?
			redirect_to request.env["HTTP_REFERER"]
		else
			redirect_to root_path
		end
	end

	def index
		@destinations = Destination.includes(:attractions, :ratings, :events, :images).with_main_image.last(9)
		@banners = Banner.all
		@blogs = TravelBlog.includes(:blog_comments, :customer).last(5)
		@hot_trips = @destinations.first(5)
		@popular_packages = @destinations.last(5)
		@customers = Customer.includes(:profile).where("is_public = ?", AppConstants::YES)
		@countries_wise_place_hash = AppUtils.get_popular_countries
		render layout: "v2"
	end

	def search
		unless params["name"].present?
			flash.now[:error] = "Please select any data"
		end	
	end

	def contact_form
		ContactJob.perform_async(params["contact"])
    flash.now[:notice] = "Succeeded"
		redirect_to "/contact"
	end

	def contact
		
	end

	def share_as_email
		if EmailValidator.valid?(params["share"]["email"])
			AirvtrMailer.blog_share_email(params["share"]["email"], params["url_path"].first).deliver_now!
		else
			flash[:error] = "Failed"
		end
	end

	def about
		render layout: "v2"
	end
	
	def partner_with_us
		render layout: "v2"
	end
  def add_listing
		render layout: "v2"
	end

  def save_listing
		listing = Listing.new(listing_params)
		if listing.save
			flash[:notice] = "Thank you for registering with us, We will review the above details and get in touch with you soon."
		else
			flash[:error] = listing.errors.full_messages.join("<br/>")
		end
		redirect_to "/add-listing"
	end

  def report_spam
		render layout: "v2"
	end

  def save_spam
		report_spam = ReportSpam.new(report_spam_params)
		if report_spam.save
			ReportSpamJob.perform_async(report_spam_params)
			flash[:notice] = "Sorry for the inconvenience, We will review the above details and get in touch with you soon."
		else
			flash[:error] = report_spam.errors.full_messages.join("<br/>")
		end
		redirect_to "/report-spam"
	end

	def report_fraud
		render layout: "v2"
	end

	 def save_fraud
		report_fraud = ReportFraud.new(report_fraud_params)
		if report_fraud.save
			ReportFraudJob.perform_async(report_fraud_params)
			flash[:notice] = "Sorry for the inconvenience, We will review the above details and get in touch with you soon."
		else
			flash[:error] = report_fraud.errors.full_messages.join("<br/>")
		end
		redirect_to "/report-fraud"
	end

  private
  def listing_params
		params.require(:listing).permit(:name, :phone, :email, :is_read, :location, :service_desc, :website, :is_owner, :message)
	end

  def report_spam_params
		params.require(:report_spam).permit(:name, :email, :is_registered_with_blubeez, :phone, :spam_against_person, :spam_profile_link, :spam_type, :message)
	end

	def report_fraud_params
		params.require(:report_fraud).permit(:name, :email, :is_registered_with_blubeez, :phone, :fraud_against_person, :fraud_against_url, :message)
	end

end