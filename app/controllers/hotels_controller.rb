class HotelsController < ApplicationController
  before_action :authenticate_admin!, except: [:index, :show, :all_index]
  before_action :set_hotel, only: [:show, :edit, :update, :destroy]

  # GET /hotels
  # GET /hotels.json
  def index
    dest = Destination.where(slug: params["slug"]).first
    @hotels = dest.hotels.all.paginate(page: params[:page], per_page: 10)
  end

  def all_index
    unless params["search_data"].present?
      if params[:sortby].present?
        if params[:sortby].to_i == AppConstants::SORT_BY_POPULARITY
          @hotels = Hotel.includes(:ratings).all.order("view_count DESC").paginate(page: params[:page], per_page: 10)
        elsif params[:sortby].to_i == AppConstants::SORT_BY_RATING
          @hotels = Hotel.includes(:ratings).all.order("ratings.score DESC").paginate(page: params[:page], per_page: 10)
        else
          @hotels = Hotel.includes(:ratings).all.order("created_at DESC").paginate(page: params[:page], per_page: 10)
        end
      else
        @hotels = Hotel.includes(:ratings).all.paginate(page: params[:page], per_page: 10)
      end
    else
      @hotels = Hotel.includes(:ratings).where(:name=> params["name"]).paginate(page: params[:page], per_page: 10)
    end
  end
  # GET /hotels/1
  # GET /hotels/1.json
  def show
    hotel = Hotel.where(id: params["id"]).first
    if hotel.present?
      AppUtils.view_counter(hotel)
      @hotel_facility = hotel.hotel_facility
      @reviews = Review.hotel_search(params[:id])
      @meta_desc = "#{hotel.name} - #{hotel.description}"
      @meta_key = hotel.name
    else
      return redirect_to no_hotel_detail_found
    end
  end

  # GET /hotels/new
  def new
    @is_with_location = params["add_location"].present? ? true : false
    unless @is_with_location
      dest = Destination.where(id: params["destination_id"]).first
      @hotel = dest.hotels.new
    else
      @hotel = Hotel.new
    end

  end

  # GET /hotels/1/edit
  def edit
    dest = Destination.where(id: params["destination_id"]).first
    unless dest.hotels.blank?
      @hotel = dest.hotels.where(id: params["id"]).first
    end
  end

  # POST /hotels
  # POST /hotels.json
  def create

    @add_location = params["add_location"].present? ? true : false

    unless @add_location
      dest = Destination.where(id: params["destination_id"]).first
    else
      if params["locations"].present?
        if params["locations"]["country"].present? && params["locations"]["state"].present? && params["locations"]["cities"].present? && params["locations"]["name"].present?
          dest = AppUtils.get_location_details_for_flight_or_hotel(params["locations"]).first
        else
          redirect_to "/add_flights/?add_location=true", :flash=> {:error=>"Enter valid location"}
        end
      else
        redirect_to "/add_flights/?add_location=true", :flash=> {:error=>"Enter valid location"}
      end
    end

    @hotel = dest.hotels.new(hotel_params)

    respond_to do |format|
      if @hotel.save
        DynamicJob.perform_async
        format.html { redirect_to "/hotels", notice: 'Hotel was successfully created.' }
        format.json { render :show, status: :created, location: "/hotels" }
      else
        format.html { render :new }
        format.json { render json: @hotel.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /hotels/1
  # PATCH/PUT /hotels/1.json
  def update
    dest = Destination.where(id: params["destination_id"]).first
    @hotel = dest.hotels.where(name: params["name"]).first
    respond_to do |format|
      if @hotel.update(hotel_params)
        DynamicJob.perform_async
        # unless params["hotel"]["main_image"] == params["main_old_image"]
        #   img = Image.find(params["main_old_image"])
        #   img.destroy
        # end
        format.html { redirect_to "/hotels", notice: 'Hotel was successfully updated.' }
        format.json { render :show, status: :ok, location: "/hotels" }
      else
        format.html { render :edit }
        format.json { render json: @hotel.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hotels/1
  # DELETE /hotels/1.json
  def destroy
    @hotel.destroy
    respond_to do |format|
      DynamicJob.perform_async
      format.html { redirect_to hotels_url, notice: 'Hotel was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def no_hotel_detail_found
    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hotel
      @hotel = Hotel.where(id: params[:id]).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def hotel_params
      params.require(:hotel).permit(:name, :description, :price_from, :image, :main_image, :destination_id, :amenities => [])
    end
end
