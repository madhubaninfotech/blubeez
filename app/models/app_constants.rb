module AppConstants

  GENDER_TYPE = 1
    MALE = 1
    FEMALE = 2
    OTHER = 3

  URL = "www.blubeez.com"

  GENDER_ARR = [["Male", MALE], ["Female", FEMALE], ["Other", OTHER]]

  GENDER_HASH = { "1" => "Male", "2" => "Female", "3" => "Other"}

  # YES OR NO
  YES = 1
  NO = 0

  # user status
  INACTIVE = 0
  ACTIVE = 1
  DELETE = 2
  USER_STATUS_ARR = [["In Active", 0],["Active", 1], ["DELETE", 2]]

  # Search Data
  SEARCH_DATA_ARRAY = ["Destinations", "Hotels", "Events", "Flights", "Blogs", "Travellers"]


  #Default Date Format
  DATE_FORMAT_DEFAULT = "%d/%m/%Y"

  BANNER_LI_STYLES = {
    0 => 'data-slotamount="7" data-masterspeed="500" data-title="Slide title 1"',
    1 => 'data-slotamount="7" data-masterspeed="500" data-title="Slide title 2"',
    2 => 'data-slotamount="7" data-masterspeed="500" data-title="Slide title 3"'
  }
  BANNER_IMG_STYLES = {
    0 => 'data-bgposition="left center" data-duration=1400 data-bgpositionend="right center"',
    1 => 'data-bgposition="left center" data-duration=1400 data-bgpositionend="right center"',
    2 => 'data-bgposition="left center" data-duration=1400 data-bgpositionend="right center"'
  }

  #DESITNATION TYPE

  FOOD = 1
  MUSIC_AND_FESTIVAL = 2
  HISTORY_AND_CULTURE = 3
  SPORTS = 4
  NATURE = 5
  ENTERTAIN = 6
  HEALTH = 7
  FASHION_AND_SHOPPING = 8
  DESTINATION_CATEGORY = [["Food", FOOD], ["Music Festival", MUSIC_AND_FESTIVAL], ["History and Culture", HISTORY_AND_CULTURE], ["Sports", SPORTS], ["Nature", NATURE], ["Entertain", ENTERTAIN], ["Health", HEALTH], ["Fashion and Shopping", FASHION_AND_SHOPPING]]
  DESTINATION_CATEGORY_HASH = {
      FASHION_AND_SHOPPING => "Fashion and Shopping",
      FOOD => "Food",
      MUSIC_AND_FESTIVAL => "Music Festival",
      HISTORY_AND_CULTURE => "History and Culture",
      SPORTS => "Sports",
      NATURE => "Nature",
      ENTERTAIN => "Entertain",
      HEALTH => "Health"
  }

  #Region Names
  EUROPE = 1
  ASIA = 2
  NORTH_AMERICA = 3
  AFRICA = 4
  ANTARTICA = 5
  SOUTH_AMERICA = 6
  AUSTRALIA = 7
  REGIONS = [["Europe", EUROPE], ["Asia", ASIA], ["North America", NORTH_AMERICA], ["Africa", AFRICA] , ["Antarctica", ANTARTICA], ["South America", SOUTH_AMERICA], ["Australia", AUSTRALIA]]
  REGIONS_HASH = {
      EUROPE => "Europe",
      ASIA => "Asia",
      NORTH_AMERICA => "North America",
      AFRICA => "Africa",
      ANTARTICA => "Antarctica",
      SOUTH_AMERICA => "South America",
      AUSTRALIA => "Australia"
  }

  # Forum Options
  VISA_AND_INSURANCE = 8
  TRAVEL_TIPS = 9
  TOOLS_AND_DEALS = 10
  AIR_TRAVEL = 11
  HOTELS = 12
  CRUISES = 13
  ADDITIONAL_FORUM_ARRAY = [VISA_AND_INSURANCE, TRAVEL_TIPS, TOOLS_AND_DEALS, AIR_TRAVEL, HOTELS, CRUISES]
  ADDITIONAL_FORUM_CATEGORIES_ARRAY = [["Visa and Insurance", VISA_AND_INSURANCE], ["Travel Tips & Ideas", TRAVEL_TIPS], ["Tools & Deals", TOOLS_AND_DEALS], ["Air Travel", AIR_TRAVEL], ["Hotels", HOTELS], ["Cruises", CRUISES]]
  ADDITIONAL_FORUM_CATEGORIES_HASH = {
      VISA_AND_INSURANCE => "Visa and Insurance",
      TRAVEL_TIPS => "Travel Tips & Ideas",
      TOOLS_AND_DEALS => "Tools & Deals",
      AIR_TRAVEL => "Air Travel",
      HOTELS => "Hotels",
      CRUISES => "Cruises"
  }
  #SEASONS
  SPRING = 1
  SUMMER = 2
  AUTUMN = 3
  WINTER = 4
  SEASON_ARR = [["Spring", SPRING], ["Summer", SUMMER], ["Autumn", AUTUMN], ["Winter", WINTER]]
  SEASON_HASH = {
      SPRING => "Spring",
      SUMMER => "Summer",
      AUTUMN => "Autumn",
      WINTER => "Winter"
  }

  #Amenities
  ROOM_SERVICE = 1
  BUNKBED_AVAILABLE = 2
  LAUNDRY = 3
  SHOWER = 4
  OUTSIDE_POOL = 4
  ROOM_MEAL_SERVICE = 5
  HIGH_SECURITY = 6
  TV_IN_ROOM = 7

  AMENITY_ARR = [["Room Service", ROOM_SERVICE],
   ["bunkbed Available", BUNKBED_AVAILABLE],
   ["laundry", LAUNDRY],
   ["shower", SHOWER],
   ["outside Pool", OUTSIDE_POOL],
   ["room Meal Service", ROOM_MEAL_SERVICE],
   ["high Security", HIGH_SECURITY],
   ["tv In Room", TV_IN_ROOM]]

  AMENITY_HASH = {
      ROOM_SERVICE => "Room Service",
      BUNKBED_AVAILABLE => "bunkbed Available",
      LAUNDRY => "laundry",
      SHOWER => "shower",
      OUTSIDE_POOL => "outside Pool",
      ROOM_MEAL_SERVICE => "room Meal Service",
      HIGH_SECURITY => "high Security",
      TV_IN_ROOM => "Tv In Room"
  }

  AMENITY_HASH_CLASS = {
      ROOM_SERVICE => "awe-icon awe-icon-unlock",
      BUNKBED_AVAILABLE => "awe-icon awe-icon-beds",
      LAUNDRY => "awe-icon awe-icon-laundry",
      SHOWER => "awe-icon awe-icon-shower",
      OUTSIDE_POOL => "awe-icon awe-icon-pool",
      ROOM_MEAL_SERVICE => "awe-icon awe-icon-meal",
      HIGH_SECURITY => "awe-icon awe-icon-key",
      TV_IN_ROOM => "awe-icon awe-icon-tv"
  }

  CATERING_SERVICE = 1
  HEALTH_CARE = 2
  LUXURY_SEATS = 3
  MOVIE = 4
  MUSIC = 5
  IN_FLIGHT_MEALS = 6
  GROUND_TRANSPORTATION = 7

  FLIGHTS_HIGHLIGHTS_ARR = [
      ["Catering service", CATERING_SERVICE],
      ["Health Care", HEALTH_CARE],
      ["Luxury seats", LUXURY_SEATS],
      ["Movie", MOVIE],
      ["Music", MUSIC],
      ["In-Flight Meals", IN_FLIGHT_MEALS],
      ["Ground Transportation", GROUND_TRANSPORTATION]
  ]

  FLIGHTS_HIGHLIGHTS_HASH =  {
      CATERING_SERVICE => "Catering service",
      HEALTH_CARE => "Health Care",
      LUXURY_SEATS => "Luxury seats",
      MOVIE => "Movie",
      MUSIC => "Music",
      IN_FLIGHT_MEALS => "In-Flight Meals",
      GROUND_TRANSPORTATION => "Ground Transportation"
  }

  TRAVEL_TRIP = 1
  TRAVEL_BLOG = 2
  TRAVEL_PHOTOS = 3
  # trip status
  READ = 0
  UNREAD = 1
  NOTIFICATION_TYPE_HASH = {
      TRAVEL_TRIP => "Travel Trip",
      TRAVEL_BLOG => "Travel Blog",
      TRAVEL_PHOTOS => "Travel Uploads"
  }

  SORT_BY_DATE = 1
  SORT_BY_POPULARITY = 2
  SORT_BY_RATING = 3
  SORTING_ARRAY = [["By Date", SORT_BY_DATE], ["By Popularity", SORT_BY_POPULARITY], ["By Rating", SORT_BY_RATING]]
  SORTING_HASH = {
      SORT_BY_DATE => "By Date",
      SORT_BY_POPULARITY => "By Popularity",
      SORT_BY_RATING => "By Rating"
  }

  SORT_BY_COUNTRY_FOR_TOPIC = 1
  SORT_BY_STATE_FOR_TOPIC = 2
  SORT_BY_CITY_FOR_TOPIC = 3
  SORTING_ARRAY_FOR_TOPIC = [["By Country", SORT_BY_COUNTRY_FOR_TOPIC]]

  SORTING_HASH_FOR_TOPIC = {
      SORT_BY_COUNTRY_FOR_TOPIC => "By Country",
      SORT_BY_STATE_FOR_TOPIC => "By State",
      SORT_BY_CITY_FOR_TOPIC => "By City"
  }

  # ADVENTURE = 1
  # ART_AND_CULTURE = 2
  # LUXURY = 3
  # NATURE_AND_WILDLIFE = 4
  # BEACH = 5
  # FOOD_AND_DRINK = 6
  # TRAVEL_STORIES = 7
  # LIFE_STYLE = 8
  # BLOG_CATEGORIES = [
  #   ["Adventure & Backpacking", ADVENTURE],
  #   ["Arts, Culture & History", ART_AND_CULTURE],
  #   ["Luxury Travel", LUXURY],
  #   ["Nature & Wildlife", NATURE_AND_WILDLIFE],
  #   ["Beach Travels", BEACH],
  #   ["Food & Drinks", FOOD_AND_DRINK],
  #   ["Travel Stories", TRAVEL_STORIES],
  #   ["Lifestyle", LIFE_STYLE]
  # ]
  BLOG_CATEGORIES = ["Adventure & Backpacking", "Arts, Culture & History", "Luxury Travel", "Nature & Wildlife",
  "Beach Travels", "Food & Drinks", "Travel Stories", "Lifestyle"]
  BLOG_ADMIN_CATEGORIES = ["Adventure", "Lifestyle", "Stories@buzz"]

  WEATHER_FONT = {
    "01d" => "01",
    "02d" => "02",
    "03d" => "03",
    "04d" => "04",
    "09d" => "09",
    "10d" => "10",
    "11d" => "11",
    "13d" => "13",
    "50d" => "50",
    "01n" => "01",
    "02n" => "02",
    "03n" => "03",
    "04n" => "04",
    "09n" => "09",
    "10n" => "10",
    "11n" => "11",
    "13n" => "13",
    "50n" => "50"
  }

end