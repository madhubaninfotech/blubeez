class AddImagesToReview < ActiveRecord::Migration
  def change
    add_column :reviews, :images, :text, array: true, :default => nil, :null => true
    add_column :reviews, :tip, :text
  end
end
