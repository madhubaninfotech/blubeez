class AddHighlightsToFlight < ActiveRecord::Migration
  def change
    add_column :flights, :highlights, :string, array: true, default: []
  end
end
