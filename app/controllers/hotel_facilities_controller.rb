class HotelFacilitiesController < ApplicationController

  def create
    @facility = HotelFacility.new(facilities_params)
    if @facility.save!
      flash.now[:notice] = "Successfully Updated"
    else
      flash.now[:error] = @facility.errors
    end
  end

  def update
    @facility = HotelFacility.where(:hotel_id=> params["hotel_id"]).first
    if @facility.update(facilities_params)
      flash.now[:notice] = "Successfully Updated"
    else
      flash.now[:error] = @facility.errors
    end
  end

  def destroy
  end

  private

  def facilities_params
    params.require(:hotel_facility).permit(:view, :activities, :mediatechnology, :food, :internet, :parking, :services, :general, :languages, :hotel_id)
  end

end