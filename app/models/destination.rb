class Destination < ActiveRecord::Base
  # mount_uploaders :destination_image, ImageUploader
  # mount_uploader :main_image, ImageUploader
  has_many :hotels, dependent: :destroy
  has_many :flights, dependent: :destroy
  has_many :attractions, dependent: :destroy
  has_many :events, dependent: :destroy
  has_many :ratings, dependent: :destroy
  has_many :reviews, dependent: :destroy
  has_many :wishlist, dependent: :destroy
  has_many :checkins, dependent: :destroy
  has_one :temp_image, dependent: :destroy
  has_many :images, as: :imageable
  geocoded_by :address, :if => :address_changed?
  after_validation :geocode, :if => :address_changed?
  validates_uniqueness_of :slug
  validates_presence_of :slug
  before_save :check_slug
  after_save :check_uploads
  # validates_uniqueness_of :destination_name, case_sensitive: false
  scope :destination_name_search, -> (d_name) { where( 'UPPER(destination_name) like ?', "%#{d_name.upcase}%")}
  scope :destination_country_search, -> (d_country) {where('destination_country = ?', d_country)}
  scope :destination_countries_search, -> (country_list) {where('destination_country in (?)', country_list)}
  scope :destination_purpose, -> (d_purpose) {where(destination_category: d_purpose)}
  scope :destination_season, -> (d_season) {where(best_for: d_season)}
  scope :destination_region, -> (d_region) {where(destination_country: d_region)}
  scope :with_main_image, -> () {where(images: {image_type: "main_image"})}
  scope :with_search, -> (starts_with) { where( 'lower(destination_name) like (?)', "#{starts_with}%") if starts_with.present? }

  def average_rating
    ratings.map(&:score).sum / (ratings.size != 0 ? ratings.size : 1)
  end

  def score
    ratings.present? ? ratings.score : 0
  end

  def self.get_hotels_flights_attractions_events_per_destin(destinations)
    hotels = Hash.new
    attractions = Hash.new
    events = Hash.new

    destinations.each do |dest|
      flights[dest.id] = dest.flights.merge!(dest.flights)
      hotels[dest.id] = dest.hotels.merge!(dest.hotels)
      attractions[dest.id] = dest.attractions.merge!(dest.attractions)
      events[dest.id] = dest.events.merge!(dest.events)
    end

    [hotels, flights, attractions, events]
  end

  def self.get_destination_filters(data)
    cond = ""
    if data["purpose"].present?
      cond += "destination_category IN (#{data["purpose"].join(",")})"
    end
    if data["region"].present?
      country_arr = Array.new
      cond += " AND " if cond != ""
      data["region"].each do |region|
        r_name = AppConstants::REGIONS_HASH[region.to_i]
        country_arr << AppUtils.countries_code_from_region(r_name)
      end
      c = country_arr.flatten.to_s
      r_str = c.gsub(/\[|\]/,"")
      cond += "destination_country IN (#{r_str})"
    end
    if data["season"].present?
      cond += " AND " if cond != ""
      cond += "best_for IN (#{data["season"].join(",")})"
    end

    if cond.blank?
      destinations = Destination.last(10)
    else
      destinations = Destination.where(cond)
    end
    destinations
  end

  def address_changed?
    changed = false
    if destination_name_changed? || destination_cities_changed? || destination_state_changed? || destination_country_changed?
      changed = true
    end
    changed
  end

  def image_url
    images.first.present? && images.first.image.present? ? images.first.image.v_3x.url : ActionController::Base.helpers.asset_path("blubeez-dummy.png")
  end

  def is_in_wishlist?(user)
    wishlist.where(customer_id: user).any?
  end

  def is_checkedin?(user)
    checkins.where(customer_id: user).any?
  end

  def self.get_desination_image_by_main_image_or_secondary(dest)
    if dest.main_image.present?
      main_image = AppUtils.get_single_image_from_image_modal(dest.main_image)
      if main_image.present?
        main_image.image.v_3x.url
      end
    elsif dest.destination_image.present? && JSON.parse(dest.destination_image).present?
      images_data = AppUtils.get_images_from_image_modal(dest.destination_image)[0] if dest.destination_image.present?
      if images_data.present?
        images_data[1].v_3x.url
      end
    else
      "img/1.jpg"
    end
  end

  def destination_images
    images.where(image_type: "destination_image")
  end

  def image
    images.where(image_type: "main_image").first
  end

  private

  def address
    [destination_name, destination_cities, destination_state, destination_country].compact.join(', ')
  end

  def check_slug
    self.slug = self.slug.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
  end

  def check_uploads
    is_image_present = Image.where("id = (?) and imageable_type = ''", self.main_image).any?
    unless is_image_present
      image = Image.where("id = (?)", self.main_image).first
      if image.present?
        AppUtils.set_image_association(image, self.id, "Destination", "main_image")
      end
    end
    if self.destination_image.present?
      images = Image.where(id: JSON.parse(self.destination_image), imageable_type: nil)
      images.each do |img|
        AppUtils.set_image_association(img, self.id, "Destination", "destination_image")
      end
    end
  end


end
