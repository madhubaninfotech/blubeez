class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.string :banner_image
      t.belongs_to :admin, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
