class CreateBlogComments < ActiveRecord::Migration
  def change
    create_table :blog_comments do |t|
      t.string :name
      t.text :comment
      t.integer :reply_for
      t.belongs_to :travel_blog, index: true, foreign_key: true
      t.belongs_to :customer, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
