class Wishlist < ActiveRecord::Base
  belongs_to :customer
  belongs_to :destination
end
