class AddTimeToDestination < ActiveRecord::Migration
  def change
    add_column :destinations, :best_for, :integer
  end
end
