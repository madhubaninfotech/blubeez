json.array!(@post_topics) do |post_topic|
  json.extract! post_topic, :id, :title, :content, :safe, :closed, :trusted, :topic_id_id
  json.url post_topic_url(post_topic, format: :json)
end
