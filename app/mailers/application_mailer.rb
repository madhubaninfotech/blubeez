class ApplicationMailer < ActionMailer::Base
  helper :application
  default from: "info@blubeez.com"
  layout 'mailer'
end
