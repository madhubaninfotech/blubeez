# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# ListEntry.create(:entry_id => 0, :entry_name => "Travels", :list_id => AdminType::FAQ_CATEGORIES)
Admin.destroy_all
Admin.create(email: "admin@gmail.com", password: "Admin@123", password_confirmation: "Admin@123")
Term.create(terms: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda magni excepturi dignissimos, nam corporis laboriosam debitis, dicta officiis. At voluptatibus eius commodi id, reprehenderit numquam assumenda odit. Eveniet, minima, aspernatur.")
Privacy.create(privacy: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, eius doloremque nesciunt hic nobis porro, totam praesentium laboriosam reiciendis quae minima voluptatibus obcaecati. Maxime aperiam consequuntur, maiores quo veniam nihil.") 
Faq.create(faq: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et voluptates saepe, error esse? Repellendus consequuntur, corporis sapiente beatae, quos maiores ex, maxime aspernatur nulla quibusdam odit ipsum? Aspernatur blanditiis, excepturi?")
Career.create(careers: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et voluptates saepe, error esse? Repellendus consequuntur, corporis sapiente beatae, quos maiores ex, maxime aspernatur nulla quibusdam odit ipsum? Aspernatur blanditiis, excepturi?")
AboutUs.create(about: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et voluptates saepe, error esse? Repellendus consequuntur, corporis sapiente beatae, quos maiores ex, maxime aspernatur nulla quibusdam odit ipsum? Aspernatur blanditiis, excepturi?")