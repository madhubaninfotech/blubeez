class CreateAdminTasks < ActiveRecord::Migration
  def change
    create_table :terms do |t|
      t.text :terms
    end
    create_table :privacies do |t|
      t.text :privacy
    end
    create_table :faqs do |t|
      t.text :faq
    end
    create_table :careers do |t|
      t.text :careers
    end
    create_table :about_us do |t|
      t.text :about
    end
  end
end
