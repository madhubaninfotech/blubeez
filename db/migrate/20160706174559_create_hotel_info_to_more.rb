class CreateHotelInfoToMore < ActiveRecord::Migration
  def change
    create_table :mores do |t|
      t.string :check_in
      t.string :check_out
      t.text :pets
      t.string :payments
      t.references :hotel, index: true, foreign_key: true
    end
  end
end
