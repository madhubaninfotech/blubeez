class Friend < ActiveRecord::Base
  belongs_to :customer

  def self.get_friends_data_by_id_hash(friends)
  	friend_list = Array.new
  	friends.each do |friend|
  		friend_list << friend.friend_id
  	end
  	friend_list
  end
end
