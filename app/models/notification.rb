class Notification < ActiveRecord::Base
  belongs_to :customer

  def self.sync_notification(content_id, type, cus_id)
    customer = Customer.find(cus_id)
    friends = customer.friends
    if friends.present?
      friends_arr = Array.new
      friends.each do |friend|
        friends_arr << friend.friend_id if friend.friend_id != cus_id
      end
      friends_arr.each do |f|
        notification = Notification.new
        notification.from_customer = cus_id
        notification.to_customer = friends_arr
        notification.customer_id = f
        notification.status = AppConstants::UNREAD
        notification.notify_type = type
        notification.content_id = content_id
        notification.save!
      end
    end

  end

end
