class Customer < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_one :profile, dependent: :destroy
  has_many :trips, dependent: :destroy
  has_many :photos, dependent: :destroy
  has_many :friends, dependent: :destroy
  has_many :travel_blogs, dependent: :destroy
  has_one :identity, dependent: :destroy
  has_many :ratings, dependent: :destroy
  has_many :reviews, dependent: :destroy
  has_many :notifications, dependent: :destroy
  has_many :blog_comments, dependent: :destroy
  has_many :topics, dependent: :destroy
  has_many :post_topics, dependent: :destroy
  has_many :wishlist, dependent: :destroy
  has_many :checkins, dependent: :destroy
  has_many :user_notifications, foreign_key: :recipient_id
  has_many :posts, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :post_images, through: :posts

  # searchkick
  searchkick

  acts_as_followable
  acts_as_follower
  acts_as_voter

  TEMP_EMAIL_PREFIX = 'change@me'
  TEMP_EMAIL_REGEX = /\Achange@me/
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable, :omniauthable, :omniauth_providers => [:facebook, :twitter, :google_oauth2]

  before_save :update_name
  # after_create :send_welcome_mail
  before_create :set_username

  validates_uniqueness_of :username
  # validates_presence_of :username

  validates_format_of :email, :without => TEMP_EMAIL_REGEX, on: :update
  validate :password_regex

  FEELING_HAPPY = 1
  FEELING_EXCITED = 2
  FEELING_TIRED = 3
  FEELING_LOVELY = 4
  FEELING_OTHERS = 5
  FEELINGS_ARR = [
    ["Feeling Happy", FEELING_HAPPY],
    ["Feeling Excited", FEELING_EXCITED],
    ["Feeling Tired", FEELING_TIRED],
    ["Feeling Lovely", FEELING_LOVELY],
    ["Others", FEELING_OTHERS]
  ]

  def self.find_for_oauth(auth, signed_in_resource = nil)
    identity = Identity.find_for_oauth(auth)
    customer = signed_in_resource ? signed_in_resource : identity.customer
    if customer.nil?
      # Get the existing customer by email if the provider gives us a verified email.
      # If no verified email was provided we assign a temporary email and ask the
      # customer to verify it on the next step via customersController.finish_signup
      email_is_verified = auth.info.email# && (auth.info.verified || auth.info.verified_email)
      email = auth.info["email"] if email_is_verified
      customer = Customer.where(:email => email).first if email

      # Create the user if it's a new registration
      if customer.nil?
        customer = Customer.new(
          name: auth.extra.raw_info.name,
          email: email ? email : "#{TEMP_EMAIL_PREFIX}-#{auth.uid}-#{auth.provider}.com",
          password: Devise.friendly_token[0,20]
        )

        # check if confirmable is enabled
        #customer.skip_confirmation! if customer.respond_to?(:skip_confirmation)
        customer.save!
        if auth.info["email"].present?
          CustomerMailer.welcome_email(customer).deliver_later
        end
      end
    end

    if identity.customer != customer
      identity.customer = customer
      identity.save!
    end

    customer
  end

  def send_welcome_mail
    CustomerMailer.welcome_email(self).deliver!
  end

  def email_verified?
    self.email && self.email !~ TEMP_EMAIL_REGEX
  end

  def create_profile_for_current_user(user)
    profile = Profile.new
    profile.name = user.name
    profile.email = user.email
    profile.customer_id = user.id
    profile.save!
  end

  def update_name
    self.name = "#{self.first_name} #{self.last_name}"
  end

  def fullname
    self.name.strip.present? ? self.name : self.email
  end

  def is_friend?(id)
    self.friends.where("friend_id = ?", id ).any?
  end

  def last_message(receiver)
    Conversation.select("messages.created_at, messages.body").joins(:messages).where("(sender_id = ? and receiver_id = ?) or (sender_id = ? and receiver_id = ?)", self.id, receiver.id, receiver.id, self.id).order("messages.created_at desc").first
  end

  def last_message_time(receiver)
    last_message(receiver).present? ? last_message(receiver).created_at.strftime("%b %d, %Y %I:%M%P") : ""
  end

  def last_msg_text(receiver)
    last_message(receiver).present? ? last_message(receiver).body : ""
  end

  def country_name
    profile.try(&:country_name)
  end
  
  def state_name
    profile.try(&:state_name)
  end

  def password_regex
    return if password.blank? || password =~ /\A(?=.*\d)(?=.*[A-Z])(?=.*\W)[^ ]{7,}\z/
    errors.add :password, 'should have more than 8 characters including 1 uppercase letter, 1 number, 1 special character'
  end

  private
  def set_username
    self.username = self.email[/^[^@]+/]+SecureRandom.hex(3)+self.created_at.to_i.to_s.last(4)+self.last_name+"_"+SecureRandom.hex(2)
  end
end
