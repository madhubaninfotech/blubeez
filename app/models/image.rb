class Image < ActiveRecord::Base
	include Rails.application.routes.url_helpers
  mount_uploader :images, ImageUploader
  mount_uploader :image, ImageUploader
  belongs_to :imageable, polymorphic: true


  def to_jq_upload(images)
    {
      "id" => id,
      "name" => read_attribute(:images),
      "size" => images.size,
      "url" => images.url,
      "thumbnail_url" => images.v_3x.url,
      "delete_url" => images_path(:id => id),
      "delete_type" => "DELETE",
      "desc" => images.model.desc
    }
  end
end