json.array!(@topics) do |topic|
  json.extract! topic, :id, :title, :content, :safe, :closed, :trusted
  json.url topic_url(topic, format: :json)
end
