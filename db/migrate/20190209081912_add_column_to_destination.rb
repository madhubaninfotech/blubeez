class AddColumnToDestination < ActiveRecord::Migration
  def change
    add_column :destinations, :slug, :string
    add_column :destinations, :title, :text
    add_column :destinations, :keywords, :string
  end
end
