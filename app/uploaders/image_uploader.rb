# encoding: utf-8

class ImageUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  include CarrierWave::RMagick
  include CarrierWave::ImageOptimizer
  # include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:

  process optimize: [{ quality: 50 }]
  process :watermark

  # def watermark
  #   manipulate! do |img|
  #     logo = Magick::Image.read("#{Rails.root}/app/assets/images/watermark.png").first
  #     img = img.composite(logo, Magick::NorthWestGravity, 15, 0, Magick::OverCompositeOp)
  #   end
  # end

  def watermark(opacity = 0.15, size = 'l')
    if model.is_watermark_enabled
      manipulate! do |img|
        logo = Magick::Image.read("#{Rails.root}/app/assets/images/watermark.png").first
        img = img.composite(logo, Magick::NorthWestGravity, 15, 0, Magick::OverCompositeOp)
      end
      # manipulate! do |img|
      #   logo = Magick::Image.read("#{Rails.root}/app/assets/images/watermark.png").first
      #   logo.alpha(Magick::ActivateAlphaChannel) 

      #   white_canvas = Magick::Image.new(logo.columns, logo.rows) { self.background_color = "none" }
      #   white_canvas.alpha(Magick::ActivateAlphaChannel)
      #   # white_canvas.opacity = Magick::QuantumRange - (Magick::QuantumRange * opacity)

      #   # Important: DstIn composite operation (white canvas + watermark)
      #   logo_opacity = logo.composite(white_canvas, Magick::NorthWestGravity, 15, 0, Magick::DstInCompositeOp)
      #   logo_opacity.alpha(Magick::ActivateAlphaChannel)

      #   # Important: Over composite operation (original image + white canvas watermarked)
      #   img = img.composite(logo_opacity, Magick::NorthWestGravity, 0, 0, Magick::OverCompositeOp)
      # end
    end
  end

  def store_dir
     "uploads/#{model.created_at.year}/#{model.created_at.month}/#{model.created_at.day}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  # version :thumb do
  #   process :resize_to_fit => [50, 50]
  # end

  # def main_image?
  #   model.try(:main_image).present? &&  model.main_image_changed?
  # end
  #
  # def is_resize_version?
  #   ImageUploader.new.main_image?
  # end

  version :v_3x do
    process :resize_to_fit_by_percentage => 0.25
  end



  private

  def resize_to_fit_by_percentage(percentage)
    width = Magick::Image.read(file.file).first.columns
    resize_to_fit width*percentage, nil
  end


  # version :destination_grid_sm do
  #   process :resize_to_fit => [439, 293]
  # end
  #
  # version :destination_grid_lg do
  #   process :resize_to_fit => [973, 585]
  # end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_white_list
    %w(jpg jpeg gif png)
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end

end
