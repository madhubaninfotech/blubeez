class AddLocationToTopic < ActiveRecord::Migration
  def change
    add_column :topics, :location_name, :string
    add_column :topics, :location_country, :string
    add_column :topics, :location_state, :string
    add_column :topics, :location_city, :string
  end
end
