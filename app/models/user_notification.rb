class UserNotification < ApplicationRecord
	belongs_to :recipient, class_name: "Customer"
	belongs_to :friend, class_name: "Customer"
	belongs_to :notifiable, polymorphic: true
	scope :unread_count, -> { where( 'read_at is null').count}
end
