class TravelBlogsController < ApplicationController
  before_action :both_accessible?, only: [:new, :edit, :create, :update]
  before_action :authenticate_admin!, only: [:new_post, :edit_post, :create_post, :update_post, :destroy]
  before_action :set_travel_blog, only: [:show, :edit, :update, :destroy]

  def blog_main
  end

  # Public posts
  def all_index
    categories = params[:categories].present? ? params[:categories] : "all"
    unless params["search_data"].present?
      if categories == "all"
        if params[:sortby].present?
          if params[:sortby].to_i == AppConstants::SORT_BY_POPULARITY
            @travel_blogs = TravelBlog.includes(:customer, :blog_comments).all.order("view_count DESC").paginate(page: params[:page], per_page: 10)
          else
            @travel_blogs = TravelBlog.includes(:customer, :blog_comments).all.order("created_at DESC").paginate(page: params[:page], per_page: 10)
          end
        else
          @travel_blogs = TravelBlog.includes(:customer, :blog_comments).all.order("created_at DESC").paginate(page: params[:page], per_page: 10)
        end
      else
        if params[:sortby].present?
          if params[:sortby].to_i == AppConstants::SORT_BY_POPULARITY
            @travel_blogs = TravelBlog.includes(:customer, :blog_comments).where("category like ?", "%#{categories}%").order("view_count DESC").paginate(page: params[:page], per_page: 10)
          else
            @travel_blogs = TravelBlog.includes(:customer, :blog_comments).where("category like ?", "%#{categories}%").order("created_at DESC").paginate(page: params[:page], per_page: 10)
          end
        else
          @travel_blogs = TravelBlog.includes(:customer, :blog_comments).where("category like ?", "%#{categories}%").order("created_at DESC").paginate(page: params[:page], per_page: 10)
        end
      end
    else
      if params["name"].present?
        params["name"] = params["name"].split("|") if params["is_str"].present?
        @travel_blogs = TravelBlog.includes(:customer, :blog_comments).tagged_with(params["name"], any: true).paginate(page: params[:page], per_page: 10)
      else
        @travel_blogs = TravelBlog.tagged_with(".", any: true).paginate(page: params[:page], per_page: 10)
      end
    end
    @travel_blogs = @travel_blogs.where(category: "1") if params[:filter].present? && params[:filter] == "1"
    @travel_blogs = @travel_blogs.where(category: "2") if params[:filter].present? && params[:filter] == "2"
    @travel_blogs = @travel_blogs.where(category: "3") if params[:filter].present? && params[:filter] == "3"

  end

  def single_post
    @travel_blog = TravelBlog.includes(:blog_comments).where(id: params["id"]).first
    if @travel_blog.present?
      # @travel_blog.category = JSON.parse(@travel_blog.category).reject(&:empty?) if @travel_blog.category.present?
      AppUtils.view_counter(@travel_blog)
      @comments = @travel_blog.blog_comments
      @related_posts = TravelBlog.tagged_with(@travel_blog.tag_list, :any => true)
    end
  end

  def new_post
    @travel_blog = TravelBlog.new
  end

  def create_post
    @travel_blog = TravelBlog.new(travel_blog_params)

    respond_to do |format|
      if @travel_blog.save
        DynamicJob.perform_async
        format.html { redirect_to "/blog", notice: 'Travel blog was successfully created.' }
        format.json { render :show, status: :created, location: customer_travel_blogs }
      else
        format.html { render :new }
        format.json { render json: @travel_blog.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit_post
    @travel_blog = TravelBlog.find(params[:id])
    # @travel_blog.category = JSON.parse(@travel_blog.category).reject(&:empty?) if @travel_blog.category.present?
  end

  def update_post
    @travel_blog = TravelBlog.find(params[:id])

    respond_to do |format|
      if @travel_blog.update(travel_blog_params)
        DynamicJob.perform_async
        if !params["main_old_image"].blank? && params["travel_blog"]["image"] != params["main_old_image"]
          img = Image.find(params["main_old_image"])
          img.destroy
        end
        format.html { redirect_to "/blog", notice: 'Travel blog was successfully updated.' }
        format.json { render :show, status: :created, location: customer_travel_blogs }
      else
        format.html { render :new }
        format.json { render json: @travel_blog.errors, status: :unprocessable_entity }
      end
    end
  end


  def index
    @customer = params[:username].present? ? Customer.where(username: params["username"]).first : Customer.find(params["customer_id"])
    @profile = @customer.profile
    @travel_blogs = TravelBlog.includes(:customer, :blog_comments).where(:customer_id=> @customer.id)
    render layout: "v2"
  end

  def show
    @customer = Customer.find(params["customer_id"])
    @travel_blog = @customer.travel_blogs.find(params["id"])
    @comments = @travel_blog.blog_comments
  end

  def new
    @customer = Customer.find(params["customer_id"])
    @travel_blog = @customer.travel_blogs.new
  end

  def edit
    @customer = Customer.find(params["customer_id"])
    unless @customer.travel_blogs.blank?
      @travel_blog = @customer.travel_blogs.find(params["id"])
    end
  end

  def create
    customer = Customer.find(params["customer_id"])
    @travel_blog = customer.travel_blogs.new(travel_blog_params)

    respond_to do |format|
      if @travel_blog.save
        DynamicJob.perform_async
        format.html { redirect_to customer_travel_blogs_path, notice: 'Travel blog was successfully created.' }
        format.json { render :show, status: :created, location: customer_travel_blogs }
      else
        format.html { render :new }
        format.json { render json: @travel_blog.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    customer = Customer.find(params["customer_id"])
    @travel_blog = customer.travel_blogs.find(params["id"])

    respond_to do |format|
      if @travel_blog.update(travel_blog_params)
        DynamicJob.perform_async
        if params["main_old_image"].present? && params["travel_blog"]["image"] != params["main_old_image"]
          img = Image.find(params["main_old_image"])
          img.destroy
        end
        format.html { redirect_to customer_travel_blogs_path, notice: 'Travel blog was successfully updated.' }
        format.json { render :show, status: :ok, location: customer_travel_blogs }
      else
        format.html { render :edit }
        format.json { render json: @travel_blog.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @travel_blog.destroy
    respond_to do |format|
      DynamicJob.perform_async
      format.html { redirect_to "/blog", notice: 'Travel blog was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_travel_blog
      @travel_blog = TravelBlog.find(params[:id])
      # @travel_blog.category = JSON.parse(@travel_blog.category).reject(&:empty?)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def travel_blog_params
      params.require(:travel_blog).permit(:title, :description, :customer_id, :admin_id, :image, :tag_list, :category)
    end
end
