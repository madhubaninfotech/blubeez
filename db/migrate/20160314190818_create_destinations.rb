class CreateDestinations < ActiveRecord::Migration
  def change
    create_table :destinations do |t|
      t.string :destination_name
      t.string :destination_country
      t.string :destination_state
      t.string :destination_cities
      t.string :destination_image
      t.string :destination_category
      t.float :price_from
      t.text :destination_description

      t.timestamps null: false
    end
  end
end
