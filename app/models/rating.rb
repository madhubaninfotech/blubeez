class Rating < ActiveRecord::Base
  belongs_to :flight
  belongs_to :hotel
  belongs_to :destination
  belongs_to :customer
  scope :hotel_rating_search, -> (hotel, customer) {where('hotel_id = ? AND customer_id = ?', hotel, customer)}
  scope :destination_rating_search, -> (destination, customer) {where('destination_id = ? AND customer_id = ?', destination, customer)}
  scope :event_rating_search, -> (event, customer) {where('event_id = ? AND customer_id = ?', event, customer)}
  scope :flight_rating_search, -> (flight, customer) {where('flight_id = ? AND customer_id = ?', flight, customer)}
  
  
  def self.get_default_rating(review, current_customer)
    if review.destination_id.present?
      rating = Rating.where(destination_id: review.destination_id, customer_id: current_customer.id).first
      score = rating.present? ? rating.score : 0
      url = rating.present? ? "/update_d_rating/##{rating.id}" : "/update_d_rating/0?is_new=true&destination_id=#{review.destination_id}"
    elsif review.hotel_id.present?
      rating = Rating.where(hotel_id: review.hotel_id, customer_id: current_customer.id).first
      score = rating.present? ? rating.score : 0
      url = rating.present? ? "/update_h_rating/##{rating.id}" : "/update_h_rating/0?is_new=true&hotel_id=#{review.hotel_id}"
    elsif review.flight_id.present?
      rating = Rating.where(flight_id: review.flight_id, customer_id: current_customer.id).first
      score = rating.present? ? rating.score : 0
      url = rating.present? ? "/update_f_rating/##{rating.id}" : "/update_f_rating/0?is_new=true&flight_id=#{review.flight_id}"
    end
    [score, url]
  end

end
