class AddIsPublicToCustomer < ActiveRecord::Migration
  def change
  	add_column :customers, :is_public, :integer, default: AppConstants::NO
  end
end
