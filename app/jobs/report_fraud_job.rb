class ReportFraudJob
    include SuckerPunch::Job
  
    def perform(contact)
      t1 = Time.now
      puts "============================= JOB STARTED ======================="
      AirvtrMailer.report_fraud_email(contact).deliver_now!
      puts "============================= JOB DONE ======================="
      puts "Time taken = #{Time.now - t1}"
    end
  
  end