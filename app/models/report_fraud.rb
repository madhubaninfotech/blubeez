class ReportFraud < ApplicationRecord
  validates_presence_of :name, :email, :is_registered_with_blubeez, :phone, :fraud_against_person, :fraud_against_url, :message
end
