class AddAdminToTravelBlog < ActiveRecord::Migration
  def change
    add_reference :travel_blogs, :admin, index: true, foreign_key: true
  end
end
