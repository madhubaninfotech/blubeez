class Review < ActiveRecord::Base
  belongs_to :customer
  belongs_to :hotel
  belongs_to :flight
  belongs_to :destination
  belongs_to :event
  scope :hotel_search, -> (hotel) {where('hotel_id = ?', hotel)}
  scope :destination_search, -> (destination) {where('destination_id = ?', destination)}
  scope :destination_name_search, -> (destination) {where('destination_name = ?', destination)}
  scope :flight_search, -> (flight) {where('flight_id = ?', flight)}
  scope :event_search, -> (event) {where('event_id = ?', event)}

  validates_presence_of :customer_id, :comment

  def get_review_by_type(review)
    reviews = nil
    review_type = ""
    if review.hotel_id.present?
      reviews = Review.hotel_search(review.hotel_id)
      review_type = "hotel"
    elsif review.destination_id.present?
      reviews = Review.destination_search(review.destination_id)
      review_type = "destination"
    elsif review.flight_id.present?
      reviews = Review.flight_search(review.flight_id)
      review_type = "flight"
    end
    [reviews, review_type]
  end

  def get_rating_by_type_of_rating(review)
    rating = nil
    if review.hotel_id.present?
      rating = Rating.hotel_rating_search(review.hotel_id, review.customer_id)
    elsif review.destination_id.present?
      rating = Rating.destination_rating_search(review.destination_id, review.customer_id)
    elsif review.event_id.present?
      rating = Rating.event_rating_search(review.event_id, review.customer_id)
    elsif review.flight_id.present?
      rating = Rating.flight_rating_search(review.flight_id, review.customer_id)
    end
    rating
  end

end
