module ApplicationHelper

  def is_accessable?(user_id)
    accessable = false
    if admin_signed_in?
      accessable = true
    elsif customer_signed_in?
      customer = Customer.find(user_id)
      if current_customer.id == customer.id
        accessable = true
      end
    end
    accessable
  end

  def url_formatter(url_name)
    url_name.gsub(" ", "-")
  end

  # def signed_in?
  #   id = nil
  #   if admin_signed_in?
  #     id = current_admin.id
  #   elsif customer_signed_in?
  #     id = current_customer.id
  #   end
  #   id
  # end

  def is_owner?(user_id)
    if customer_signed_in?
      customer = Customer.find(user_id)
      current_customer.id == customer.id
    elsif admin_signed_in?
      true
    end
  end

  def both_accessible?
    if signed_in?
      true
    else
      authenticate_customer!
    end
  end


  def get_image_name_from_images_array(array)
    data = JSON.parse(array)
    arr = []
    data.each do |d|
      temp = []
      temp << d
      arr << temp.to_json
    end
    images = Image.where("images in (?)", arr)
    images
  end

  def get_images_from_image_modal(array)
    data_arr = JSON.parse(array)
    images_arr = [];
    images = Image.where("id in (?)", data_arr)
    images.each do |image|
      images_arr << [image.id, image.images]
    end
    images_arr
  end

  def get_single_image_from_image_modal(img)
    images = Image.where("id = (?)", img).first
    images
  end

  def is_admin_path
    params["controller"] == "admins" && ["manage_admin", "new_admin", "update_admin", "create_admin"].include?(params["action"])
  end

  def is_user_management_path
    params["controller"] == "admins" && ["manage_users", "update_users"].include?(params["action"])
  end

  def destination_id_from_name
    dest = Destination.where(destination_name: params["destination_name"]).first
    dest.id if dest.present?
  end

  def whishlist_count
    Wishlist.where("destination_id = (?)", @destination.id).count
  end

  def checkin_count
    Checkin.where("destination_id = (?)", @destination.id).count
  end

  def unread_messages
    unread = 0
    if current_customer.present?
      unread = Conversation.joins(:messages).where("(sender_id = ? OR receiver_id = ?) and messages.read = ? and messages.customer_id != ?", current_customer.id, current_customer.id, false, current_customer.id).count
    end
    unread
  end

  def conversation_user
    @conversation.chat_user(current_customer)
  end

  def fetch_smiley
    return "" unless @customer.present? 
    case @customer.feel_type

    when Customer::FEELING_HAPPY
      image_tag "feeling-happy.png"
    when Customer::FEELING_EXCITED
      image_tag "feeling-excited.png"
    when Customer::FEELING_LOVELY
      image_tag "feeling-lovely.png"
    when Customer::FEELING_TIRED
      image_tag "feeling-tired.png"
    end
  end

  def mailer_image_url(source)
    if Rails.env.development?
      URI.join("http://localhost:3000/", image_path(source))
    else
      URI.join(root_url, image_path(source))
    end
  end

end
