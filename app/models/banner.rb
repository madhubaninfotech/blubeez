class Banner < ActiveRecord::Base
  belongs_to :admin
  mount_uploader :banner_image, ImageUploader
  has_one :temp_image, dependent: :destroy
end
