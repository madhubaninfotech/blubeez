class ConversationsController < ApplicationController
	before_action :authenticate_customer!
  layout "v2"

	def index
    @customer = current_customer
    @profile = current_customer.profile
		@users = Customer.where.not(id: current_customer.id)
    @conversations = Conversation.includes(:messages).where("sender_id = ? OR receiver_id = ?", current_customer.id, current_customer.id)
    if params[:sender_id].present? && params[:receiver_id].present? && !Conversation.between(params[:sender_id], params[:receiver_id]).present?
      Conversation.create!(conversation_params)
      @conversations = Conversation.includes(:messages).where("sender_id = ? OR receiver_id = ?", current_customer.id, current_customer.id)
    end
    conversation_users = @conversations.pluck(:sender_id, :receiver_id).flatten.uniq.select{|c| c != current_customer.id }
    @conversation_users = Customer.where(id: conversation_users)
    @messages = @conversations.map{|c| c.messages }.flatten
    if params[:notification_id].present?
      notification = UserNotification.where(id: params[:notification_id]).first
      notification.update(read_at: DateTime.now) if notification.present?
    end
	end

  def create
    if Conversation.between(params[:sender_id], params[:receiver_id]).present?
      @conversation = Conversation.between(params[:sender_id], params[:receiver_id]).first
    else
      @conversation = Conversation.create!(conversation_params)
    end

    redirect_to conversation_messages_path("en", @conversation)
  end

  private
    def conversation_params
      params.permit(:sender_id, :receiver_id)
    end
end
