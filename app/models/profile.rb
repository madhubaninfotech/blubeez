class Profile < ActiveRecord::Base
  belongs_to :Customer
  mount_uploader :avatar, CropperUploader
  # crop_uploaded :avatar
  after_update :crop_avatar

  attr_accessor :crop_x, :crop_y, :crop_w, :crop_h

  def crop_avatar
    avatar.recreate_versions! if crop_x.present?
  end
  
  def country_code
    location.present? && location.to_s.split(", ")[2] ? ISO3166::Country[location.to_s.split(", ")[2]].alpha2 : ""
  end

  def country_name
  	location.present? && location.to_s.split(", ")[2] ? ISO3166::Country[location.to_s.split(", ")[2]].name : ""
  end
  
  def state_name
    states_list = CS.states(country_code.to_sym)
    state_code = location.present? && location.to_s.split(", ")[1] ? location.to_s.split(", ")[1] : ""
    state_code.present? ? states_list[state_code.to_sym] : ""
  end
end
