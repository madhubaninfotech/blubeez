class MessagesController < ApplicationController
	layout "v2"
  before_action :authenticate_customer!

  before_action do
    @conversation = Conversation.find(params[:conversation_id])
    @customer = current_customer
    @profile = @customer.profile
  end

  def index
    @messages = @conversation.messages
    @messages.where("customer_id != ? AND `read` = ?", current_customer.id, false).update_all(read: true)

    @message = @conversation.messages.new
  end

  def create
    @message = @conversation.messages.new(message_params)
    @message.customer = current_customer

    if @message.save
      # redirect_to "/conversations/#{@conversation.id}/messages"
    end
  end

  private
    def message_params
      params.require(:message).permit(:body, :customer_id)
    end
end
