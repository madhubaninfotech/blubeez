class CreateFaclitiesToFlight < ActiveRecord::Migration
  def change
    create_table :flight_facilities do |t|
      t.string :checkin
      t.string :checkout
      t.text :cancelorprepay
      t.string :childrenbeds
      t.string :pets
      t.text :groups
      t.string :cardsaccepted
      t.belongs_to :flight, index: true, foreign_key: true
    end
  end
end
