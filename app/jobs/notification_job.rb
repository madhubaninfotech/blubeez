class NotificationJob
  include SuckerPunch::Job
  workers 4

  def perform(data, type, cus_id)
    t1 = Time.now
    puts "============================= JOB STARTED ======================="
    Notification.sync_notification(data, type, cus_id)
    puts "============================= JOB DONE ======================="
    puts "Time taken = #{Time.now - t1}"
  end
end