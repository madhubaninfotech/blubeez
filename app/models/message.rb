class Message < ApplicationRecord
  belongs_to :conversation
  belongs_to :customer

  validates_presence_of :body, :conversation_id, :customer_id
  after_save :update_notification

  def update_notification
  	UserNotification.create({
  		recipient_id: receiver,
  		friend_id: customer_id,
  		action: "Message",
  		notifiable: self 
  	})
  end

  def receiver
		([conversation.sender_id, conversation.receiver_id] - [customer_id])[0]
  end

  private
    def message_time
      created_at.strftime("%d/%m/%y at %l:%M %p")
    end
end
