class AdminsController < ApplicationController
	before_filter :authenticate_admin!, :except=> [:terms, :faq, :privacy, :about_us, :careers, :edit_terms, :edit_about_us, :edit_faq, :edit_privacy, :edit_careers, :update_terms, :update_faq, :update_privacy, :update_careers, :update_about_us]
  layout "v2_admin"

	def manage_users
		@customers = Customer.all	
	end

	def update_users
		customer = Customer.find(params["user_id"])
		if params[:user][:status].to_i == AppConstants::ACTIVE
			customer.user_status = 1
			if customer.save!
				flash[:notice] = "#{customer.name} updated to Active"
			else
				flash[:error] = customer.errors
			end
		elsif params[:user][:status].to_i == AppConstants::INACTIVE
			customer.user_status = 0
			if customer.save!
				flash[:notice] =  "#{customer.name} updated to In Active"
			else
				flash[:error] = customer.errors
			end
		elsif params[:user][:status].to_i == AppConstants::DELETE
			if customer.destroy!
				flash[:notice]  = "#{customer.name} Deleted Succesfully"
			else
				flash[:error] = customer.errors
			end
		end

	end

	def manage_admin
		@admins = Admin.where("is_admin != ?", true)
	end

	def new_admin
	end

  def create_admin
		@admin = Admin.new(admin_params)
		if @admin.save
			flash.now[:notice] = "Successfully Added"
			redirect_to manage_admin_path
		else
			flash.now[:error] = @admin.errors
		end
	end

	def update_admin
		customer = Admin.find(params["id"])
		if params[:admin][:is_active].to_i == AppConstants::ACTIVE
			customer.is_active = 1
			if customer.save!
				flash[:notice] = "#{customer.name} updated to Active"
			else
				flash[:error] = customer.errors
			end
		elsif params[:admin][:is_active].to_i == AppConstants::INACTIVE
			customer.is_active = 0
			if customer.save!
				flash[:notice] =  "#{customer.name} updated to In Active"
			else
				flash[:error] = customer.errors
			end
		elsif params[:admin][:is_active].to_i == AppConstants::DELETE
			if customer.destroy!
				flash[:notice]  = "#{customer.name} Deleted Succesfully"
			else
				flash[:error] = customer.errors
			end
		end
		redirect_to manage_admin_path
	end

	def terms
		@terms = Term.first
	end

	def faq
		@faq = Faq.first
	end

	def privacy
		@privacy = Privacy.first
	end

	def about_us
		@about = AboutUs.first
	end

	def careers
		@career = Career.first
	end

	def edit_terms
		@terms = Term.first
	end

	def edit_about_us
		@about = AboutUs.first
	end

	def edit_faq
		@faq = Faq.first
	end

	def edit_privacy
		@privacy = Privacy.first
	end

	def edit_careers
		@career = Career.first
	end

	def update_terms
		@terms = Term.first
		@terms.terms = params["term"]["terms"]
		if @terms.save
			flash[:notice] = "Terms updated Successfully"
			redirect_to "/terms"
		else
			flash[:error] = @terms.errors
		end
	end

	def update_faq
		@faq = Faq.first
		@faq.faq = params["faq"]["faq"]
		if @faq.save
			flash[:notice] = "FAQ Successfully"
			redirect_to "/faq"
		else
			flash[:error] = @faq.errors
		end
	end

	def update_privacy
		@privacy = Privacy.first
		@privacy.privacy = params["privacy"]["privacy"]
		if @privacy.save
			flash[:notice] = "Privacy updated Successfully"
			redirect_to "/privacy"
		else
			flash[:error] = @privacy.errors
		end
	end

	def update_careers
		@career = Career.first
		@career.careers = params["career"]["careers"]
		if @career.save
			flash[:notice] = "Careers updated Successfully"
			redirect_to "/careers"
		else
			flash[:error] = @career.errors
		end
	end

	def update_about_us
		@about = AboutUs.first
		@about.about = params["about_us"]["about"]
		if @about.save
			flash[:notice] = "About us updated Successfully"
			redirect_to "/about"
		else
			flash[:error] = @about.errors
		end
	end
  
end