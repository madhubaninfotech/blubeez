class Customers::ConfirmationsController < Devise::ConfirmationsController
  layout "auth"
  # GET /resource/confirmation/new
  def new
    super
  end

  # POST /resource/confirmation
  def create
    super
  end

  # GET /resource/confirmation?confirmation_token=abcdef
  def show
    super
  end

  # protected

  # The path used after resending confirmation instructions.
  # def after_resending_confirmation_instructions_path_for(resource_name)
  #   super(resource_name)
  # end

  # The path used after confirmation.
  def after_confirmation_path_for(resource_name, resource)
    CustomerMailer.welcome_email(resource).deliver!
    sign_in(resource)
    "/customers/#{resource.id}/edit_profile"
  end
end
