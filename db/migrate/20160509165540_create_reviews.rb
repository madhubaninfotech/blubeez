class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.string :name
      t.text :comment
      t.belongs_to :customer, index: true, foreign_key: true
      t.belongs_to :hotel, index: true, foreign_key: true
      t.belongs_to :flight, index: true, foreign_key: true
      t.belongs_to :destination, index: true, foreign_key: true
      t.belongs_to :event, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
