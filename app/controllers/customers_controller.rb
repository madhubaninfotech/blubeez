require 'will_paginate/array'
class CustomersController < ApplicationController
	before_action :both_accessible?, except: [:meet_travellers, :search_traveler]
	def finish_signup
	    # authorize! :update, @customer 
	    if request.patch? && params[:customer] #&& params[:customer][:email]
	      if @customer.update(customer_params)
	        @customer.skip_reconfirmation!
	        sign_in(@customer, :bypass => true)
	        redirect_to @customer, notice: 'Your profile was successfully updated.'
	      else
	        @show_errors = true
	      end
	    end
	    redirect_to root_path
	end

	def show
		@customer = Customer.find (params[:id])
		render "user"
	end

	def traveler
		@customer = Customer.where(username: params[:username]).first
		@profile = @customer.profile
		if @profile.nil?
			return redirect_to "/customers/#{@customer.id}/edit_profile"
		else
      @profile.d_o_b = @profile.d_o_b.to_date.strftime("%d/%m/%Y") if !@profile.d_o_b.nil?
      unless @profile.location.nil? 
        @location = AppUtils.location_formatter_parse(@profile.location)
      else
        @location = Hash.new
      end
    end
    
		render layout: "v2"
	end

	def friends
		@customer = Customer.find (params[:customer_id])
		@profile = @customer.profile
		friends = @customer.friends
		unless friends.nil?
			friends_result = Friend.get_friends_data_by_id_hash(friends)
			@friends = Customer.where(:id=>friends_result)
		end
		render layout: "v2"
	end

	def all_users
		@all_customers = Customer.all
		customer = Customer.find (current_customer.id)
		@friends = customer.friends
		@customers = AppUtils.other_than_friends_sorter(@all_customers, @friends, customer.id)
	end

	def meet_travellers
		customer_id = customer_signed_in? ? current_customer.id : ""
		unless params["search_data"].present?
      @customers = Customer.includes(:profile).where("id != ?", customer_id.to_i).where.not(id: customer_id.to_i)
    else
      @customers = Customer.includes(:profile).where(:name=> params["name"].to_s).where.not(id: customer_id.to_i)
    end
	@customers = @customers.map {|a| a if a.profile != nil}
	@customers = @customers.compact.paginate(page: params[:page], per_page: 10)
	render layout: "v2"
	end

	def add_users
		if customer_signed_in?
			customer = current_customer
			unless customer.is_friend?(params[:customer_id])
				friend = customer.friends.new
				friend.customer_id = customer.id
				friend.friend_id = params[:customer_id]
				friend.name = params[:name]
				if friend.save!
					flash[:success] = "friend has been added"
					@customer = customer
				else
					flash[:error] = "Soemting went wrong"
				end
			end
		else
			flash[:error] = "Sign in Required"
			redirect_to new_customer_session_path
		end
	end

	def follow
		if customer_signed_in?
			customer = current_customer
			user = Customer.find params[:customer_id]
			customer.follow(user)
			UserNotification.create({
	  		recipient_id: user.id,
	  		friend_id: customer.id,
	  		action: "Follows",
	  		notifiable: customer 
	  	})
			flash[:success] = "Followed"
		else
			flash[:error] = "Sign in Required"
			redirect_to new_customer_session_path
		end
	end

	def unfollow
		if customer_signed_in?
			customer = current_customer
			user = Customer.find params[:customer_id]
			customer.stop_following(user)
			flash[:success] = "Unfollowed"
		else
			flash[:error] = "Sign in Required"
			redirect_to new_customer_session_path
		end
	end

  def disconnect_users
		@customer = Customer.find(params["customer_id"])
		friend = @customer.friends.where(friend_id: params["id"]).first
		if friend.destroy!
			flash.now[:success] = "Successfully disconected"
		else
			flash.now[:error] = friend.errors
		end
	end

	def update_visibility
		current_customer.is_public = params["checked"] == "true" ? AppConstants::YES : AppConstants::NO
		current_customer.save
	end

	def posts
		@customer = Customer.find(params["customer_id"])
		@profile = @customer.profile
		@posts = @customer.posts.includes(:post_images).order("created_at DESC")
		render layout: "v2"
	end

	def edit_post
		@customer = current_customer
		@profile = @customer.profile
		@post = Post.where(id: params[:id]).first
		render layout: "v2"
	end
	
	def show_post
		@customer = current_customer
		@profile = @customer.profile
		@post = Post.where(id: params[:id]).first
		render layout: "v2"
	end

	def update_post
		@customer = current_customer
		@profile = @customer.profile
		@post = Post.where(id: params[:id]).first
		if @post.update(post_params)
			delete_images
			upload_images if params[:post_images].present?
		end
	end

	def gallery
		@customer = Customer.find(params["customer_id"])
		@profile = @customer.profile
		@posts = @customer.posts.includes(:post_images).order("created_at DESC")
		@posts_by_country = @posts.group_by{|p| p.country }
		render layout: "v2"
	end

	def followings
		@customer = Customer.find(params["customer_id"])
		@profile = @customer.profile
		@friends = @customer.all_following
		render layout: "v2"
	end

	def followers
		@customer = Customer.find(params["customer_id"])
		@profile = @customer.profile
		@friends = @customer.followers
		render layout: "v2"
	end

	def notifications
		@customer = current_customer
		@profile = @customer.profile
		@notifications = @customer.user_notifications
		render layout: "v2"
	end

	def create_post
		@customer = current_customer
		@profile = @customer.profile
		@post = @customer.posts.new
	end

	def save_post
		if params[:post_images].present?
			@post = Post.new(post_params)
			if @post.save
				flash[:notice] = "Posted!"
				upload_images if params[:post_images].present?
			end
		else
			flash[:error] = "Image must be there"
		end
	end

	def comment
		@comment = Comment.new(comment_params)
		@comment.save
		@comments = Comment.where(post_id: params[:id])
	end

	def like
		@post = Post.where(id: params[:id]).first
		if current_customer.voted_for?(@post)
			@post.unliked_by current_customer
		else
			@post.liked_by current_customer
		end
	end

  def likes
    @customer = Customer.find(params[:customer_id])
    @profile = @customer.profile
    @likes = @customer.find_voted_items.pluck(:id)
    @posts = Post.includes(:post_images).where(id: @likes).order("created_at DESC")
	@likes_data = @customer.find_voted_items
    render layout: "v2"
  end

  def reviews
    @customer = Customer.find(params[:customer_id])
    @profile = @customer.profile
    post_ids = @customer.comments.pluck(:post_id)
    @posts = Post.includes(:post_images).where(id: post_ids).order("created_at DESC")
	@each_comment = current_customer.comments
    render layout: "v2"
  end

  def recommendation
	render layout: "v2"
  end

	def remove_post
		@post = Post.where(id: params[:id]).first
		@post.destroy
	end

	def search_traveler
		customer_id = customer_signed_in? ? current_customer.id : ""
		if params["search"].present?
			# customers = Customer.search params["search"], where: {
			# 	_or: [{id: !customer_id }]
			# }
			# @customers = customers.each{|customer|  customer }
			@customers = Customer.includes(:profile).where("username like (?) or first_name like (?) or last_name like (?) or email like (?)", "%#{params["search"].to_s}%", "%#{params["search"].to_s}%", "%#{params["search"].to_s}%", "%#{params["search"].to_s}%").where.not(id: customer_id.to_i)
		else
      @customers = Customer.includes(:profile).where.not(id: customer_id.to_i)
		end
	end

	def gallery_by_country
		@customer = Customer.find(params["customer_id"])
		@profile = @customer.profile
		@gallery_images = @customer.post_images.where("posts.country = ?", params[:country])
	end

	def change_status
	end

	def update_status
		customer = current_customer
		customer.feel_type = params[:feeling]
		if Customer::FEELING_OTHERS == params[:feeling].to_i
			customer.feel_type = params[:feeling]
			customer.feel_msg = params[:feel_msg]
		end
		customer.save
		@customer = current_customer
	end

  def collections
    @customer = Customer.find(params["customer_id"])
		@profile = @customer.profile
    @wishlst_count = Wishlist.where(customer_id: params[:customer_id]).count
    @likes_count = @customer.find_voted_items.count
	@checkin_count = @customer.checkins.count
	@reviews_count = @customer.reviews.count
		render layout: "v2"
  end

  def validate_username
  	render json: Customer.where("username = ?", params["username"].to_s).count
  end


	private
	def set_customer
		@customer = Customer.find(params[:id])
	end

	def customer_params
		accessible = [ :name, :email ] # extend with your own params
		accessible << [ :password, :password_confirmation ] unless params[:customer][:password].blank?
		params.require(:customer).permit(accessible)
	end

	def post_params
		params.require(:post).permit(:name, :address, :customer_id, :state, :city, :country, post_images: [:id, image: []])
	end

	def upload_images
		params[:post_images].each do |img|
			PostImage.create({
				image: img,
				post: @post
			})
		end
	end

	def delete_images
		img_ids = @post.post_images.pluck(:id)
		if img_ids.present?
			if params[:preloaded].present?
				needs_to_delete = img_ids - params[:preloaded].map(&:to_i)
				PostImage.where(id: needs_to_delete).destroy_all if needs_to_delete.present?
			else
				flash[:error] = "You can not delete all the post images!"
			end
		end
	end

	def comment_params
		params.require(:comment).permit(:comment, :post_id, :customer_id)
	end

end
