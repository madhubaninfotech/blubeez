class BannersController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_banner, only: [:show, :edit, :update, :destroy]
  layout "v2_admin"

  # GET /banners
  # GET /banners.json
  def index
    @admin = current_admin
    @banners = @admin.banners.all
  end

  # GET /banners/new
  def new
    @admin = Admin.find(params["admin_id"])
    @banner = @admin.banners.new
  end

  # GET /banners/1/edit
  def edit
    @admin = Admin.find(params["admin_id"])
    @banner = @admin.banners.find(params["id"])
  end

  # POST /banners
  # POST /banners.json
  def create
    @admin = Admin.find(params["admin_id"])
    @banner = @admin.banners.new(banner_params)

    respond_to do |format|
      if @banner.save
        format.html { redirect_to admin_banners_path, notice: 'Banner was successfully created.' }
        format.json { render :show, status: :created, location: @banner }
      else
        format.html { render :new }
        format.json { render json: @banner.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /banners/1
  # PATCH/PUT /banners/1.json
  def update
    @admin = Admin.find(params["admin_id"])
    @banner = @admin.banners.find(params[:id])

    respond_to do |format|
      if @banner.update(banner_params)
        format.html { redirect_to admin_banners_path, notice: 'Banner was successfully updated.' }
        format.json { render :show, status: :ok, location: @banner }
      else
        format.html { render :edit }
        format.json { render json: @banner.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /banners/1
  # DELETE /banners/1.json
  def destroy
    @banner.destroy
    respond_to do |format|
      format.html { redirect_to admin_banners_path, notice: 'Banner was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_banner
      @banner = Banner.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def banner_params
      params.require(:banner).permit(:banner_image, :admin_id, :title, :desc, :button_text, :button_link)
    end
end
