require File.expand_path('../boot', __FILE__)

require 'rails/all'
require 'mail'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Airvtr
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true

    config.middleware.use Rack::Deflater
  end
end

class Array
  # Use this function only for Two Dimensional Arrays
  def get_by_key_from_2d_array(idx)
    each {|x| return x[0] if (x[1] == idx)} 
    return ""
  end
 
  def get_kv_hash
    _hash = {}
    each {|val, key| _hash[key] = val}
    _hash
  end
  
  def get_value_by_key_from_2d_array(key)
    each {|x| return x[1] if (x[0] == key)}
    return ""
  end
  def get_second_value_by_key_from_2d_array(key)
    each {|x| return x[2] if (x[0] == key)}
    return ""
  end
  def get_third_value_by_key_from_2d_array(key)
    each {|x| return x[3] if (x[0] == key)}
    return ""
  end
  def get_fourth_value_by_key_from_2d_array(key)
    each {|x| return x[4] if (x[0] == key)}
    return ""
  end
  def get_fifth_value_by_key_from_2d_array(key)
    each {|x| return x[5] if (x[0] == key)}
    return ""
  end

  def get_6th_value_by_key_from_2nd_array(key)#booking_type
    each {|x| return x[6] if (x[0] == key)}
    return ""
  end

  def get_7th_value_by_key_from_2nd_array(key)#created_by
    each {|x| return x[7] if (x[0] == key)}
    return ""
  end

  def get_8th_value_by_key_from_2nd_array(key)
    each {|x| return x[8] if (x[0] == key)}
    return ""
  end

  def get_9th_value_by_key_from_2nd_array(key)
    each {|x| return x[9] if (x[0] == key)}
    return ""
  end

  def get_nth_value_by_key_from_2nd_array(key, n)
    each {|x| return x[n] if (x[0] == key)}
    return ""
  end

  def contains_all? other  
    other = other.dup    
    each{|e| if i = other.index(e) then other.delete_at(i) end}    
    other.empty?    
  end
end