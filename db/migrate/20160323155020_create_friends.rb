class CreateFriends < ActiveRecord::Migration
  def change
    create_table :friends do |t|
      t.string :name
      t.integer :friend_id
      t.belongs_to :customer, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
