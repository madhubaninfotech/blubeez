class EventsController < ApplicationController
  before_action :authenticate_admin!, except: [:index, :show, :all_index]
  before_action :set_event, only: [:edit, :update, :destroy]
  add_breadcrumb "Destination", "/destinations"
  add_breadcrumb "Flights", "/flights"

  # GET /events
  # GET /events.json
  def index
    @dest = Destination.where(slug: params["slug"]).first
    @events = @dest.events.all
  end

  def all_index
    unless params["search_data"].present?
      if params[:sortby].present?
        if params[:sortby].to_i == AppConstants::SORT_BY_POPULARITY #TODO need to fix for ratings and popularity
          # @events = Event.includes(:ratings).order("view_count DESC").paginate(page: params[:page], per_page: 10)
        elsif params[:sortby].to_i == AppConstants::SORT_BY_RATING
          # @events = Event.includes(:ratings).order("ratings.score DESC").paginate(page: params[:page], per_page: 10)
        else
          # @events = Event.all.order("created_at DESC").paginate(page: params[:page], per_page: 10)
        end
        @events = Event.all.order("created_at DESC").paginate(page: params[:page], per_page: 10)
      else
        @events = Event.all.order("created_at DESC").paginate(page: params[:page], per_page: 10)
      end
    else
      @events = Event.where(:name=> params["name"])
    end
  end

  # GET /events/1
  # GET /events/1.json
  def show
    @event = Event.where(slug: params[:event_slug]).first
    @destination = @event.destination
  end

  # GET /events/new
  def new
    @is_with_location = params["add_location"].present? ? true : false
    unless @is_with_location
      dest = Destination.where(id: params["destination_id"]).first
      @event = dest.events.new
    else
      @event = Event.new
    end
    
    #add_breadcrumb "New Event", new_destination_event_path(params["destination_id"]), "new event"
  end

  # GET /events/1/edit
  def edit
    dest = Destination.where(id: params["destination_id"]).first
    unless dest.events.blank?
      @event = dest.events.find(params["id"])
    end
    # add_breadcrumb "Edit Event", new_destination_event_path(params["destination_id"]), "Edit"
  end

  # POST /events
  # POST /events.json
  def create

    @add_location = params["add_location"].present? ? true : false

    unless @add_location
      dest = Destination.where(id: params["destination_id"]).first
    else
      if params["locations"].present?
        if params["locations"]["country"].present? && params["locations"]["state"].present? && params["locations"]["cities"].present? && params["locations"]["name"].present?
          dest = AppUtils.get_location_details_for_flight_or_hotel(params["locations"]).first
        else
          redirect_to "/add_events/?add_location=true", :flash=> {:error=>"Enter valid location"}
        end
      else
        redirect_to "/add_events/?add_location=true", :flash=> {:error=>"Enter valid location"}
      end
    end

    @event = dest.events.new(event_params)

    respond_to do |format|
      if @event.save
        DynamicJob.perform_async
        format.html { redirect_to "/events", notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    dest = Destination.where(id: params["destination_id"]).first
    @event = dest.events.find(params["id"])
    respond_to do |format|
      if @event.update(event_params)
        DynamicJob.perform_async
        # unless params["event"]["image"] == params["main_old_image"]
        #   img = Image.find(params["main_old_image"])
        #   img.destroy
        # end
        format.html { redirect_to "/events", notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      DynamicJob.perform_async
      format.html { redirect_to destination_events_path, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:name, :image, :about, :detail, :date, :destination_id, :start_date, :end_date, :slug)
    end
end