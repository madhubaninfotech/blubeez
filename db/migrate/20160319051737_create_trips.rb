class CreateTrips < ActiveRecord::Migration
  def change
    create_table :trips do |t|
      t.string :trip_name
      t.string :location
      t.text :trip_description
      t.string :trip_photos
      t.datetime :start_date
      t.datetime :end_date
      t.integer :trip_status
      t.belongs_to :customer, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
