class CreateFlights < ActiveRecord::Migration
  def change
    create_table :flights do |t|
      t.string :name
      t.string :service_no
      t.integer :price
      t.string :image
      t.belongs_to :destination, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
