class RatingsController < ApplicationController

  def update_destination
    if params["is_new"].present?
      @rating = Rating.create(destination_id: params["destination_id"], customer_id: current_customer.id, score: params["score"].to_i)
    else
      @rating = Rating.find(params[:id])
      if @rating.update_attributes(score: params[:score])
        respond_to do |format|
          format.js
        end
      end
    end
    @destination = @rating.destination
  end

  def update_flight
    if params["is_new"].present?
      @rating = Rating.create(flight_id: params["flight_id"], customer_id: current_customer.id, score: params["score"].to_i)
    else
      @rating = Rating.find(params[:id])
      if @rating.update_attributes(score: params[:score])
        respond_to do |format|
          format.js
        end
      end
    end
    @flight = @rating.flight
  end

  def update_hotel
    if params["is_new"].present?
      @rating = Rating.create(hotel_id: params["hotel_id"], customer_id: current_customer.id, score: params["score"].to_i)
    else
      @rating = Rating.find(params[:id])
      if @rating.update_attributes(score: params[:score])
        respond_to do |format|
          format.js
        end
      end
    end
    @hotel = @rating.hotel
  end

end