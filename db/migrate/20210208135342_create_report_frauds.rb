class CreateReportFrauds < ActiveRecord::Migration[5.0]
  def change
    create_table :report_frauds do |t|
      t.string :name
      t.string :email
      t.boolean :is_registered_with_blubeez
      t.string :phone
      t.string :fraud_against_person
      t.string :fraud_against_url
      t.text :message
      t.boolean :is_action_taken, default: false

      t.timestamps
    end
  end
end
