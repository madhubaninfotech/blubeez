class VideoUploader < CarrierWave::Uploader::Base
  include CarrierWave::Video

  # process encode_video: [:mp4, callbacks: { after_transcode: :set_success } ]
  
  version :mp4 do
   	process :encode_video=> [:mp4, audio_codec: "libfdk_aac"]#,:custom => "-strict experimental -q:v 5 -preset slow -g 30"]
  end

end
