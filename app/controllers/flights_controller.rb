class FlightsController < ApplicationController
  before_action :authenticate_admin!, except: [:index, :show, :all_index]
  before_action :set_flight, only: [:edit, :update, :destroy]
  add_breadcrumb "Destination", "/destinations"
  add_breadcrumb "Flights", "/flights"  
  # GET /flights
  # GET /flights.json
  def index
    dest = Destination.where(slug: params["slug"]).first
    @flights = dest.flights.all
  end

  def all_index
    unless params["search_data"].present?
      if params[:sortby].present?
        if params[:sortby].to_i == AppConstants::SORT_BY_POPULARITY
          @flights = Flight.includes(:ratings).all.order("view_count DESC")
        elsif params[:sortby].to_i == AppConstants::SORT_BY_RATING
          @flights = Flight.includes(:ratings).all.order("ratings.score DESC")
        else
          @flights = Flight.includes(:ratings).all.order("created_at DESC")
        end
      else
        @flights = Flight.includes(:ratings).all
      end
    else
      @flights = Flight.includes(:ratings).flight_name_search(params["name"])
    end
  end

  # GET /flights/1
  # GET /flights/1.json
  def show
    flight = Flight.where(slug: params["flight_slug"]).first
    AppUtils.view_counter(flight)
    @flight_facility = flight.flight_facility
    @reviews = Review.flight_search(params[:id])
  end

  # GET /flights/new
  def new
    @is_with_location = params["add_location"].present? ? true : false
    unless @is_with_location
      dest = Destination.where(id: params["destination_id"]).first
      @flight = dest.flights.new
      # add_breadcrumb "New Flights", new_destination_flight_path(params["destination_id"]), "new Flights"
    else
      @flight = Flight.new
    end
  end

  # GET /flights/1/edit
  def edit
    dest = Destination.where(id: params["destination_id"]).first
    unless dest.flights.blank?
      @flight = dest.flights.where(id: params["id"]).first
    end
    # add_breadcrumb "Edit Flights", new_destination_flight_path(params["destination_id"]), "Edit"
  end

  # POST /flights
  # POST /flights.json
  def create
    @add_location = params["add_location"].present? ? true : false

    unless @add_location
      dest = Destination.where(id: params["destination_id"]).first
    else
      if params["locations"].present?
        if params["locations"]["country"].present? && params["locations"]["state"].present? && params["locations"]["cities"].present? && params["locations"]["name"].present?
          dest = AppUtils.get_location_details_for_flight_or_hotel(params["locations"]).first
        else
          redirect_to "/add_flights/?add_location=true", :flash=> {:error=>"Enter valid location"}
        end
      else
        redirect_to "/add_flights/?add_location=true", :flash=> {:error=>"Enter valid location"}
      end
    end

    @flight = dest.flights.new(flight_params)

    respond_to do |format|
      if @flight.save
        DynamicJob.perform_async
        format.html { redirect_to "/flights", notice: 'Flight was successfully created.' }
        format.json { render :show, status: :created, location: @flight }
      else
        format.html { render :new }
        format.json { render json: @flight.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /flights/1
  # PATCH/PUT /flights/1.json
  def update
    dest = Destination.where(id: params["destination_id"]).first
    @flight = dest.flights.find(params["id"])
    respond_to do |format|
      if @flight.update(flight_params)
        DynamicJob.perform_async
        # unless params["flight"]["main_image"] == params["main_old_image"]
        #   img = Image.find(params["main_old_image"])
        #   img.destroy
        # end
        format.html { redirect_to "/flights", notice: 'Flight was successfully updated.' }
        format.json { render :show, status: :ok, location: @flight }
      else
        format.html { render :edit }
        format.json { render json: @flight.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /flights/1
  # DELETE /flights/1.json
  def destroy
    @flight.destroy
    respond_to do |format|
      DynamicJob.perform_async
      format.html { redirect_to flights_url, notice: 'Flight was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_flight
      @flight = Flight.where(id: params[:id]).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def flight_params
      params.require(:flight).permit(:name, :service_no, :price, :destination_id, :image, :main_image, :flight_to, :slug)
    end
end
