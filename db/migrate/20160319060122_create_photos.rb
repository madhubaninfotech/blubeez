class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.string :trip_name
      t.string :location
      t.text :trip_description
      t.string :attachments, array: true, default: []
      t.string :videos
      t.datetime :date
      t.belongs_to :customer, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
