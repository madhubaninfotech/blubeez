class AddAmenitiesToHotels < ActiveRecord::Migration
  def change
    add_column :hotels, :amenities, :string, array: true, default: []
  end
end
