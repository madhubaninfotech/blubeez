class TravelBlog < ActiveRecord::Base
  belongs_to :customer
  belongs_to :admin
  has_one :temp_image, dependent: :destroy
  has_many :blog_comments, dependent: :destroy
  # mount_uploader :image, ImageUploader
  acts_as_taggable
  scope :name_search, ->(s_name) { where('title like ?' ,s_name) }
  scope :single_post_query, ->(post_params){
  	joins("LEFT JOIN blog_comments on travel_blogs.id = blog_comments.travel_blog_id").joins("LEFT JOIN customers on customers.id = travel_blogs.customer_id")
  	.select("travel_blogs.*, blog_comments.id as comment_id, blog_comments.name as comment_name, blog_comments.comment as comment_desc, blog_comments.reply_for as reply_for, blog_comments.customer_id as commentor_id, customers.id as customer_id, customers.name as customer_name")
  	.group("blog_comments.id")
  	.where(id: post_params["id"])
  }


  def self.single_post
  	
  end

  def author_image
    customer.profile.present? && customer.profile.avatar.present? ? customer.profile.avatar.url  : ""
  end

  def author_name
    customer.first_name.present? || customer.last_name.present? ? "#{customer.first_name} #{customer.last_name}" : customer.email
  end

end
