//= require v2/jquery.min
//= require jquery_ujs
//= require turbolinks
//= require jquery.remotipart
//= require v2/jquery-migrate-3.0.1.min
//= require v2/ionicicons
//= require jquery-ui.js
//= require chosen.jquery.js
//= require v2/image-uploader.js
//= require v2/popper.min
//= require v2/bootstrap.min
//= require v2/jquery.easing.1.3
//= require v2/jquery.waypoints.min
//= require v2/jquery.stellar.min
//= require v2/owl.carousel.min
//= require v2/jquery.magnific-popup.min
//= require v2/masonry
//= require v2/aos
//= require v2/slick
//= require alertify
//= require jssocials
//= require jcrop
//= require jquery.raty

//= require jquery.lazyload.js
//= require jquery.fancybox.js

//https://github.com/wedgies/jquery-emoji-picker
//= require jquery.emojis
//= require jquery.emojipicker

//= require jquery.validate.min
//= require v2/jquery.animateNumber.min
//= require v2/scrollax.min

//= require jquery-fileupload


//= require web
//= require v2/main

