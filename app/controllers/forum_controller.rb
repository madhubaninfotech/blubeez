class ForumController < ApplicationController
  # theme "v2"

  def index
    if params["sortby"].present?
      @topics = Topic.filter_topics(params)
    else
      @topics_by_country = Topic.search_topic_by_country
      @topics = Topic.includes(:customer, :post_topics).last(100)
      @recent_topics = @topics.last(5)
      @topics_countries = @topics.map(&:location_country).uniq.reject{ |c| c.empty? }
    end
    add_breadcrumb "Forum", "/forums"
    render layout: "v2"
  end

  def country
    @topics = Topic.includes(:post_topics, :customer).where(location_country:params["country_name"]).paginate(page: params[:page], per_page: 10)
    add_breadcrumb "Forum", "/forum"
    if ISO3166::Country[params["country_name"]].present?
      region = ISO3166::Country[params["country_name"]].continent
      region_code = AppUtils.get_region_code(region)
      add_breadcrumb "#{region}", "/forum/#{region_code}"
      country_name = AppUtils.country_name_by_country_code(params["country_name"])
      add_breadcrumb "#{country_name}", "#{params["country_name"]}"
    else
      return render :html=> "<h4 class='text-center'>Not Found</h4>".html_safe, layout: "application"
    end
    render layout: "v2"
  end
end