class AddMainImageToAttraction < ActiveRecord::Migration
  def up
    add_column :attractions, :main_image, :string
    change_column :attractions, :image, :text, array: true, :default => nil, :null => true
  end
end
