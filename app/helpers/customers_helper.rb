module CustomersHelper

	def notification_message(notification)
		case notification.action
		when "Message"
			link_to "#{notification.friend.fullname} has sent a Message to you", "/conversations?sender_id=#{notification.friend.id}&receiver_id=#{notification.recipient.id}&notification_id=#{notification.id}", remote: true
		when "Follows"
			link_to "#{notification.friend.fullname} has followed you", "/customers/#{notification.friend.id}/profile?notification_id=#{notification.id}"
		else
		end
	end

end