class CreateFacilities < ActiveRecord::Migration
  def change
    create_table :hotel_facilities do |t|
      t.string :view
      t.text :activities
      t.text :mediatechnology
      t.text :food
      t.string :internet
      t.string :parking
      t.text :services
      t.text :general
      t.text :languages
      t.references :hotel, index: true, foreign_key: true
    end
  end
end
