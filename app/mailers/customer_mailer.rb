class CustomerMailer < ApplicationMailer
	helper :application
  # default from: 'mkmmslearn@gmail.com'
  # CustomerMailer.welcome_email
  layout "v2_mailer"
  
  def welcome_email(customer)
    @customer = customer
    @url  = 'https://blubeez.com/'
    mail(to: @customer.email, subject:"Welcome To blubeez")
  end

  def inactivity_email(customer)
  	@customer = customer
    @url  = 'https://blubeez.com/'
    mail(to: @customer.email, subject:"Inactivity")
  end

end
