class CreateTopics < ActiveRecord::Migration
  def change
    create_table :topics do |t|
      t.string :title
      t.text :content
      t.boolean :safe
      t.boolean :closed
      t.boolean :trusted
      t.belongs_to :customer, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
