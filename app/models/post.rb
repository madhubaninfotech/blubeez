class Post < ApplicationRecord
  belongs_to :customer
  has_many :post_images, dependent: :destroy
  has_many :comments, dependent: :destroy

  acts_as_votable

  # accepts_nested_attributes_for :post_images
  
  attr_accessor :location

  def get_likes(customer)
  	ActsAsVotable::Vote.where("votable_type = 'Post' and voter_id = ? and votable_id = ?", customer.id, self.id).count
  end
end
