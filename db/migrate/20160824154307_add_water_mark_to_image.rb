class AddWaterMarkToImage < ActiveRecord::Migration
  def change
    add_column :images, :is_watermark_enabled, :boolean
  end
end
