class Event < ActiveRecord::Base
  belongs_to :destination
  # mount_uploader :image, ImageUploader
  has_many :reviews, dependent: :destroy
  has_one :temp_image, dependent: :destroy
  validates_uniqueness_of :slug
  validates_presence_of :slug
  before_save :check_slug

  def check_slug
    self.slug = self.slug.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
  end

end
