class AddUsernameToCustomer < ActiveRecord::Migration[5.0]
  def change
    add_column :customers, :username, :string
    add_index :customers, :username
    add_index :customers, :first_name
    add_index :customers, :last_name
  end
end
