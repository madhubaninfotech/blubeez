Rails.application.routes.draw do
  if Rails.env.production?
    default_url_options :host => "https://blubeez.com"
  else
    default_url_options :host => "http://localhost:3000"
  end
  # require 'sidekiq/web'

  # mount Sidekiq::Web, at: '/sidekiq'
  match "es" => "home#locale_index", :via => [:get], :defaults => {:locale => 'es'}
  match "en" => "home#locale_index", :via => [:get], :defaults => {:locale => 'en'}

  devise_for :admins, :skip => [:registrations], :controllers=> {sessions: "admins/sessions"}
  devise_for :customers, controllers: { 
    registrations: 'customers/registrations', 
    sessions: "customers/sessions" , 
    omniauth_callbacks: 'omniauth_callbacks',
    passwords: "customers/passwords",
    confirmations: "customers/confirmations" 
  }, path_names: { sign_in: 'signin', sign_up: 'signup'}
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".
  get '/s/create', to: 'short_links#create'
  get '/s/:slug', to: 'short_links#show', as: :short


  # post "images/create" => "images#create"
  resources :images, :only => [:index, :create]
  delete "/images" => "images#destroy"
  get "/multiple_image" => "images#multiple_index"
  post "/multiple_image" => "images#create_multiple"
  post "/images/update_description" => "images#update_description"


  get "/settings" => "banners#index" 
  post "/settings" => "settings#update_settings"


  resources :admin, :only=> [:show] do
    resources :banners
    get "manage-users" => "admins#manage_users"
    post "manage-users" => "admins#update_users"
  end
  get "new-admin" => "admins#new_admin"
  post "create-admin" => "admins#create_admin"
  get "manage-admin" => "admins#manage_admin"
  get "edit-admin" => "admins#edit_admin"
  post "manage-admin/:id" => "admins#update_admin"
  resources :reviews
  get "/vote/:id" => "reviews#vote"
  match '/customers/:id/finish_signup' => 'customers#finish_signup', via: [:get, :patch], :as => :finish_signup

  scope "(:locale)", :locale => /en|es/ do

    # Destinations Path
    get "/destinations" => "destinations#index"
    post "/destinations"  => "destinations#create"
    get "/destinations/new" => "destinations#new"
    get "/destinations/:id/edit"  => "destinations#edit"
    get "/destinations/:id/traveller_images" => "destinations#traveller_images"
    get "/destinations/:slug" => "destinations#show"
    patch "/destinations/:id" => "destinations#update"
    put "/destinations/:id" => "destinations#update"
    delete "/destinations/:id" => "destinations#destroy"
    get "/destinations-by-country" => "destinations#country_wise_destinations"

    # Hotels Path
    get "/destinations/:slug/hotels" => "hotels#index"
    post  "/destinations/:destination_id/:destination_name/hotels" => "hotels#create"
    get "/destinations/:destination_id/:destination_name/hotels/new" => "hotels#new"
    get "/destinations/:destination_id/:destination_name/hotels/:id/edit"  => "hotels#edit"
    get "/destinations/:destination_id/:destination_name/hotels/:id/:name" => "hotels#show"
    put "/destinations/:destination_id/:destination_name/hotels/:id" => "hotels#update"
    delete  "/destinations/:destination_id/:destination_name/hotels/:id" => "hotels#destroy"
    patch "/destinations/:destination_id/:destination_name/hotels/:id" => "hotels#update"

    #Flights Path
    get "/destinations/:slug/flights" => "flights#index"
    post  "/destinations/:destination_id/:destination_name/flights" => "flights#create"
    get "/destinations/:destination_id/:destination_name/flights/new" => "flights#new"
    get "/destinations/:destination_id/:destination_name/flights/:id/edit"  => "flights#edit"
    get "/destinations/:destination_id/:destination_name/flights/:id/:name" => "flights#show"
    put "/destinations/:destination_id/:destination_name/flights/:id" => "flights#update"
    delete  "/destinations/:destination_id/:destination_name/flights/:id" => "flights#destroy"
    patch "/destinations/:destination_id/:destination_name/flights/:id" => "flights#update"

    # Attractions Path
    get "/attractions/:slug" => "attractions#index"
    get "/attractions/:destination_slug/:attraction_slug" => "attractions#show"
    post  "/destinations/:destination_id/:destination_name/attractions" => "attractions#create"
    get "/destinations/:destination_id/:destination_name/attractions/new" => "attractions#new"
    get "/destinations/:destination_id/:destination_name/attractions/:id/edit"  => "attractions#edit"
    get "/destinations/:destination_id/:destination_name/attractions/:id/:name" => "attractions#show"
    put "/destinations/:destination_id/:destination_name/attractions/:id" => "attractions#update"
    delete  "/destinations/:destination_id/:destination_name/attractions/:id" => "attractions#destroy"
    patch "/destinations/:destination_id/:destination_name/attractions/:id" => "attractions#update"

    # Events Paths
    get "/events/:slug" => "events#index"
    get "/events/:destination_slug/:event_slug" => "events#show"
    post  "/destinations/:destination_id/:destination_name/events" => "events#create"
    get "/destinations/:destination_id/:destination_name/events/new" => "events#new"
    get "/destinations/:destination_id/:destination_name/events/:id/edit"  => "events#edit"
    get "/destinations/:destination_id/:destination_name/events/:id/:name" => "events#show"
    put "/destinations/:destination_id/:destination_name/events/:id" => "events#update"
    delete  "/destinations/:destination_id/:destination_name/events/:id" => "events#destroy"
    patch "/destinations/:destination_id/:destination_name/events/:id" => "events#update"



    # Others
    get "/all-users" => "customers#all_users"
    get "/meet-travelers" => "customers#meet_travellers"
    post 'travelers/search' => "customers#search_traveler"
    post '/validate_username' => "customers#validate_username"
    
    get "/traveler/:username" => "customers#traveler"
    get "/traveler/:username/travel_blogs" => "travel_blogs#index"
    get "back_to_blog_comment/:id" => "blog_comments#back_to_comment"
    get "reply_to_comment/:id" => "blog_comments#reply_to_comment"
    # You can have the root of your site routed with "root"
    root 'home#index'
    get "/get_cities" => "destinations#cities_per_country"
    get "/get_states" => "destinations#states_per_country"
    get "/hotels" => "hotels#all_index"
    get "/flights" => "flights#all_index"
    get "/events" => "events#all_index"
    get "/attractions" => "attractions#all_index"
    get "/search" => "home#search"
    get "/blog_categories" => "travel_blogs#get_categorized_data"
    get "/blog" => "travel_blogs#all_index"
    get "/blog_main" => "travel_blogs#blog_main"
    get "/blog/post/:id" => "travel_blogs#single_post"
    get "/blog/new" => "travel_blogs#new_post"
    post "/blog/new" => "travel_blogs#create_post"
    get "/blog/edit/:id" => "travel_blogs#edit_post"
    delete "/blog/delete/:id" => "travel_blogs#destroy"
    post "/blog/edit/:id" => "travel_blogs#update_post"
    post "blog/post/:id/comment" => "blog_comments#create"
    get "dynamic_js_content.:ext" => "home#dynamic_content"
    get "/filter-destinations" => "destinations#filter_destinations"
    get "contact" => "home#contact"
    get "faq" => "home#faq"
    get "contribute" => "home#contribute"
    get "advertise" => "home#advertise"
    get "privacy" => "home#privacy"
    get "careers" => "home#careers"
    get "terms" => "home#terms"
    get "partner_with_us" => "home#partner_with_us"
    post "contact" => "home#contact_form"
    get "/about" => "home#about"
    get "/add-listing" => "home#add_listing"
    post "/add-listing" => "home#save_listing"
    get "/report-spam" => "home#report_spam"
    post "/report-spam" => "home#save_spam"
    get "/report-fraud" => "home#report_fraud"
    post "/report-fraud" => "home#save_fraud"
    post "about" => "home#about"
    post "/share-as-email" => "home#share_as_email"
    get 'wishlists/add' => "wishlists#create"
    get '/customers/:customer_id/wishlists' => "wishlists#index"
    get 'recommendations/add' => "recommendations#create"
    get '/customers/:customer_id/recommendations' => "recommendations#index"
    get '/customers/:customer_id/posts' => "customers#posts"
    post '/posts/:id/comment' => "customers#comment"
    get '/posts/:id/show' => "customers#show_post"
    get '/posts/:id/edit' => "customers#edit_post"
    post '/posts/:id/update' => "customers#update_post"
    get '/posts/:id/like' => "customers#like"
    get '/posts/:id/remove' => "customers#remove_post"
    get '/customers/:customer_id/gallery' => "customers#gallery"
    get '/customers/:customer_id/gallery/:country' => "customers#gallery_by_country"
    get '/customers/:customer_id/followings' => "customers#followings"
    get '/customers/:customer_id/followers' => "customers#followers"
    get "customers/accept_as_public_traveller" => "customers#update_visibility"
    get "/customers/create-post" => "customers#create_post"
    post "/customers/create-post" => "customers#save_post"
    get "/recommendation" => "customers#recommendation"
    get '/wishlists/auth' => "wishlists#index"
    get '/checkins/auth' => "checkins#index"
    get '/recommendation/auth' => "recommendations#index"
    delete 'customers/:customer_id/wishlists/:id' => "wishlists#destroy"
    delete 'customers/:customer_id/recommendations/:id' => "recommendations#destroy"
    get 'checkins/add' => "checkins#create"
    get 'customers/:customer_id/checkins' => "checkins#index"
    delete 'customers/:customer_id/checkins/:id' => "checkins#destroy"
    get '/terms' => "admins#terms"
    get '/faq' => "admins#faq"
    get '/careers' => "admins#careers"
    get '/privacy' => "admins#privacy"
    get '/about' => "admins#about_us"
    get '/terms/edit' => "admins#edit_terms"
    get '/faq/edit' => "admins#edit_faq"
    get '/careers/edit' => "admins#edit_careers"
    get '/privacy/edit' => "admins#edit_privacy"
    get '/about/edit' => "admins#edit_about_us"
    post '/terms/edit' => "admins#update_terms"
    post '/faq/edit' => "admins#update_faq"
    post '/careers/edit' => "admins#update_careers"
    post '/privacy/edit' => "admins#update_privacy"
    post '/about/edit' => "admins#update_about_us"
    get "/change-status" => "customers#change_status"
    post "/change-status" => "customers#update_status"

    resources :destinations do
      get "traveller_images"
      resources :flights do
        resources :flight_facilities
      end
      resources :hotels do
        resource :hotel_facilities
        resource :mores
      end
      resources :attractions
      resources :events
    end

    patch "/update_d_rating/:id" => "ratings#update_destination"
    patch "update_f_rating/:id" => "ratings#update_flight"
    patch "update_h_rating/:id" => "ratings#update_hotel"
    post "/destinations/:id/uploads/create" => "destinations#upload_create"

    # post "images/create" => "images#create"
    get "/add_flights" => "flights#new"
    get "/add_hotels" => "hotels#new"
    get "/add_events" => "events#new"
    get "/add_attractions" => "attractions#new"
    get "/forums" => "forum#index"
    get "/forum/:region" => "topics#index"
    get "/forum/:id/additional" => "topics#additional_index"
    get "/forum/:id/topics/:topic_id" => "topics#additional_show"
    get "/forum/country/:country_name" => "forum#country"
    scope "/forum" do
      resources :topics do
        resources :post_topics
      end
    end

    resources :customers, :only => [:show] do
      resources :trips
      resources :photos
      resources :travel_blogs do 
        resources :blog_comments
      end
      get "friends" => "customers#friends"
      get "add-friend" => "customers#add_users"
      get "follow" => "customers#follow"
      get "unfollow" => "customers#unfollow"
      get "edit_profile" => "profiles#edit"
      post "edit_profile" => "profiles#update"
      post  "edit_image"  =>  "profiles#update_profile_image"
      post "disconnect-users/:id" => "customers#disconnect_users"
      get "profile" => "profiles#show"
      get "collections" => "customers#collections"
      get "likes" => "customers#likes"
      get "reviews" => "customers#reviews"
    end
    get "/notifications" => "customers#notifications"

    resources :conversations, only: [:index, :create] do
      resources :messages, only: [:index, :create]
    end
    
  end
  
  get '*unmatched_route', to: 'application#not_found'
end

