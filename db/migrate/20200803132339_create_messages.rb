class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
      t.text :body
      t.references :conversation, foreign_key: true
      t.references :customer, foreign_key: true
      t.boolean :read, default: false

      t.timestamps
    end
  end
end
