class CheckinsController < ApplicationController
  before_filter :authenticate_customer!

  def index
    @customer = Customer.includes(:checkins).find(params["customer_id"])
    @profile = @customer.profile
    @checkin = @customer.checkins.pluck(:destination_id)
    @destinations = Destination.includes(:attractions, :ratings, :events).where("id in (?) ", @checkin).order("created_at DESC")
    render layout: "v2"
  end
  
  def create
    @checkin = Checkin.where("customer_id = (?) AND destination_id = (?)", params["checkin"]["customer_id"], params["checkin"]["destination_id"])
    @is_added = true
    if @checkin.present?
      @destination = @checkin.first.destination_id
      if @checkin.destroy_all
        @is_added = false
        flash.now[:success] = "Removed from checkin"
      else
        flash.now[:error] = @checkin.errors
      end
    else
      @checkin = Checkin.new(checkin_params)
      @destination = @checkin.destination_id
      if @checkin.save!
        flash.now[:success] = "Added to checkin"
      else
        flash.now[:error] = @checkin.errors
      end
    end
    @checkin_count = Checkin.where("destination_id = (?)", params["checkin"]["destination_id"]).count
  end

  def destroy
    @customer = Customer.find(params["customer_id"])
    @checkin = Checkin.where("customer_id = (?) AND destination_id = (?)", params["customer_id"], params["id"])
    if @checkin.destroy_all
      flash[:success] = "Removed from checkin"
      redirect_to "/customers/#{@customer.id}/checkins"
    end
  end
  
  private
  def checkin_params
    params.require(:checkin).permit(:customer_id, :destination_id)
  end

end
