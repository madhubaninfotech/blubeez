class ChangeKeywordsToDestinations < ActiveRecord::Migration
  def change
  	change_column :destinations, :keywords, :text
  	change_column :events, :keywords, :text
  	change_column :attractions, :keywords, :text
  	change_column :hotels, :keywords, :text
  	change_column :flights, :keywords, :text
  end
end
