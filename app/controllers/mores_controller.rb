class MoresController < ApplicationController

  def create
    @more = More.new(more_params)
    if @more.save!
      flash.now[:notice] = "Successfully Updated"
    else
      flash.now[:error] = @more.errors
    end
  end

  def update
    @more = More.where(:hotel_id=> params["hotel_id"]).first
    if @more.update(more_params)
      flash.now[:notice] = "Successfully Updated"
    else
      flash.now[:error] = @more.errors
    end
  end

  def destroy
  end

  private

  def more_params
    params.require(:more).permit(:check_in, :check_out, :pets, :payments, :hotel_id)
  end

end