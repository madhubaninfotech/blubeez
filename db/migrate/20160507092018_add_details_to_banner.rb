class AddDetailsToBanner < ActiveRecord::Migration
  def change
    add_column :banners, :title, :string
    add_column :banners, :desc, :text
    add_column :banners, :button_text, :string
    add_column :banners, :button_link, :text
  end
end
