class AttractionsController < ApplicationController
  before_action :authenticate_admin!, except: [:index, :show, :all_index]
  before_action :set_attraction, only: [:edit, :update, :destroy]

  # GET /attractions
  # GET /attractions.json
  def index
    @dest = Destination.includes(:attractions).where(slug: params["slug"]).first
    if @dest.present?
      @attractions = @dest.attractions.all
    else
      return redirect_to "/attractions"
    end
  end

  # GET /attractions/1
  # GET /attractions/1.json
  def show
    @attraction = Attraction.where(slug: params[:attraction_slug]).first
    if @attraction.present?
      @destination = @attraction.destination
    else
      return redirect_to "/attractions"
    end
  end

  def all_index
    @attractions = Attraction.all
  end

  # GET /attractions/new
  def new
    @is_with_location = params["add_location"].present? ? true : false
    unless @is_with_location
      dest = Destination.where(id: params["destination_id"]).first
      @attraction = dest.attractions.new
      # add_breadcrumb "New Flights", new_destination_atrraction_path(params["destination_id"]), "new Flights"
    else
      @attraction = Attraction.new
    end
  end

  # GET /attractions/1/edit
  def edit
    dest = Destination.where(id: params["destination_id"]).first
    unless dest.attractions.blank?
      @attraction = dest.attractions.find(params["id"])
    end
  end

  # POST /attractions
  # POST /attractions.json
  def create

    @add_location = params["add_location"].present? ? true : false

    unless @add_location
      dest = Destination.where(id: params["destination_id"]).first
    else
      if params["locations"].present?
        if params["locations"]["country"].present? && params["locations"]["state"].present? && params["locations"]["cities"].present? && params["locations"]["name"].present?
          dest = AppUtils.get_location_details_for_flight_or_hotel(params["locations"]).first
        else
          redirect_to "/add_attractions/?add_location=true", :flash=> {:error=>"Enter valid location"}
        end
      else
        redirect_to "/add_attractions/?add_location=true", :flash=> {:error=>"Enter valid location"}
      end
    end

    @attraction = dest.attractions.new(attraction_params)

    respond_to do |format|
      if @attraction.save
        DynamicJob.perform_async
        format.html { redirect_to "/attractions", notice: 'Flight was successfully created.' }
        format.json { render :show, status: :created, location: @attraction }
      else
        format.html { render :new }
        format.json { render json: @attraction.errors, status: :unprocessable_entity }
      end
    end

  end

  # PATCH/PUT /attractions/1
  # PATCH/PUT /attractions/1.json
  def update
    dest = Destination.where(id: params["destination_id"]).first
    @attraction = dest.attractions.find(params["id"])
    respond_to do |format|
      if @attraction.update(attraction_params)
        format.html { redirect_to "/attractions", notice: 'Attraction was successfully updated.' }
        format.json { render :show, status: :ok, location: @attraction }
      else
        format.html { render :edit }
        format.json { render json: @attraction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /attractions/1
  # DELETE /attractions/1.json
  def destroy
    @attraction.destroy
    respond_to do |format|
      format.html { redirect_to attractions_url, notice: 'Attraction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_attraction
      @attraction = Attraction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def attraction_params
      params.require(:attraction).permit(:name, :addr_no, :addr_street, :addr_city, :image, :main_image, :price, :destination_id, :slug)
    end
end
