json.array!(@attractions) do |attraction|
  json.extract! attraction, :id, :name, :addr_no, :addr_street, :addr_city, :image, :price, :destination_id
  json.url attraction_url(attraction, format: :json)
end
