class AirvtrMailer < ApplicationMailer
  helper :application
  
  def contact_email(contact_params)
    sub  = "#{contact_params["subject"]}"
    @data = contact_params
    mail(:from=> contact_params["email"], :to => "contact@blubeez.com", :subject => sub, :date => Time.now)
  end

  def report_spam_email(contact_params)
    sub  = "#{contact_params["subject"]}"
    @data = contact_params
    mail(:from=> contact_params["email"], :to => "report@blubeez.com", :subject => sub, :date => Time.now)
  end

  def report_fraud_email(contact_params)
    sub  = "#{contact_params["subject"]}"
    @data = contact_params
    mail(:from=> contact_params["email"], :to => "report@blubeez.com", :subject => sub, :date => Time.now)
  end

  def blog_share_email(email, path)
    @path = path
    sub  = "Blog Link"
    mail(:from=> "info@blubeez.com", :to => email, :subject => sub, :date => Time.now)
  end

end
