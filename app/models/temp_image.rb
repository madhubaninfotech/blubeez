class TempImage < ActiveRecord::Base
	belongs_to :hotel
	belongs_to :destination
	belongs_to :flight
	belongs_to :event
	belongs_to :attraction
	belongs_to :banner
	belongs_to :travel_blog
	belongs_to :trip
end