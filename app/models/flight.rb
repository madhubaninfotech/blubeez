class Flight < ActiveRecord::Base
  # mount_uploaders :image, ImageUploader
  # mount_uploader :main_image, ImageUploader
  # validates_uniqueness_of :name, case_sensitive: false
  belongs_to :destination
  has_many :ratings, dependent: :destroy
  has_many :reviews, dependent: :destroy
  has_one :temp_image, dependent: :destroy
  has_one :flight_facility, dependent: :destroy
  validates_uniqueness_of :slug
  validates_presence_of :slug
  scope :flight_name_search, -> (f_name) { where( 'name = ?', f_name)}

  def average_rating
    ratings.map(&:score).sum / (ratings.size != 0 ? ratings.size : 1)
  end
  
  def self.get_desination_image_by_main_image_or_secondary(flight)
    if flight.main_image.present?
      main_image = AppUtils.get_single_image_from_image_modal(flight.main_image)
      if main_image.present?
        main_image.image.v_3x.url
      end
    elsif flight.image.present? && JSON.parse(flight.image).present?
      images_data = AppUtils.get_images_from_image_modal(flight.image)[0] if flight.image.present?
      if images_data.present?
        images_data[1].v_3x.url
      end
    else
      "img/1.jpg"
    end
  end
  
end
