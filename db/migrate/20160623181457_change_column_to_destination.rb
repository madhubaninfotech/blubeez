class ChangeColumnToDestination < ActiveRecord::Migration
  def change
    change_column :destinations, :destination_image, :text, array: true, :default => nil, :null => true
    change_column :flights, :image, :string, array: true, :default => nil, :null => true
    change_column :hotels, :image, :string, array: true, :default => nil, :null => true
  end
end
