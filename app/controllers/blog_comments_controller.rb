class BlogCommentsController < ApplicationController
  before_action :set_blog_comment, only: [:show, :edit, :update, :destroy]

  def index
    @blog_comments = BlogComment.all
  end

  def show
  end

  def new
    # @blog_comment = BlogComment.new
  end

  def back_to_comment
    @blog_comment = BlogComment.find(params["id"])
    @travel_blog = @blog_comment.travel_blog
    @customer = @blog_comment.customer
  end

  def reply_to_comment
    @blog_comment = BlogComment.find(params[:id])
    @customer = @blog_comment.customer
    @travel_blog = @blog_comment.travel_blog
  end

  def edit
    @customer = Customer.find(params["customer_id"])
    @travel_blog = @customer.travel_blogs.find(params["travel_blog_id"])
    @blog_comment = @travel_blog.blog_comments.find(params["id"])
  end

  def create
    params["travel_blog_id"] = params["blog_comment"]["travel_blog_id"] if params["blog_comment"].present?
    @travel_blog = TravelBlog.find(params["travel_blog_id"])
    @blog_comment = BlogComment.new(blog_comment_params)
    if @blog_comment.save
      @blog_comments =  @travel_blog.blog_comments.all
    else
      @blog_comment.errors
    end
  end

  def update
    @travel_blog = TravelBlog.find(params["travel_blog_id"])
    @blog_comment = @travel_blog.blog_comments.find(params["id"])

    # respond_to do |format|
    if @blog_comment.update(blog_comment_params)
      @blog_comment
    else
      @blog_comment.errors
    end

  end
  
  def destroy
    @blog_comment.destroy
    respond_to do |format|
      format.html { redirect_to blog_comments_url, notice: 'Blog comment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_blog_comment
      @blog_comment = BlogComment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def blog_comment_params
      params.require(:blog_comment).permit(:name, :comment, :travel_blog_id, :reply_for, :customer_id)
    end
end
