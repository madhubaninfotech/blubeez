class ShortLinksController < ApplicationController
  def show
    @link = ShortLink.find_by_slug(params[:slug]) 
    render 'errors/404', status: 404 if @link.nil?
    @link.update_attribute(:clicked, @link.clicked + 1)
    redirect_to @link.url
  end
  
  def create
    short_url = ShortLink.shorten(params[:url])
    render json: {url: short_url}.to_json
  end
end