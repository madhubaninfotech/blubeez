class AddSlugToEvents < ActiveRecord::Migration
  def change
    add_column :events, :slug, :string
    add_index :events, :slug
    add_column :events, :title, :text
    add_column :events, :keywords, :string

    add_column :flights, :slug, :string
    add_index :flights, :slug
    add_column :flights, :title, :text
    add_column :flights, :keywords, :string

    add_column :hotels, :slug, :string
    add_index :hotels, :slug
    add_column :hotels, :title, :text
    add_column :hotels, :keywords, :string

    add_column :attractions, :slug, :string
    add_index :attractions, :slug
    add_column :attractions, :title, :text
    add_column :attractions, :keywords, :string
  end
end
