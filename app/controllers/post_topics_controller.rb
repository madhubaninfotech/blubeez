class PostTopicsController < ApplicationController
  before_action :set_post_topic, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_customer!

  # GET /post_topics
  # GET /post_topics.json
  def index
    @post_topics = PostTopic.all
  end

  # GET /post_topics/1
  # GET /post_topics/1.json
  def show
  end

  # GET /post_topics/new
  def new
    @topic = Topic.find(params["topic_id"])
    @post_topic = @topic.post_topics.new
    if params["is_quote"].present?
      if params["is_topic"].present? && params["is_topic"] == "true"
        @quote_content = @topic.content
      else
        @quote_content = @topic.post_topics.find(params["topic"]).content
      end
    end
  end

  # GET /post_topics/1/edit
  def edit
  end

  # POST /post_topics
  # POST /post_topics.json
  def create
    if params["post_topic"]["quote"].present?
      params["post_topic"]["content"] = "<div class='quote-text'>"+params["post_topic"]["quote"]+"</div><br/><br/>"+params["post_topic"]["content"]
    end
    @post_topic = PostTopic.new(post_topic_params)
    
    if @post_topic.save
      flash.now["notice"] = 'Post topic was successfully created.'
    else
      flash.now["error"] = 'Post topic was failed.'
    end
  end

  # PATCH/PUT /post_topics/1
  # PATCH/PUT /post_topics/1.json
  def update
    respond_to do |format|
      if @post_topic.update(post_topic_params)
        format.html { redirect_to @post_topic, notice: 'Post topic was successfully updated.' }
        format.json { render :show, status: :ok, location: @post_topic }
      else
        format.html { render :edit }
        format.json { render json: @post_topic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /post_topics/1
  # DELETE /post_topics/1.json
  def destroy
    @post_topic.destroy
    respond_to do |format|
      format.html { redirect_to post_topics_url, notice: 'Post topic was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post_topic
      @post_topic = PostTopic.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_topic_params
      params.require(:post_topic).permit(:title, :content, :safe, :closed, :trusted, :topic_id, :customer_id)
    end
end
