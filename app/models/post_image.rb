class PostImage < ApplicationRecord
  belongs_to :post
	mount_uploader :image, ImageUploader  
  
  attr_accessor :is_watermark_enabled
end
