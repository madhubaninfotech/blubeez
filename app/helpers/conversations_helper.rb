module ConversationsHelper
	def get_conversion(user)
		conversation = Conversation.where("(sender_id = ? and receiver_id = ?) or (receiver_id = ? and sender_id = ?)", user, current_customer.id, user, current_customer.id).first
		conversation.id
	end
end
