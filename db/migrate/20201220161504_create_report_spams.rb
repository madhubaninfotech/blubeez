class CreateReportSpams < ActiveRecord::Migration[5.0]
  def change
    create_table :report_spams do |t|
      t.string :name
      t.string :email
      t.boolean :is_registered_with_blubeez
      t.string :phone
      t.string :spam_against_person
      t.string :spam_profile_link
      t.integer :spam_type
      t.text :message
      t.boolean :is_action_taken, default: false

      t.timestamps
    end
  end
end
