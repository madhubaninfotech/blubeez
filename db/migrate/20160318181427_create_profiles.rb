class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :name
      t.string :email
      t.datetime :d_o_b
      t.string :avatar
      t.text :about_user
      t.string :location
      t.string :gender
      t.belongs_to :customer, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
