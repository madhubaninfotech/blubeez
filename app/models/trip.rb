class Trip < ActiveRecord::Base
  belongs_to :customer
  mount_uploader :trip_photos, ImageUploader
  has_one :temp_image, dependent: :destroy
end
