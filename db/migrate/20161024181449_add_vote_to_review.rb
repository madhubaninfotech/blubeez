class AddVoteToReview < ActiveRecord::Migration
  def change
    add_column :reviews, :vote, :integer, default: 0
    add_column :reviews, :voter_id, :text
  end
end
