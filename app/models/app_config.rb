class AppConfig
  APP_YML_CONFIG = YAML::load(IO.read("#{Rails.root}/config/application.yml"))

  def self.config
    APP_YML_CONFIG
  end

  def self.get_weather_key
		self.config["weather_api"]["key"]
  end

  def self.geocoder_key
  	self.config["geocoder"]["key"]
  end

end