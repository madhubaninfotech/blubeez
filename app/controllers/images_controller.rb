class ImagesController < ApplicationController

  def index
    if params["dest_id"].present?
      image = Destination.where(id: params["dest_id"]).first.main_image
    elsif params["hotel_id"].present?
      image = Hotel.where(id: params["hotel_id"]).first.main_image
    elsif params["flight_id"].present?
      image = Flight.where(id: params["flight_id"]).first.main_image
    elsif params["event_id"].present?
      image = Event.where(id: params["event_id"]).first.image
    elsif params["attraction_id"].present?
      image = Attraction.where(id: params["attraction_id"]).first.main_image
    end
    if image.present?
      @pictures = Image.where(id: image).first
      render :json => [@pictures.to_jq_upload(@pictures.image)].to_json
    else
      render :json => [].to_json
    end
  end

  def multiple_index
    if params["dest_id"].present?
      images = Destination.where(id: params["dest_id"]).first.destination_image
    elsif params["hotel_id"].present?
      images = Hotel.where(id: params["hotel_id"]).first.image
    elsif params["flight_id"].present?
      images = Flight.where(id: params["flight_id"]).first.image
    elsif params["rev_id"].present?
      images = Review.where(id: params["rev_id"]).first.images
    elsif params["attraction_id"].present?
      images = Attraction.where(id: params["attraction_id"]).first.image
    end
    if images.present?
      images = JSON.parse(images)
      @pictures = Image.where(id: images)
      render :json => @pictures.collect { |p| p.to_jq_upload(p.images) }.to_json
    else
      render :json => [].to_json
    end
  end

  def create_multiple
    p_attr = params[:image]
    p_attr[:images] = params[:image][:images].first if params[:image][:images].class == Array

    @picture = Image.new
    @picture.is_watermark_enabled = params[:image][:is_watermark_enabled].present? ? true : false
    @picture.images = p_attr[:images]
    @picture.desc = params[:image][:desc]
    if @picture.save
      respond_to do |format|
        format.html {  
          render :json => [@picture.to_jq_upload(@picture.images)].to_json, 
          :content_type => 'text/html',
          :layout => false
        }
        format.json {  
          render :json => { :files => [@picture.to_jq_upload(@picture.images)] }         
        }
      end
    else 
      render :json => [{:error => "custom_failure"}], :status => 304
    end
  end

  def create
    @picture = Image.new
    @picture.is_watermark_enabled = params[:image][:is_watermark_enabled].present? ? true : false
    @picture.image = params[:image][:image]
    @picture.desc = params[:image][:desc]
    if @picture.save
      respond_to do |format|
        format.html {  
          render :json => [@picture.to_jq_upload(@picture.image)].to_json, 
          :content_type => 'text/html',
          :layout => false
        }
        format.json {  
          render :json => { :files => [@picture.to_jq_upload(@picture.image)] }        
        }
      end
    else 
      render :json => [{:error => "custom_failure"}], :status => 304
    end
  end

  def update_description
    @image = Image.find(params["image_id"])
    @image.desc = params["image"]["desc"]
    if @image.save
      flash[:notice] = "Image Uploaded successfully"
    else
      flash[:notice] = @image.errors
    end
  end

  def destroy
    @picture = Image.find(params[:id])
    @picture.destroy
    render :json => true
  end

  # def create
  #   params["image"]["images"] = params["image"]["images"][0] if params["image"]["images"].present?
  #   @image = Image.new(image_params)
  #   if @image.save!
  #   else
  #   end
  # end

  def update
    
  end

  def delete
    
  end

  private
  def image_params
    params.require(:image).permit(:image,  :id, :desc, :images, :is_watermark_enabled)
  end
end