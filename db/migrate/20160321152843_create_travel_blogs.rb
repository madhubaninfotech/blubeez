class CreateTravelBlogs < ActiveRecord::Migration
  def change
    create_table :travel_blogs do |t|
      t.string :title
      t.text :description
      t.belongs_to :customer, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
