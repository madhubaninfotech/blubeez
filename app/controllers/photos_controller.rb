class PhotosController < ApplicationController
  before_action :set_photo, only: [:show, :edit, :update, :destroy]
  before_action :both_accessible?, except: [:index, :show]

  # GET /photos
  # GET /photos.json
  def index
    @customer = Customer.find(params["customer_id"])
    @photos = @customer.photos.all
  end

  # GET /photos/1
  # GET /photos/1.json
  def show
    @customer = Customer.find(params["customer_id"])
    @photo = @customer.photos.find(params["id"])
  end

  # GET /photos/new
  def new
    @customer = Customer.find(params["customer_id"])
    @photo = @customer.photos.new
    @location = Hash.new
  end

  # GET /photos/1/edit
  def edit
    @customer = Customer.find(params["customer_id"])
    unless @customer.photos.blank?
      @photo = @customer.photos.find(params["id"])
      @location = AppUtils.location_formatter_parse(@photo.location) if !@photo.location.nil?
    end
  end

  # POST /photos
  # POST /photos.json
  def create
    customer = Customer.find(params["customer_id"])
    params[:photo][:location] = AppUtils.location_formatter_stringify(params[:locations])
    @photo = customer.photos.new(photo_params)

    respond_to do |format|
      if @photo.save
        NotificationJob.perform_async(@photo.id, AppConstants::TRAVEL_PHOTOS, current_customer.id)
        format.html { redirect_to customer_photos_path, notice: 'Photo was successfully created.' }
        format.json { render :show, status: :created, location: @photo }
      else
        format.html { render :new }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /photos/1
  # PATCH/PUT /photos/1.json
  def update
    customer = Customer.find(params["customer_id"])
    params[:photo][:location] = AppUtils.location_formatter_stringify(params[:locations])
    @photo = customer.photos.find(params["id"])
    
    respond_to do |format|
      if @photo.update(photo_params)
        NotificationJob.perform_async(@photo.id, AppConstants::TRAVEL_PHOTOS, current_customer.id)
        format.html { redirect_to customer_photos_path, notice: 'Photo was successfully updated.' }
        format.json { render :show, status: :ok, location: @photo }
      else
        format.html { render :edit }
        format.json { render json: @photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /photos/1
  # DELETE /photos/1.json
  def destroy
    @photo.destroy
    respond_to do |format|
      format.html { redirect_to customer_photos_path, notice: 'Photo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_photo
      @photo = Photo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def photo_params
      params.require(:photo).permit(:trip_name, :location, :trip_description, {:attachments => []}, :date, :customer_id, :videos)
    end
end
