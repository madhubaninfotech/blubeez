class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.references :flight, index: true, foreign_key: true
      t.references :hotel, index: true, foreign_key: true
      t.references :destination, index: true, foreign_key: true
      t.references :customer, index: true, foreign_key: true
      t.integer :score, default: 0

      t.timestamps null: false
    end
  end
end
