class DestinationsController < ApplicationController
  before_action :authenticate_admin!, except: [:index, :show, :dynamic_content, :cities_per_country, :states_per_country, :filter_destinations, :upload_create, :traveller_images, :country_wise_destinations]
  before_action :set_destination, only: [:edit, :update, :destroy]
  add_breadcrumb "Destination", :destinations_path

  def index
    sorter_str = "destinations.created_at"
    session[:sort_by] = session[:sort_by].present? ? session[:sort_by] : AppConstants::SORT_BY_DATE
    session[:starts_with] = session[:starts_with].present? ? session[:starts_with] : ""
    if params[:sortby].present?
      session[:sort_by] = params[:sortby].to_i
    end
    if params[:starts_with].present?
      session[:starts_with] = params[:starts_with] == "all" ? "" : params[:starts_with]
    end
    if session[:sort_by] == AppConstants::SORT_BY_POPULARITY
      sorter_str = "destinations.view_count"
    elsif session[:sort_by] == AppConstants::SORT_BY_RATING
      sorter_str = "ratings.score"
    end
    if params["search_data"].present?
      session[:starts_with] = ""
      @destinations = Destination.
        includes(:images, :attractions, :ratings, :events, :wishlist, :checkins).
        # with_main_image.
        destination_name_search(params["name"]).
        with_search(session[:starts_with]).
        order("#{sorter_str} DESC").
        paginate(page: params[:page], per_page: 10)
    else
      @destinations = Destination.
        includes(:images, :attractions, :ratings, :events, :wishlist, :checkins).
        # with_main_image.
        with_search(session[:starts_with]).
        order("#{sorter_str} DESC").
        paginate(page: params[:page], per_page: 10)      
    end
    render layout: "v2"
  end

  def show
    @destination = Destination.includes(:attractions, :events, :images).where(slug: params[:slug]).first
    @seo_title = ""
    if @destination.present? 
      @seo_title = @destination.title
      places  = Array.new
      places << @destination
      if @destination.nearbys(100).present?
        @destination.nearbys(100).each do |dest|
          places << dest
        end
      end
      @marker_hash = Gmaps4rails.build_markers(places) do |d, marker|
        marker.lat d.latitude
        marker.lng d.longitude
      end
      AppUtils.view_counter(@destination)
      @reviews = Review.destination_search(@destination.id)
      # images = AppUtils.get_travellers_images(@reviews)
      # @traveller_images = Image.where(:id=> images) if images.present?
      @tags = AppUtils.get_tag_names_for_destination(@destination)
      @meta_desc = "#{@destination.destination_name} - #{@destination.destination_description}"
      @meta_key = @destination.keywords
      render layout: "v2"
    else
      return render :html=> "<h4 class='text-center'>No Destination Found</h4>".html_safe, layout: "application"
    end
  end
  
  def traveller_images
    @reviews = Review.destination_search(params[:id])
    @traveller_images = AppUtils.get_travellers_images(@reviews)
  end

  # GET /destinations/new
  def new
    @destination = Destination.new
  end

  def edit
  end

  def create
    @destination = Destination.new(destination_params)

    respond_to do |format|
      if @destination.save
        DynamicJob.perform_async
        format.html { redirect_to "/destinations/#{@destination.slug}", notice: 'Destination was successfully created.' }
        format.json { render :show, status: :created, location: "/destinations/#{@destination.id}/#{@destination.destination_name}" }
      else
        format.html { render :new }
        format.json { render json: @destination.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /destinations/1
  # PATCH/PUT /destinations/1.json
  def update
    respond_to do |format|
      if @destination.update(destination_params)
        DynamicJob.perform_async
        unless params["destination"]["main_image"] == params["main_old_image"]
          img = Image.where(id: params["main_old_image"]).first
          img.destroy if img.present?
        end
        format.html { redirect_to "/destinations/#{@destination.slug}", notice: 'Destination was successfully updated.' }
        format.json { render :show, status: :ok, location: "/destinations/#{@destination.id}/#{@destination.destination_name}" }
      else
        format.html { render :edit }
        format.json { render json: @destination.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /destinations/1
  # DELETE /destinations/1.json
  def destroy
    @destination.destroy
    respond_to do |format|
      DynamicJob.perform_async
      format.html { redirect_to destinations_url, notice: 'Destination was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  def cities_per_country
    respond_to do |format|
      format.json{
        country_code = params["country"].downcase
        state_code = params["state"].downcase
        cities_arr = AppUtils.get_cities_selectable_format(country_code, state_code)
        render json: cities_arr
      }
    end
  end

  def states_per_country
    respond_to do |format|
      format.json{
        country_code = params["country"].downcase
        states_arr = AppUtils.get_states_selectable_format(country_code)
        render json: states_arr
      }
    end
  end

  def dynamic_content
  end
  
  def upload_create
  end

  def filter_destinations
    @destinations = Destination.get_destination_filters(params)
  end
  
  def country_wise_destinations
    session[:keyword] = session[:keyword].present? ? session[:keyword] : ""
    if params[:keyword].present?
      session[:keyword] = params[:keyword] == "All" ? "" : params[:keyword]
    end
    @countries = AppUtils.country_list_by_first_letter session[:keyword].to_s
    @destinations = Destination.destination_countries_search @countries.map{|c| c.alpha2}
    @destination_group = @destinations.group_by(&:destination_country)
    render layout: "v2"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_destination
      @destination = Destination.where(id: params[:id]).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def destination_params
      params.require(:destination).permit(:destination_name, :destination_country, :destination_cities, :destination_state, :destination_description, :destination_image, :main_image, :price_from, :destination_category, :best_for, :slug, :title, :keywords)
    end
end
