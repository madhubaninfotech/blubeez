class ReportSpam < ApplicationRecord
  validates_presence_of :name, :email, :is_registered_with_blubeez, :phone, :spam_against_person, :spam_profile_link, :spam_type, :message

  FAKE_ACCOUNT          = 1
  FAKE_NAME             = 2
  INAPPROPRIATE_PHOTO   = 3
  INAPPROPRIATE_POST    = 4
  OTHER                 = 5
end
