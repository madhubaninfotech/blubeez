class AddStatusToCustomer < ActiveRecord::Migration
  def change
    add_column :customers, :user_status, :integer
  end
end
