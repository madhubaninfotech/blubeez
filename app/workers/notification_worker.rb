class NotificationWorker
  include Sidekiq::Worker

  def perform(data, type, cus_id)
    Notification.sync_notification(data, type, cus_id)
  end
end