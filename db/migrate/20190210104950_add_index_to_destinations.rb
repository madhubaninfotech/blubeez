class AddIndexToDestinations < ActiveRecord::Migration
  def change
    add_index :destinations, :slug
  end
end
