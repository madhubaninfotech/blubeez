class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.string :image
      t.text :about
      t.text :detail
      t.datetime :date
      t.belongs_to :destination, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
