class AddNameToAdmin < ActiveRecord::Migration
  def change
    add_column :admins, :name, :string
    add_column :admins, :is_admin, :boolean
    add_column :admins, :is_active, :integer
  end
end
