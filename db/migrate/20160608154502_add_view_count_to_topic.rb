class AddViewCountToTopic < ActiveRecord::Migration
  def change
    add_column :topics, :view_count, :integer, default: 0
  end
end
