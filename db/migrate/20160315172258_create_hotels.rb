class CreateHotels < ActiveRecord::Migration
  def change
    create_table :hotels do |t|
      t.belongs_to :destination, index: true
      t.string :name
      t.text :description
      t.string :image
      t.text :price_from

      t.timestamps null: false
    end
  end
end
