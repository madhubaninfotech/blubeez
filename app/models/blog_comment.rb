class BlogComment < ActiveRecord::Base
  belongs_to :travel_blog
  belongs_to :customer
  validates_presence_of :customer_id, :travel_blog_id, :comment, :name
end
