class Listing < ApplicationRecord
  validates_presence_of :name, :phone, :email, :location, :service_desc, :website, :is_owner, :message
end
