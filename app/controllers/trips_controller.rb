class TripsController < ApplicationController
  before_action :both_accessible?, except: [:index, :show]
  before_action :set_trip, only: [:show, :edit, :update, :destroy]

  # GET /trips
  # GET /trips.json
  def index
    @customer = Customer.find(params["customer_id"])
    @trips = @customer.trips.all
  end

  # GET /trips/1
  # GET /trips/1.json
  def show
    @customer = Customer.find(params["customer_id"])
    @trip = @customer.trips.find(params["id"])
  end

  # GET /trips/new
  def new
    @customer = Customer.find(params["customer_id"])
    @trip = @customer.trips.new
    @location = Hash.new
  end

  # GET /trips/1/edit
  def edit
    @customer = Customer.find(params["customer_id"])
    unless @customer.trips.blank?
      @trip = @customer.trips.find(params["id"])
      @trip.start_date = @trip.start_date.to_date.strftime("%d/%m/%Y")
      @trip.end_date = @trip.end_date.to_date.strftime("%d/%m/%Y")
      @location = AppUtils.location_formatter_parse(@trip.location) if !@trip.location.nil?
    end
  end

  # POST /trips
  # POST /trips.json
  def create
    customer = Customer.find(params["customer_id"])
    params[:trip][:location] = AppUtils.location_formatter_stringify(params[:locations])
    @trip = customer.trips.new(trip_params)

    respond_to do |format|
      if @trip.save
        NotificationJob.perform_async(@trip.id, AppConstants::TRAVEL_TRIP, current_customer.id)
        format.html { redirect_to customer_trips_path, notice: 'Trip was successfully created.' }
        format.json { render :show, status: :created, location: @trip }
      else
        format.html { render :new }
        format.json { render json: @trip.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trips/1
  # PATCH/PUT /trips/1.json
  def update
    customer = Customer.find(params["customer_id"])
    params[:trip][:location] = AppUtils.location_formatter_stringify(params[:locations])
    @trip = customer.trips.find(params["id"])

    respond_to do |format|
      if @trip.update(trip_params)
        NotificationJob.perform_async(@trip.id, AppConstants::TRAVEL_TRIP, current_customer.id)
        format.html { redirect_to customer_trips_path, notice: 'Trip was successfully updated.' }
        format.json { render :show, status: :ok, location: @trip }
      else
        format.html { render :edit }
        format.json { render json: @trip.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trips/1
  # DELETE /trips/1.json
  def destroy
    @trip.destroy
    respond_to do |format|
      format.html { redirect_to customer_trips_path, notice: 'Trip was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trip
      @trip = Trip.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trip_params
      params.require(:trip).permit(:trip_name, :location, :trip_description, :trip_photos, :start_date, :end_date, :trip_status, :customer_id)
    end
end
