class Hotel < ActiveRecord::Base
  belongs_to :destination
  # mount_uploaders :image, ImageUploader
  # mount_uploader :main_image, ImageUploader
  # validates_uniqueness_of :name, case_sensitive: false
  has_one :hotel_facility, dependent: :destroy
  has_one :more, dependent: :destroy
  has_one :temp_image, dependent: :destroy
  has_many :ratings, dependent: :destroy
  has_many :reviews, dependent: :destroy
  validates_uniqueness_of :slug
  validates_presence_of :slug

  def average_rating
    ratings.map(&:score).sum / (ratings.size != 0 ? ratings.size : 1)
  end
  
  def self.get_desination_image_by_main_image_or_secondary(hotel)
    if hotel.main_image.present?
      main_image = AppUtils.get_single_image_from_image_modal(hotel.main_image)
      if main_image.present?
        main_image.image.v_3x.url
      end
    elsif hotel.image.present? && JSON.parse(hotel.image).present?
      images_data = AppUtils.get_images_from_image_modal(hotel.image)[0] if hotel.image.present?
      if images_data.present?
        images_data[1].v_3x.url
      end
    else
      "img/1.jpg"
    end
  end

end
