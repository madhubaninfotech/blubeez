class Attraction < ActiveRecord::Base
  #mount_uploader :destination_image, ImageUploader
  belongs_to :destination
  validates_uniqueness_of :slug
  validates_presence_of :slug
  before_save :check_slug

  def self.get_desination_image_by_main_image_or_secondary(attraction)
    if attraction.main_image.present?
      main_image = AppUtils.get_single_image_from_image_modal(attraction.main_image)
      if main_image.present?
        main_image.image.v_3x.url
      end
    elsif attraction.image.present? && JSON.parse(attraction.image).present?
      images_data = AppUtils.get_images_from_image_modal(attraction.image)[0] if attraction.image.present?
      if images_data.present?
        images_data[1].v_3x.url
      end
    else
      "img/1.jpg"
    end
  end

  def check_slug
    self.slug = self.slug.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
  end

end
