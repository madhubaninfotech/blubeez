class AddPopularityToDestination < ActiveRecord::Migration
  def change
    add_column :destinations, :view_count, :integer, default: 0
    add_column :hotels, :view_count, :integer, default: 0
    add_column :flights, :view_count, :integer, default: 0
    add_column :events, :view_count, :integer, default: 0
    add_column :attractions, :view_count, :integer, default: 0
    add_column :travel_blogs, :view_count, :integer, default: 0
    add_column :trips, :view_count, :integer, default: 0
  end
end
